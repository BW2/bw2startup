' define global information classes
'
Namespace BW2Global

    ' The BW2 base exception class
    Public Class BW2Exception
        Inherits Exception

        Private Shared m_debug As Boolean = False
        Private Shared m_translator As Object = Nothing

        Public Shared Property Debug() As Boolean
            Get
                Return m_debug
            End Get
            Set(ByVal Value As Boolean)
                If System.Reflection.Assembly.GetCallingAssembly.GetName.Name.ToLower = "bw2client" Then
                    m_debug = Value
                End If
            End Set
        End Property
        Public Shared Property Translator() As Object
            Get
                Return m_translator
            End Get
            Set(ByVal Value As Object)
                m_translator = Value
            End Set
        End Property
        Protected msg As String

        <System.Diagnostics.DebuggerStepThrough()> Public Sub New(ByVal name As String)
            MyBase.New(name)
            msg = name
        End Sub
        <System.Diagnostics.DebuggerStepThrough()> Public Sub New(ByVal name As String, ByVal ex As Exception)
            MyBase.New(name, ex)
            msg = name
        End Sub

        ' concatenate the chain of exceptions into the StringBuilder
        <System.Diagnostics.DebuggerStepThrough()> _
        Public Overloads Shared Function ToString(ByVal ex As Exception, ByVal sb As System.Text.StringBuilder) As System.Text.StringBuilder
            Dim exb As Exception = ex.InnerException
            If m_debug Then
                sb.Append(ex.GetType().Name)
                sb.Append(": ")
            End If
            Dim m As String = ""
            If TypeOf ex Is BW2Exception Then
                m = CType(ex, BW2Exception).msg
            Else
                m = ex.Message
            End If
            If Not m_translator Is Nothing Then
                Try
                    m = CStr(m_translator.GetCaptionByKey(m))
                Catch
                End Try
            End If
            sb.Append(m)

            'sb.Append("")
            If Not exb Is Nothing Then
                If Not m_translator Is Nothing Then
                    Try
                        m = CStr(m_translator.GetCaptionByKey("InnerException"))
                    Catch
                        m = " {"
                    End Try
                    sb.Append(vbCrLf)
                    sb.Append(vbCrLf)
                    sb.Append(m)
                    sb.Append(vbCrLf)
                    ToString(exb, sb)
                    If m = " {" Then
                        sb.Append(" }")
                    End If
                    sb.Append(vbCrLf)
                Else
                    sb.Append(vbCrLf)
                    sb.Append(" {")
                    ToString(exb, sb)
                    sb.Append(vbCrLf)
                    sb.Append("} ")
                End If

            ElseIf m_debug Then
                sb.Append(vbCrLf)
                sb.Append(ex.StackTrace)
                sb.Append(vbCrLf)
            End If
            Return sb
        End Function

        ' get the chain of exceptions as a readable string. the innermost exception is dumped as stacktrace.
        <System.Diagnostics.DebuggerStepThrough()> Private Function CreateString() As String
            Dim sb As New System.Text.StringBuilder
            ToString(Me, sb)
            Return sb.ToString()
        End Function

        ' get the inner exception as an error
        <System.Diagnostics.DebuggerStepThrough()> Public Function ToErrorString() As String
            Dim ex As Exception = Me.GetBaseException
            If m_debug Then
                Return ex.GetType().Name & ": " & ex.Message
            End If
            Return ex.Message
        End Function

        Public Overrides ReadOnly Property Message() As String
            Get
                Return ReplaceTranslations(CreateString())
            End Get
        End Property

        Private Function ReplaceTranslations(ByVal s As String) As String
            Dim StartIndex As Integer = 1
            Dim EndIndex As Integer = 1

            Dim RetVal As String = s
            Dim Key As String

            Do Until (Not StartIndex > 0) OrElse (Not EndIndex > 0)
                StartIndex = InStr(EndIndex, s, "[{")
                If StartIndex > 0 Then
                    EndIndex = InStr(StartIndex, s, "}]")

                    Key = s.Substring(StartIndex + 1, EndIndex - StartIndex - 2)

                    RetVal = Replace(s, "[{" & Key & "}]", m_translations(Key.ToLower))
                End If
            Loop

            Return RetVal
        End Function

        Private m_translations As New Specialized.StringDictionary

        Default Public Property TranslationItem(ByVal key As String) As String
            Get
                Dim s As String = m_translations(key.ToLower)
                If s Is Nothing Then s = ""
                Return s
            End Get
            Set(ByVal Value As String)
                m_translations(key.ToLower) = Value
            End Set
        End Property

    End Class

    ' The Server Exception class
    Public Class BW2ServerException
        Inherits BW2Exception

        Public Sub New(ByVal name As String, Optional ByVal mark As Object = "")
            MyBase.New(name)
            mymsg = name
            Me.Mark = mark
        End Sub

        Public Sub New(ByVal name As String, ByVal ex As Exception, Optional ByVal mark As Object = "")
            MyBase.New(name, ex)
            mymsg = name
            Me.Mark = mark
        End Sub

        Public Property Mark() As Object
            Get
                Return m_mark
            End Get
            Set(ByVal value As Object)
                m_mark = value
                If Not m_mark Is Nothing AndAlso m_mark.ToString <> "" Then
                    msg = mymsg & " [" & m_mark.ToString & "]"
                Else
                    msg = mymsg
                End If
            End Set
        End Property

        Public Property SqlErrorCode() As Integer
            Get
                Return m_sqlerrorcode
            End Get
            Set(ByVal Value As Integer)
                m_sqlerrorcode = Value
            End Set
        End Property

        Private m_mark As Object
        Private m_sqlerrorcode As Integer
        Private mymsg As String

    End Class

    ' The IO Exception class
    Public Class BW2IOException
        Inherits BW2Exception

        Public Sub New(ByVal name As String)
            MyBase.New(name)
        End Sub

        Public Sub New(ByVal name As String, ByVal ex As Exception)
            MyBase.New(name, ex)
        End Sub
    End Class

    ' The Security Exception class
    Public Class BW2SecurityException
        Inherits BW2Exception

        Public Sub New(ByVal name As String)
            MyBase.New(name)
        End Sub

        Public Sub New(ByVal name As String, ByVal ex As Exception)
            MyBase.New(name, ex)
        End Sub
    End Class


    Public Module BW2GlobalModule
        Public Function CDBStr(ByVal value As Decimal) As String
            Return value.ToString(Globalization.NumberFormatInfo.InvariantInfo)
        End Function
        Public Function CDBStr(ByVal value As Double) As String
            Return value.ToString(Globalization.NumberFormatInfo.InvariantInfo)
        End Function
        Public Function CDBStr(ByVal value As Long) As String
            Return value.ToString(Globalization.NumberFormatInfo.InvariantInfo)
        End Function
        Public Function CDBStr(ByVal value As Integer) As String
            Return value.ToString(Globalization.NumberFormatInfo.InvariantInfo)
        End Function
        Public Function CDBStr(ByVal value As Boolean) As String
            If value Then
                Return "1"
            Else
                Return "0"
            End If
        End Function
        Public Function CDBStr(ByVal value As String) As String
            Return "'" & Replace(value, "'", "''") & "'"
        End Function

        Public Function CDBStr(ByVal obj As Object) As String
            If obj Is Nothing Then
                Return "NULL"
            ElseIf TypeOf obj Is IFormattable Then
                Return CType(obj, IFormattable).ToString(Nothing, Globalization.NumberFormatInfo.InvariantInfo)
            Else
                Return CDBStr(obj.ToString)
            End If
        End Function

    End Module

    ' the date types supported by BW2
    Public Enum DataTypeEnum As Byte
        tLong
        tInteger
        tShort
        tByte
        tBoolean
        tDouble
        tDecimal
        tDate
        tString
        tByteArray
        tUniqueID
    End Enum


    ' storage restrictions 
    Public Enum StorageRestriction As Byte
        Unrestricted = 0
        LeadingStorage = 1
        NonLeadingStorage = 2
    End Enum

    ' dbscript check type 
    Public Enum CheckType As Byte
        NonEmpty = 0
        Empty = 1
        CompareValue = 2
    End Enum

    ' dbscript check type 
    Public Enum DBCondition As Byte
        '        None = 0
        NonEmpty = 1
        Empty = 2
        OneRow = 3
        CompareValue = 4
    End Enum

    ' database mutation type
    Public Enum DBMutationType As Byte
        Create = 0
        Change = 1
        Delete = 2
    End Enum

    ' track mutation type
    Public Enum TrackMutationType As Byte
        None = 0
        NoTracking = 1
        LastMutationOnly = 2
        Version = 3
        Mutation = 4
        Historize = 5
    End Enum

    ' Server Events
    Public Enum ServerEventEnum As Integer
        LockEvent = 1
        UnlockEvent = 2

        LoginEvent = 3
        LogoutEvent = 4

        ShuttingDownEvent = 6
        ImmediateShutdownEvent = 7
        StartingUpEvent = 8
        UpAndRunningEvent = 9

        CreateEvent = 10
        UpdateEvent = 11
        DeleteEvent = 12

        ParentPropertyChangedEvent = 13
        RefreshRelatedItemEvent = 14
        HierarchyChangedEvent = 15

        ShuttingDownEvent_SaveAndClose = 16

        AsyncNotificationEvent = 18

        OpenItemNotification = 20
        BrowseNotification = 21

        QueryBrowser = 22
        QueryBrowserResponse = 23

        InstantMessageNotification = 25

        ItemReadEvent = 32
        ItemUnreadEvent = 33

        CustomBroadcastNotification = 512
        LastCustomBroadcastNotification = CustomSpecificUpdateEvent - 1

        CustomSpecificUpdateEvent = 1024 ' will be filtered for user
        LastCustomSpecificUpdateEvent = CustomSpecificNotification - 1

        CustomSpecificNotification = 4 * 1024
        LastCustomSpecificNotification = CustomNotification - 1

        CustomNotification = 8 * 1024
        LastCustomNotification = 32 * 1024 - 1

        ServiceNotificationOut = 32 * 1024

        ServiceNotificationOutMessage = ServerEventEnum.ServiceNotificationOut + 0

        LastServiceNotificationOut = ServiceNotificationOut + 1024 - 1

        ServiceNotificationIn = 32 * 1024 + 1024

        ServiceNotificationInMessage = ServerEventEnum.ServiceNotificationIn + 0

        LastServiceNotificationIn = ServiceNotificationIn + 1024 - 1


    End Enum



    Public Class StorageGroupInfo

        Public Sub New(ByVal connectString As String)
            m_connstring = connectString
        End Sub

        Public ReadOnly Property IsSQLServer() As Boolean
            Get
                Return True
            End Get
        End Property

        Protected m_connstring As String
    End Class

    Public Class Session

    End Class



    Public Class User
        Public Property Name() As String
            Get
                Return CStr(Prop("name"))
            End Get
            Set(ByVal Value As String)
                m_prop("name") = Value
            End Set
        End Property

        Public Property Prop(ByVal name As String) As Object
            Get
                Return m_prop(name.ToLower)
            End Get
            Set(ByVal Value As Object)
                m_prop(name.ToLower) = Value
            End Set
        End Property

        Private m_prop As New Hashtable
    End Class

    Public Interface LogWriter
        Sub log(ByVal s As String)
        Sub logError(ByVal s As String)
    End Interface

    Public Class Application

        ' Public Delegate Sub LogEventHandler(ByVal line As String)
        'Public Shared Event LogEvent As LogEventHandler
        Public Shared Event LogEvent(ByVal line As String)
        Public Shared Event LogErrorEvent(ByVal line As String)

        Protected Shared m_StartupPath As String = "./"
        Protected Shared m_logger As LogWriter = Nothing

        Public Shared ReadOnly Property StartupPath() As String
            Get
                Return m_StartupPath
            End Get
        End Property

        Public Shared Sub log(ByVal s As String)
            If Not m_logger Is Nothing Then
                m_logger.log(s)
            End If
            RaiseEvent LogEvent(s)
        End Sub

        Public Shared Sub logError(ByVal s As String)
            If Not m_logger Is Nothing Then
                m_logger.logError(s)
            End If
            RaiseEvent LogErrorEvent(s)
        End Sub

        Protected Friend Property ProtectedData(ByVal name As String) As Object
            Get
                Return m_prop(name.ToLower)
            End Get
            Set(ByVal Value As Object)
                m_prop(name.ToLower) = Value
            End Set
        End Property

        Private m_prop As New Hashtable

    End Class


    Public Class FileTools

        Public Shared Sub AssertPathExists(ByVal path As String, Optional ByVal isFilePath As Boolean = False)
            If isFilePath Then
                Dim fi As New IO.FileInfo(path)
                If Not fi.Directory.Exists Then
                    fi.Directory.Create()
                End If
            ElseIf Not IO.Directory.Exists(path) Then
                IO.Directory.CreateDirectory(path)
            End If
        End Sub

    End Class

End Namespace
