Option Strict Off
Option Explicit On

Imports System.IO
Imports System.Text
Imports System.Security.Cryptography

Namespace BW2Global.Security

    Public Enum EncryptionMethod
        None
        RC2_128Bit
    End Enum

    Public Enum CompressionMethod
        None
        DeflateCompression
    End Enum

    Public Enum MessageAuthenticationMethod
        None
        SHA1
    End Enum

    <DebuggerStepThrough()> Public Class CryptoProvider
        Private Shared m_PublicKey() As Byte = New Byte() {115, 37, 210, 20, 67, 98, 123, 43, 234, 167, 211, 98, 93, 85, 23, 111}
        Public Shared MinCompressionSize As Integer = 100

        'Declare Function Compress Lib "zlib.dll" Alias "compress" (ByRef Dest As Byte, ByRef DestLen As Integer, ByRef Source As Byte, ByVal SourceLen As Integer) As Integer
        'Declare Function DeCompress Lib "zlib.dll" Alias "uncompress" (ByRef Dest As Byte, ByRef DestLen As Integer, ByRef Source As Byte, ByVal SourceLen As Integer) As Integer

        Function Compress(ByRef Dest As Byte(), ByRef DestLen As Integer, ByRef Source As Byte(), ByVal SourceLen As Integer) As Integer
            Using ms As New IO.MemoryStream(SourceLen)
                Using x As New System.IO.Compression.DeflateStream(ms, IO.Compression.CompressionMode.Compress, True)
                    x.Write(Source, 0, SourceLen)
                    x.Flush()
                    x.Close()
                End Using
                ms.Write(BitConverter.GetBytes(SourceLen), 0, 4)
                Dest = ms.ToArray
                DestLen = Dest.Length
                Return DestLen
            End Using
        End Function

        Function DeCompress(ByRef Dest As Byte(), ByRef DestLen As Integer, ByRef Source As Byte(), ByVal SourceLen As Integer) As Integer
            Using ms As New IO.MemoryStream(Source, 0, SourceLen, False)
                Using x As New System.IO.Compression.DeflateStream(ms, IO.Compression.CompressionMode.Decompress)
                    Return x.Read(Dest, 0, DestLen)
                End Using
            End Using
        End Function

#Region "RC4"

        Public Class RC4
            '/*=============================================================================================================================*/
            '/*                                  � 2004-2005 by HP. Lassnig, HyPeWare
            '/*=============================================================================================================================*/
            '/*
            '/* Description :   Crypting Tools  
            '/*
            '/* Remarks     :   32-Bit Version
            '/*
            '/* Programming :   HP. Lassnig
            '/*
            '/*=============================================================================================================================*/
            '/* Version:    Date:       Name:           Comments:
            '/*-----------------------------------------------------------------------------------------------------------------------------*/
            '/* 1           03.01.2001  HP. Lassnig
            '/* 3           05.11.2001  HP. Lassnig     Portierung f�r orell f�ssli Sphinx-Prototyp
            '/* 4           14.08.2002  HP. Lassnig     Portierung in .NET
            '/*
            '/*=============================================================================================================================*/

            '/*-----------------------------------------------------------------------------------------------------------------------------*/
            '/*  <Theme> Private Objekte
            '/*-----------------------------------------------------------------------------------------------------------------------------*/

            '/*-----------------------------------------------------------------------------------------------------------------------------*/
            '/*  <Theme> Private Konstanten, Variabeln und Definitionen
            '/*-----------------------------------------------------------------------------------------------------------------------------*/

            '/*--- RC4 Variabeln ---*/
            Private strRC4_Key As String
            Private strRC4_Data As String

            '/*-----------------------------------------------------------------------------------------------------------------------------*/
            '/*  <Theme> Public Enums
            '/*-----------------------------------------------------------------------------------------------------------------------------*/

            '/*--- RC4 Errors ---*/
            Public Enum enRC4_Errors
                errRC4none = 0
                errRC4noData = 10001
                errRC4noKey = 10002
            End Enum

            '/*=============================================================================================================================*/
            '/* RC4 Stream Cipher Algorithmus (1987 by Ronald Rivest)
            '/*=============================================================================================================================*/
            '/*
            '/* RC4 is a stream cipher designed by Rivest for RSA Data Security (now RSA Security).
            '/* It is a variable key-size stream cipher with byte-oriented operations. The algorithm is based on
            '/* the use of a random permutation. Analysis shows that the period of the cipher is overwhelmingly
            '/* likely to be greater than 10^100. Eight to sixteen machine operations are required per output byte,
            '/* and the cipher can be expected to run very quickly in software. Independent analysts have
            '/* scrutinized the algorithm and it is considered secure.
            '/*
            '/* RC4 is used for file encryption in products such as RSA SecurPC. It is also used for secure
            '/* communications, as in the encryption of traffic to and from secure web sites using the SSL protocol.
            '/*
            '/*=============================================================================================================================*/
            '/* <Method> Encrypt Data mit RC4 Algorithmus
            '/*=============================================================================================================================*/
            Public Function EncryptRC4Value(ByVal strData As String, ByVal strKey As String, Optional ByRef eErr As RC4.enRC4_Errors = enRC4_Errors.errRC4none) As String
                eErr = EncryptRC4(strData, strKey)
                EncryptRC4Value = strData
            End Function

            Public Function EncryptRC4(ByRef strData As String, ByVal strKey As String) As RC4.enRC4_Errors

                '/*--- <FEHLER> Schl�ssel/Daten nicht vorhanden ---*/
                If strData = "" Then EncryptRC4 = enRC4_Errors.errRC4noData
                If strKey = "" Then EncryptRC4 = enRC4_Errors.errRC4noKey

                '/*--- Variabeln & Definitionen ---*/
                On Error Resume Next

                strRC4_Data = strData
                strRC4_Key = strKey

                '/*--- Encrypt Data ---*/
                Call RC4_Initialize()
                Call RC4_DoXor()
                Call RC4_Stretch()

                '/*--- ReturnValue ---*/
                strData = strRC4_Data
                EncryptRC4 = Err.Number

                On Error GoTo 0
            End Function

            '/*=============================================================================================================================*/
            '/* <Method> Decrypt Data mit RC4 Algorithmus
            '/*=============================================================================================================================*/
            Public Function DecryptRC4Value(ByVal strData As String, ByVal strKey As String, Optional ByRef eErr As RC4.enRC4_Errors = enRC4_Errors.errRC4none) As String
                eErr = DecryptRC4(strData, strKey)
                DecryptRC4Value = strData
            End Function

            Public Function DecryptRC4(ByRef strData As String, ByVal strKey As String) As RC4.enRC4_Errors

                '/*--- <FEHLER> Schl�ssel/Daten nicht vorhanden ---*/
                If strData = "" Then DecryptRC4 = enRC4_Errors.errRC4noData
                If strKey = "" Then DecryptRC4 = enRC4_Errors.errRC4noKey

                '/*--- Variabeln & Definitionen ---*/
                On Error Resume Next

                strRC4_Data = strData
                strRC4_Key = strKey

                '/*--- Encrypt Data ---*/
                Call RC4_Initialize()
                Call RC4_Shrink()
                Call RC4_DoXor()

                '/*--- ReturnValue ---*/
                strData = strRC4_Data
                DecryptRC4 = Err.Number

                On Error GoTo 0
            End Function

            '/*=============================================================================================================================*/
            '/* Property    ->  RC4 Schl�ssel
            '/*=============================================================================================================================*/
            Private WriteOnly Property RC4_KeyString() As String
                Set(ByVal Value As String)
                    strRC4_Key = Value
                    Call RC4_Initialize()
                End Set
            End Property

            '/*=============================================================================================================================*/
            '/* Property    ->  RC4 ENCRYPTED/DECRYPTED Data
            '/*=============================================================================================================================*/
            Private Property RC4_Data() As String
                Get
                    RC4_Data = strRC4_Data
                End Get

                '/*-----------------------------------------------------------------------------------------------------------------------------*/

                Set(ByVal Value As String)
                    strRC4_Data = Value
                End Set
            End Property

            '/*=============================================================================================================================*/
            '/* <Method> RC4 Initializes random numbers using the key string
            '/*=============================================================================================================================*/
            Private Sub RC4_Initialize()

                '/*--- Variabeln & Definitionen ---*/
                Dim lN As Integer

                Randomize(Rnd(-1))

                For lN = 1 To Len(strRC4_Key)
                    Randomize(Rnd(-Rnd() * Asc(Mid(strRC4_Key, lN, 1))))
                Next lN
            End Sub

            '/*=============================================================================================================================*/
            '/* <Method> RC4 Exclusive-or method to encrypt or decrypt
            '/*=============================================================================================================================*/
            Private Sub RC4_DoXor()

                '/*--- Variabeln & Definitionen ---*/
                On Error GoTo Exit_DoXor
                Dim lC, lB, lN As Integer

                For lN = 1 To Len(strRC4_Data)
                    lC = Asc(Mid(strRC4_Data, lN, 1))
                    lB = Int(Rnd() * 256)
                    Mid(strRC4_Data, lN, 1) = Chr(lC Xor lB)
                Next lN

Exit_DoXor:
            End Sub

            '/*=============================================================================================================================*/
            '/* <Method> RC4 Convert any string to a printable, displayable string
            '/*=============================================================================================================================*/
            Private Sub RC4_Stretch()

                '/*--- Variabeln & Definitionen ---*/
                On Error GoTo Exit_Stretch
                Dim lA As Integer
                Dim lC As Integer
                Dim lJ As Integer
                Dim lK As Integer
                Dim lN As Integer
                Dim strB As String

                lA = Len(strRC4_Data)
                strB = Space(lA + (lA + 2) \ 3)

                For lN = 1 To lA
                    lC = Asc(Mid(strRC4_Data, lN, 1))
                    lJ = lJ + 1
                    Mid(strB, lJ, 1) = Chr((lC And 63) + 59)

                    Select Case lN Mod 3
                        Case 1
                            lK = lK Or ((lC \ 64) * 16)
                        Case 2
                            lK = lK Or ((lC \ 64) * 4)
                        Case 0
                            lK = lK Or (lC \ 64)
                            lJ = lJ + 1
                            Mid(strB, lJ, 1) = Chr(lK + 59)
                            lK = 0
                    End Select
                Next lN

                If lA Mod 3 Then
                    lJ = lJ + 1
                    Mid(strB, lJ, 1) = Chr(lK + 59)
                End If

                '/*--- ReturnValue ---*/
                strRC4_Data = strB

Exit_Stretch:
            End Sub

            '/*=============================================================================================================================*/
            '/* <Method> RC4 Inverse of the Stretch method / result can contain any of the 256-byte values
            '/*=============================================================================================================================*/
            Private Sub RC4_Shrink()

                '/*--- Variabeln & Definitionen ---*/
                On Error GoTo Exit_Shrink
                Dim lA As Integer
                Dim lB As Integer
                Dim lC As Integer
                Dim lD As Integer
                Dim lE As Integer
                Dim lJ As Integer
                Dim lK As Integer
                Dim lN As Integer
                Dim strB As String

                lA = Len(strRC4_Data)
                lB = lA - 1 - (lA - 1) \ 4
                strB = Space(lB)

                For lN = 1 To lB
                    lJ = lJ + 1
                    lC = Asc(Mid(strRC4_Data, lJ, 1)) - 59

                    Select Case lN Mod 3
                        Case 1
                            lK = lK + 4
                            If lK > lA Then lK = lA
                            lE = Asc(Mid(strRC4_Data, lK, 1)) - 59
                            lD = ((lE \ 16) And 3) * 64
                        Case 2
                            lD = ((lE \ 4) And 3) * 64
                        Case 0
                            lD = (lE And 3) * 64
                            lJ = lJ + 1
                    End Select

                    Mid(strB, lN, 1) = Chr(lC Or lD)
                Next lN

                '/*--- ReturnValue ---*/
                strRC4_Data = strB

Exit_Shrink:
            End Sub

        End Class

#End Region

        <DebuggerHidden()> Public Shared Function PublicKey() As Byte()
            Return m_PublicKey
        End Function
        <DebuggerHidden()> Public Function GetRandomBytes(ByVal Count As Integer) As Byte()
            Dim r As New RNGCryptoServiceProvider
            Dim b(Count - 1) As Byte
            r.GetBytes(b)
            Return b
        End Function

        <DebuggerHidden()> Public Overloads Function GetMAC(ByVal s As String) As Byte()
            Dim enc As New UnicodeEncoding
            Return GetMAC(enc.GetBytes(s))
        End Function
        <DebuggerHidden()> Public Overloads Function GetMAC(ByVal s As String, ByVal Key() As Byte) As Byte()
            Dim enc As New UnicodeEncoding
            Return GetMAC(enc.GetBytes(s), Key)
        End Function
        <DebuggerHidden()> Public Overloads Function GetMAC(ByVal s As String, ByVal Key As String) As Byte()
            Dim enc As New UnicodeEncoding
            Return GetMAC(enc.GetBytes(s), enc.GetBytes(Key))
        End Function

        <DebuggerHidden()> Public Overloads Function GetMAC(ByVal b() As Byte) As Byte()
            Return GetMAC(b, 0, b.Length)
        End Function

        <DebuggerHidden()> Public Overloads Function GetMAC(ByVal b() As Byte, ByVal Start As Integer, ByVal Length As Integer) As Byte()
            Dim bKey() As Byte = {136, 65, 250, 9, 13, 63, 45, 141, 214, 190, 68, 75, 12, 163, 99, 87, 16, 96, 34, 74, 212, 178}
            Return GetMAC(b, Start, Length, bKey)
        End Function

        <DebuggerHidden()> Public Overloads Function GetMAC(ByVal b() As Byte, ByVal Key As String) As Byte()
            Return GetMAC(b, 0, b.Length, Key)
        End Function

        <DebuggerHidden()> Public Overloads Function GetMAC(ByVal b() As Byte, ByVal Start As Integer, ByVal Length As Integer, ByVal Key As String) As Byte()
            Dim ms As New MemoryStream, bw As New BinaryWriter(ms), bKey() As Byte
            bw.Write(Key) : bKey = ms.ToArray : ms.Close() : bw.Close()
            Return GetMAC(b, Start, Length, bKey)
        End Function

        <DebuggerHidden()> Public Overloads Function GetMAC(ByVal b() As Byte, ByVal Key() As Byte) As Byte()
            Return GetMAC(b, 0, b.Length, Key)
        End Function

        <DebuggerHidden()> Public Overloads Function GetMAC(ByVal b() As Byte, ByVal Start As Integer, ByVal Length As Integer, ByVal Key() As Byte) As Byte()
            ReDim Preserve Key(63)
            Dim sha As New HMACSHA1(Key)
            sha.ComputeHash(b, Start, Length)
            Return sha.Hash
        End Function

        <DebuggerHidden()> Public Overloads Function Encrypt(ByVal b() As Byte, ByVal Key As String, ByVal Encryption As EncryptionMethod, ByVal Compression As CompressionMethod, ByVal Authentication As MessageAuthenticationMethod) As Byte()
            Return Encrypt(b, Key, 0, Encryption, Compression, Authentication)
        End Function

        <DebuggerHidden()> Public Overloads Function Encrypt(ByVal b() As Byte, ByVal Key As String, ByVal Seed As Integer, ByVal Encryption As EncryptionMethod, ByVal Compression As CompressionMethod, ByVal Authentication As MessageAuthenticationMethod) As Byte()
            Dim enc As New UnicodeEncoding
            Return Encrypt(b, enc.GetBytes(Key), Seed, Encryption, Compression, Authentication)
        End Function

        <DebuggerHidden()> Public Overloads Function Encrypt(ByVal b() As Byte, ByVal Key() As Byte, ByVal Encryption As EncryptionMethod, ByVal Compression As CompressionMethod, ByVal Authentication As MessageAuthenticationMethod) As Byte()
            Return Encrypt(b, Key, 0, Encryption, Compression, Authentication)
        End Function

        <DebuggerHidden()> Public Overloads Function Encrypt(ByVal s As String, ByVal Key As String, ByVal Encryption As EncryptionMethod, ByVal Compression As CompressionMethod, ByVal Authentication As MessageAuthenticationMethod) As String
            Dim enc As New UnicodeEncoding
            Dim b() As Byte = enc.GetBytes(s)
            b = Encrypt(b, Key, 0, Encryption, Compression, Authentication)
            Return Convert.ToBase64String(b)
        End Function

        <DebuggerHidden()> Public Overloads Function Encrypt(ByVal b() As Byte, ByVal Key() As Byte, ByVal Seed As Integer, ByVal Encryption As EncryptionMethod, ByVal Compression As CompressionMethod, ByVal Authentication As MessageAuthenticationMethod) As Byte()
            Dim bHK1(63) As Byte, i As Integer
            Dim bT() As Byte

            'Mask Key to 512 Bit
            Dim shaM As New SHA512Managed
            Key = shaM.ComputeHash(Key)

            'Compress
            Try
                Select Case Compression
                    Case CompressionMethod.None

                    Case CompressionMethod.DeflateCompression
                        If b.Length > MinCompressionSize Then
                            Dim BufferSize As Integer = b.Length
                            Dim bLen() As Byte = BitConverter.GetBytes(BufferSize)

                            BufferSize = BufferSize + CInt(BufferSize * 0.01) + 12
                            Dim TempBuffer() As Byte = Nothing
                            'Compress byte array (data)
                            Dim RetVal As Integer
                            RetVal = Compress(TempBuffer, BufferSize, b, b.Length)

                            b = TempBuffer
                            'Array.Copy(bLen, 0, b, b.GetUpperBound(0) - 3, 4)

                        Else
                            Compression = CompressionMethod.None
                        End If

                    Case Else
                        Throw New ArgumentException("invalid value for parameter 'Compression'")

                End Select
            Catch e As Exception
                Throw New BW2Exception("error in compressing data", e)
            End Try

            'Encryption
            Try
                If Encryption <> EncryptionMethod.None Then
                    Dim alg As SymmetricAlgorithm
                    Dim bKey() As Byte, bIV() As Byte

                    bKey = Key
                    bIV = BitConverter.GetBytes(Seed + 7)

                    Select Case Encryption
                        Case EncryptionMethod.RC2_128Bit
                            alg = SymmetricAlgorithm.Create("RC2")
                            ReDim Preserve bKey(15)
                            ReDim Preserve bIV(15)
                        Case Else
                            alg = SymmetricAlgorithm.Create("RC2")
                            ReDim Preserve bKey(15)
                            ReDim Preserve bIV(15)
                    End Select

                    Dim sse As ICryptoTransform = alg.CreateEncryptor(bKey, bIV)
                    b = sse.TransformFinalBlock(b, 0, b.Length)
                End If
            Catch e As Exception
                Throw New BW2Exception("error in encrypting data", e)
            End Try

            'AddMAC
            Select Case Authentication
                Case MessageAuthenticationMethod.None
                    ReDim bT(2)
                Case MessageAuthenticationMethod.SHA1
                    Array.Copy(Key, bHK1, 64)
                    Dim sha As New HMACSHA1(bHK1)
                    bT = sha.ComputeHash(b)
                    ReDim Preserve bT(bT.Length + 2)
                Case Else
                    Throw New ArgumentException("invalid value for parameter 'Authentication'")
            End Select


            'AddInfos
            i = b.Length
            ReDim Preserve b(b.Length + bT.Length - 1)
            Array.Copy(bT, 0, b, i, bT.Length)
            i = b.Length
            b(i - 3) = CByte(Encryption)
            b(i - 2) = CByte(Compression)
            b(i - 1) = CByte(Authentication)
            Return b


        End Function


        Private Function MaskKey(ByVal Key() As Byte) As Byte()
            Dim bHK() As Byte = {136, 65, 65, 255, 9, 13, 64, 45, 141, 214, 190, 68, 75, 75, 12, 163, 255, 9, 13, 64, 45, 141, 214, 12, 163, 255, 9, 13, 64, 45, 141, 214, 190, 68, 75, 12, 163, 99, 87, 16, 96, 34, 74, 212, 178}
            Dim bT() As Byte, iLength As Integer
            Dim sha As New HMACSHA1(bHK)
            sha.ComputeHash(Key)
            bT = (sha.Hash)
            ReDim Preserve bT(bT.Length + Key.Length - 1)
            Array.Copy(Key, bT, Key.Length)
            iLength = bT.Length
            ReDim Preserve bT(127)
            If iLength < 128 Then
                Array.Copy(bHK, 0, bT, iLength, Math.Min(bHK.Length, 128 - iLength))
            End If
            Return bT
        End Function

        <DebuggerHidden()> Public Overloads Function Decrypt(ByVal b() As Byte, ByVal Key As String) As Byte()
            Return Decrypt(b, Key, 0)
        End Function

        <DebuggerHidden()> Public Overloads Function Decrypt(ByVal b() As Byte, ByVal Key As String, ByVal Seed As Integer) As Byte()
            Dim enc As New UnicodeEncoding
            Return Decrypt(b, enc.GetBytes(Key), Seed)
        End Function

        <DebuggerHidden()> Public Overloads Function Decrypt(ByVal b() As Byte, ByVal Key() As Byte) As Byte()
            Return Decrypt(b, Key, 0)
        End Function

        <DebuggerHidden()> Public Overloads Function Decrypt(ByVal s As String, ByVal Key As String) As String
            Dim b() As Byte = Convert.FromBase64String(s)
            b = Decrypt(b, Key, 0)
            Dim enc As New UnicodeEncoding
            Return enc.GetString(b)
        End Function

        <DebuggerHidden()> Public Overloads Function Decrypt(ByVal b() As Byte, ByVal Key() As Byte, ByVal Seed As Integer) As Byte()
            Dim bHK1(63) As Byte, i As Integer, n As Integer
            Dim bT() As Byte
            Dim Encryption As EncryptionMethod, Compression As CompressionMethod, Authentication As MessageAuthenticationMethod

            'GetInfos
            i = b.Length
            Try
                Encryption = CType(b(i - 3), EncryptionMethod)
            Catch
                Throw New Exception("invalid encryption method")
            End Try
            Try
                Compression = CType(b(i - 2), CompressionMethod)
            Catch
                Throw New Exception("invalid compression method")
            End Try
            Try
                Authentication = CType(b(i - 1), MessageAuthenticationMethod)
            Catch
                Throw New Exception("invalid authentication method")
            End Try


            'Mask Key to 512 Bit
            Dim shaM As New SHA512Managed
            Key = shaM.ComputeHash(Key)


            'Check MAC
            n = b.Length
            Select Case Authentication
                Case MessageAuthenticationMethod.None
                    ReDim Preserve b(n - 4)
                Case MessageAuthenticationMethod.SHA1
                    Array.Copy(Key, bHK1, 64)
                    Dim sha As New HMACSHA1(bHK1)
                    bT = sha.ComputeHash(b, 0, n - 23)
                    For i = 0 To 19
                        If b(n - 23 + i) <> bT(i) Then
                            Throw New Exception("Invalid Hashcode")
                        End If
                    Next i
                    ReDim Preserve b(n - 24)
            End Select

            'Decryption
            Try
                If Encryption <> EncryptionMethod.None Then
                    Dim alg As SymmetricAlgorithm
                    Dim bKey() As Byte, bIV() As Byte

                    bKey = Key
                    bIV = BitConverter.GetBytes(Seed + 7)

                    Select Case Encryption

                        Case EncryptionMethod.RC2_128Bit
                            alg = SymmetricAlgorithm.Create("RC2")
                            ReDim Preserve bKey(15)
                            ReDim Preserve bIV(15)

                        Case Else
                            Throw New Exception("unknown Decryption")

                    End Select

                    Dim ssd As ICryptoTransform = alg.CreateDecryptor(bKey, bIV)
                    b = ssd.TransformFinalBlock(b, 0, b.Length)
                End If
            Catch e As Exception
                Throw New BW2Exception("error in decrypting data")
            End Try

            'Decompress
            Try
                Select Case Compression
                    Case CompressionMethod.None

                    Case CompressionMethod.DeflateCompression
                        Dim BufferSize As Integer = BitConverter.ToInt32(b, b.GetUpperBound(0) - 3)

                        BufferSize = BufferSize + CInt(BufferSize * 0.01) + 12

                        Dim TempBuffer(BufferSize - 1) As Byte

                        'Decompress data
                        Dim RetVal As Integer
                        RetVal = DeCompress(TempBuffer, BufferSize, b, b.Length - 4)

                        ReDim Preserve TempBuffer(BufferSize - 1)
                        b = TempBuffer

                End Select
            Catch e As Exception
                Throw New BW2Exception("error in decompressing data")
            End Try

            Return b

        End Function

        <DebuggerHidden()> Public Shared Sub EncryptRC4(ByRef ByteArray() As Byte, ByVal sKey As String)

            Dim i As Integer, j As Integer, Temp As Byte, Offset As Integer, OrigLen As Integer
            Dim CipherLen As Integer
            Dim sBox(255) As Byte

            'For Temp = 0 To 255
            '    sBox(Temp) = Temp
            'Next

            PREPARE_RC4_KEY(sBox, sKey)

            'Get the size of the source array
            OrigLen = ByteArray.Length
            CipherLen = OrigLen

            'Encrypt the data
            For Offset = 0 To (OrigLen - 1)
                i = (i + 1) Mod 256
                j = (j + sBox(i)) Mod 256
                Temp = sBox(i)
                sBox(i) = sBox(j)
                sBox(j) = Temp
                ByteArray(Offset) = ByteArray(Offset) Xor (sBox((sBox(i) + sBox(j)) Mod 256))
            Next

        End Sub


        Private Shared Sub PREPARE_RC4_KEY(ByRef sBox() As Byte, ByVal sKEY As String)
            ' Take Variable Length Key and Set the RC4 State.
            '   This procedure should be run once before
            '   each encryption/decryption session.
            Dim temp As Byte
            Dim rc4key(255) As Byte
            For temp = 0 To 255
                sBox(temp) = temp
            Next
            Dim k As Integer = Len(sKEY)
            Dim i As Integer
            Dim j As Integer
            For i = 0 To k - 1
                Dim b As Byte = CByte(Asc(sKEY.Chars(i + 1)) And 255)
                For j = i To 255 Step k
                    rc4key(j) = b
                Next j
            Next i

            j = 0
            For i = 0 To 255
                k = (sBox(i) + rc4key(i)) Mod 256
                j = (j + k) Mod 256
                Dim b As Byte = sBox(i)
                sBox(i) = sBox(j)
                sBox(j) = b
            Next i
        End Sub


    End Class

End Namespace