Option Strict On
Option Explicit On 

Imports System.IO

Namespace BW2Global

    Public Structure ServerEvent
        Public ServerEvent_ID As Long
        Public ServerEvent_Type_ID As Integer
        Public ServerEvent_Item_ID As Long
        Public ServerEvent_Item_BOID As Long
        Public ServerEvent_Source_ID As Long
        Public ServerEvent_Destination_ID As Long
        Public ServerEvent_Date As Date
        Public ServerEvent_Filter_Text As String
        Public ServerEvent_Session_ID As Long
        Public ServerEvent_Data As String

        Default Public ReadOnly Property Item(ByVal index As Integer) As Object
            Get
                Select Case index
                    Case 0
                        Return ServerEvent_ID
                    Case 1
                        Return ServerEvent_Item_ID
                    Case 2
                        Return ServerEvent_Item_BOID
                    Case 3
                        Return ServerEvent_Source_ID
                    Case 4
                        Return ServerEvent_Destination_ID
                    Case 5
                        Return ServerEvent_Type_ID
                    Case 6
                        Return ServerEvent_Date
                    Case 7
                        Return ServerEvent_Filter_Text
                    Case 8
                        Return ServerEvent_Session_ID
                    Case 9
                        Return ServerEvent_Data
                    Case Else
                        Return 0
                End Select
            End Get
        End Property

        Public Sub WriteToStream(ByVal w As IO.BinaryWriter)
            w.Write(ServerEvent_ID)
            w.Write(ServerEvent_Type_ID)
            w.Write(ServerEvent_Item_ID)
            w.Write(ServerEvent_Item_BOID)
            w.Write(ServerEvent_Source_ID)
            w.Write(ServerEvent_Destination_ID)
            w.Write(ServerEvent_Date.Ticks)
            w.Write(ServerEvent_Filter_Text)
            w.Write(ServerEvent_Session_ID)
            If ServerEvent_Data Is Nothing Then
                w.Write("")
            Else
                w.Write(ServerEvent_Data)
            End If
        End Sub
        Public Sub ReadFromStream(ByVal r As IO.BinaryReader)
            ServerEvent_ID = r.ReadInt64
            ServerEvent_Type_ID = r.ReadInt32
            ServerEvent_Item_ID = r.ReadInt64
            ServerEvent_Item_BOID = r.ReadInt64
            ServerEvent_Source_ID = r.ReadInt64
            ServerEvent_Destination_ID = r.ReadInt64
            ServerEvent_Date = New Date(r.ReadInt64)
            ServerEvent_Filter_Text = r.ReadString
            ServerEvent_Session_ID = r.ReadInt64
            ServerEvent_Data = r.ReadString
        End Sub

    End Structure

    ' define some constants, interfaces for transportation and encapsulation of data
    Namespace Transport


        ' Response Exception is thrown from Message.OnResponse (called from Message.Request)
        Public Class BW2IOException
            Inherits BW2Exception

            Public Sub New(ByVal name As String)
                MyBase.New(name)
            End Sub
            Public Sub New(ByVal name As String, ByVal ex As Exception)
                MyBase.New(name, ex)
            End Sub
        End Class


        ' The set of message ids
        Public Enum MessageID As Byte
            LoginMessageID = 1
            LogoutMessageID = 2
            UpdateMessageID = 3
            CacheUpdateMessageID = 4
            ServerEventMessageID = 5

            DownloadMessageID = 6
            ServerRPCID = 7

            BatchMessageID = 10

            DBScriptMessageID = 20
        End Enum


        Public Class Converter
            Shared Sub ToWriter(ByVal bw As BinaryWriter, ByVal o As Object)
                If TypeOf o Is MemoryStream Then
                    bw.Write(DataTypeEnum.tByteArray)
                    Dim mem As MemoryStream = CType(o, MemoryStream)
                    bw.Write(mem.Length)
                    mem.WriteTo(bw.BaseStream)
                ElseIf TypeOf o Is Byte() Then
                    bw.Write(DataTypeEnum.tByteArray)
                    bw.Write(CType(o, Byte()).Length)
                    bw.Write(CType(o, Byte()))
                ElseIf TypeOf o Is Guid Then
                    bw.Write(DataTypeEnum.tUniqueID)
                    bw.Write(CType(o, Guid).ToByteArray)
                ElseIf TypeOf o Is String Then
                    bw.Write(DataTypeEnum.tString)
                    bw.Write(CStr(o))
                Else
                    bw.Write(DataTypeEnum.tString)
                    bw.Write(o.ToString())
                End If
            End Sub
            Shared Function FromReader(ByVal br As BinaryReader) As Object
                Dim b As Byte = br.ReadByte
                If b = DataTypeEnum.tByteArray Then
                    Dim len As Integer = br.ReadInt32
                    Return br.ReadBytes(len)
                ElseIf b = DataTypeEnum.tString Then
                    Return br.ReadString()
                ElseIf b = DataTypeEnum.tUniqueID Then
                    Return New Guid(br.ReadBytes(16))
                Else
                    Throw New BW2Exception("not yet implemented")
                End If
            End Function

            Public Shared Function ToArray(ByVal params As IDictionary) As Byte()
                Dim mem As New MemoryStream()
                Dim bw As New BinaryWriter(mem)
                Try
                    bw.Write(params.Count)
                    Dim e As IDictionaryEnumerator = params.GetEnumerator()
                    While e.MoveNext()
                        bw.Write(CStr(e.Key))
                        ToWriter(bw, e.Value)
                    End While
                    Return mem.ToArray()
                Finally
                    bw.Close()
                End Try
            End Function
            Public Shared Function FromReader(ByVal br As BinaryReader, ByVal dict As IDictionary) As IDictionary
                Dim n As Integer = br.ReadInt32()
                Dim params As New Specialized.ListDictionary()
                Dim i As Integer
                For i = 1 To n
                    Dim key As String = br.ReadString()
                    Dim o As Object = FromReader(br)
                    dict.Add(key, o)
                Next
                Return dict
            End Function

        End Class


    End Namespace

End Namespace
