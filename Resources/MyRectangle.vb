﻿Imports System
Imports System.Collections.Generic
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks

Namespace Resources
    Friend Class MyRectangle
        Private location As Point
        Private radius As Single
        Private grPath As GraphicsPath
        Private x As Single
        Private y As Single
        Private width As Single
        Private height As Single

        Public Sub New()
        End Sub

        Public Sub New(ByVal width As Single, ByVal height As Single, ByVal radius As Single, ByVal Optional x As Single = 0F, ByVal Optional y As Single = 0F)
            Me.location = New Point(0, 0)
            Me.radius = radius
            Me.x = x
            Me.y = y
            Me.height = height
            Me.width = width
            Me.grPath = New GraphicsPath()

            If radius <= 0F Then
                Me.grPath.AddRectangle(New RectangleF(x, y, width, height))
            Else
                Dim ef As RectangleF = New RectangleF(x, y, 1.0F * radius, 1.0F * radius)
                Dim ef2 As RectangleF = New RectangleF((width - (1.0F * radius)) - 1.0F, x, 1.0F * radius, 1.0F * radius)
                Dim ef3 As RectangleF = New RectangleF(x, (height - (1.0F * radius)) - 1.0F, 1.0F * radius, 1.0F * radius)
                Dim ef4 As RectangleF = New RectangleF((width - (1.0F * radius)) - 1.0F, (height - (1.0F * radius)) - 1.0F, 1.0F * radius, 1.0F * radius)
                Me.grPath.AddArc(ef, 180.0F, 90.0F)
                Me.grPath.AddArc(ef2, 270.0F, 90.0F)
                Me.grPath.AddArc(ef4, 0F, 90.0F)
                Me.grPath.AddArc(ef3, 90.0F, 90.0F)
                Me.grPath.CloseAllFigures()
            End If
        End Sub

        Public ReadOnly Property Path As GraphicsPath
            Get
                Return Me.grPath
            End Get
        End Property

        Public ReadOnly Property Rect As RectangleF
            Get
                Return New RectangleF(Me.x, Me.y, Me.width, Me.height)
            End Get
        End Property

    End Class
End Namespace

