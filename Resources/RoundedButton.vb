﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Drawing.Drawing2D

Namespace Resources
    Public Class RoundedButton
        Inherits Button

        Private _borderColor As Color
        Private _onHoverBorderColor As Color
        Private _buttonColor As Color
        Private _onHoverButtonColor As Color
        Private _textColor As Color
        Private _onHoverTextColor As Color
        Public button As Button = New Button()

        Public Sub New()
            DoubleBuffered = True
        End Sub

        Private Function GetRoundPath(ByVal Rect As RectangleF, ByVal radius As Integer) As GraphicsPath
            Dim m As Single = 2.75F
            Dim r2 As Single = radius / 2.0F
            Dim GraphPath As GraphicsPath = New GraphicsPath()
            GraphPath.AddArc(Rect.X + m, Rect.Y + m, radius, radius, 180, 90)
            GraphPath.AddLine(Rect.X + r2 + m, Rect.Y + m, Rect.Width - r2 - m, Rect.Y + m)
            GraphPath.AddArc(Rect.X + Rect.Width - radius - m, Rect.Y + m, radius, radius, 270, 90)
            GraphPath.AddLine(Rect.Width - m, Rect.Y + r2, Rect.Width - m, Rect.Height - r2 - m)
            GraphPath.AddArc(Rect.X + Rect.Width - radius - m, Rect.Y + Rect.Height - radius - m, radius, radius, 0, 90)
            GraphPath.AddLine(Rect.Width - r2 - m, Rect.Height - m, Rect.X + r2 - m, Rect.Height - m)
            GraphPath.AddArc(Rect.X + m, Rect.Y + Rect.Height - radius - m, radius, radius, 90, 90)
            GraphPath.AddLine(Rect.X + m, Rect.Height - r2 - m, Rect.X + m, Rect.Y + r2 + m)
            GraphPath.CloseFigure()
            Return GraphPath
        End Function

        Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
            Dim borderRadius As Integer = 5
            Dim borderThickness As Single = 1.75F
            MyBase.OnPaint(e)
            e.Graphics.SmoothingMode = (CType(SmoothingMode.HighQuality, SmoothingMode))
            Dim Rect As RectangleF = New RectangleF(0, 0, Me.Width, Me.Height)
            Dim GraphPath As GraphicsPath = GetRoundPath(Rect, borderRadius)
            Me.Region = New Region(GraphPath)

            Using pen As Pen = New Pen(Color.Transparent, borderThickness)
                pen.Alignment = PenAlignment.Inset
                e.Graphics.DrawPath(pen, GraphPath)
            End Using
        End Sub

        Public Property BorderColor As Color
            Get
                Return _borderColor
            End Get
            Set(ByVal value As Color)
                _borderColor = value
                Invalidate()
            End Set
        End Property

        Public Property OnHoverBorderColor As Color
            Get
                Return _onHoverBorderColor
            End Get
            Set(ByVal value As Color)
                _onHoverBorderColor = value
                Invalidate()
            End Set
        End Property

        Public Property ButtonColor As Color
            Get
                Return _buttonColor
            End Get
            Set(ByVal value As Color)
                _buttonColor = value
                Invalidate()
            End Set
        End Property

        Public Property OnHoverButtonColor As Color
            Get
                Return _onHoverButtonColor
            End Get
            Set(ByVal value As Color)
                _onHoverButtonColor = value
                Invalidate()
            End Set
        End Property

        Public Property TextColor As Color
            Get
                Return _textColor
            End Get
            Set(ByVal value As Color)
                _textColor = value
                Invalidate()
            End Set
        End Property

        Public Property OnHoverTextColor As Color
            Get
                Return _onHoverTextColor
            End Get
            Set(ByVal value As Color)
                _onHoverTextColor = value
                Invalidate()
            End Set
        End Property
    End Class
End Namespace

