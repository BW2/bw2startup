﻿Imports System
Imports System.Collections.Generic
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms

Namespace Resources
    Public Class RoundedTextbox
        Inherits Control

        Private radius As Integer = 10
        Public textBox As TextBox = New TextBox()
        Private shape As GraphicsPath
        Private innerRect As GraphicsPath
        Private br As Color

        Public Sub New()
            MyBase.SetStyle(ControlStyles.SupportsTransparentBackColor, True)
            MyBase.SetStyle(ControlStyles.UserPaint, True)
            MyBase.SetStyle(ControlStyles.ResizeRedraw, True)
            Me.textBox.Parent = Me
            MyBase.Controls.Add(Me.textBox)
            Me.textBox.BorderStyle = BorderStyle.None
            textBox.Font = Me.Font
            Me.BackColor = Color.Transparent
            Me.ForeColor = Color.Black
            Me.br = Color.White
            textBox.BackColor = Me.br
            Me.Text = Nothing
            Me.Font = New Font("Century Gothic", 10.0F)
            MyBase.Size = New Size(&H87, &H21)
            Me.DoubleBuffered = True
            AddHandler textBox.TextChanged, New EventHandler(AddressOf textBox_TextChanged)
            Me.textBox.PasswordChar = "*"c
        End Sub

        Private Sub TextBox_MouseDoubleClick(ByVal sender As Object, ByVal e As MouseEventArgs)
            If e.Button = MouseButtons.Left Then
                textBox.SelectAll()
            End If
        End Sub

        Private Sub TextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
            Me.Text = textBox.Text
        End Sub

        Private Sub TextBox_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)
            If e.Control AndAlso (e.KeyCode = Keys.A) Then
                textBox.SelectionStart = 0
                textBox.SelectionLength = Me.Text.Length
            End If
        End Sub

        Protected Overrides Sub OnFontChanged(ByVal e As EventArgs)
            MyBase.OnFontChanged(e)
            textBox.Font = Me.Font
            MyBase.Invalidate()
        End Sub

        Protected Overrides Sub OnForeColorChanged(ByVal e As EventArgs)
            MyBase.OnForeColorChanged(e)
            textBox.ForeColor = Me.ForeColor
            MyBase.Invalidate()
        End Sub

        Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
            Me.shape = New MyRectangle(CSng(MyBase.Width), CSng(MyBase.Height), CSng(Me.radius), 0F, 0F).Path
            Me.innerRect = New MyRectangle(MyBase.Width - 0.5F, MyBase.Height - 0.5F, CSng(Me.radius), 0.5F, 0.5F).Path

            If textBox.Height >= (MyBase.Height - 4) Then
                MyBase.Height = textBox.Height + 4
            End If

            textBox.Location = New Point(Me.radius - 5, (MyBase.Height / 2) - (textBox.Font.Height / 2))
            textBox.Width = MyBase.Width - (CInt((Me.radius * 1.5)))
            e.Graphics.SmoothingMode = (CType(SmoothingMode.HighQuality, SmoothingMode))
            Dim bitmap As Bitmap = New Bitmap(MyBase.Width, MyBase.Height)
            Dim graphics As Graphics = Graphics.FromImage(bitmap)
            e.Graphics.DrawPath(Pens.Gray, Me.shape)

            Using brush As SolidBrush = New SolidBrush(Me.br)
                e.Graphics.FillPath(CType(brush, Brush), Me.innerRect)
            End Using

            Trans.MakeTransparent(Me, e.Graphics)
            MyBase.OnPaint(e)
        End Sub

        Protected Overrides Sub OnTextChanged(ByVal e As EventArgs)
            MyBase.OnTextChanged(e)
            textBox.Text = Me.Text
        End Sub

        Public Sub SelectAll()
            textBox.SelectAll()
        End Sub

        Public Property PasswordField As Char
            Get
                Return Me.textBox.PasswordChar
            End Get
            Set(ByVal value As Char)
                Me.textBox.PasswordChar = value
            End Set
        End Property

        Public Overrides Property BackColor As Color
            Get
                Return MyBase.BackColor
            End Get
            Set(ByVal value As Color)
                MyBase.BackColor = Color.Transparent
            End Set
        End Property
    End Class
End Namespace
