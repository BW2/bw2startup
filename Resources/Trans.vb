﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Drawing
Imports System.Windows.Forms

Namespace Resources
    Friend Class Trans
        Public Shared Sub MakeTransparent(ByVal control As Control, ByVal g As Graphics)
            Dim parent As Control = control.Parent

            If parent IsNot Nothing Then
                Dim rectangle As Rectangle = control.Bounds
                Dim controls As Control.ControlCollection = parent.Controls
                Dim index As Integer = controls.IndexOf(control)
                Dim bitmap As Bitmap = Nothing

                For i As Integer = controls.Count - 1 To index + 1
                    Dim control3 As Control = controls(i)

                    If control3.Bounds.IntersectsWith(rectangle) Then

                        If bitmap Is Nothing Then
                            bitmap = New Bitmap(control.Parent.ClientSize.Width, control.Parent.ClientSize.Height)
                        End If

                        control3.DrawToBitmap(bitmap, control3.Bounds)
                    End If
                Next

                If bitmap IsNot Nothing Then
                    g.DrawImage(CType(bitmap, Image), control.ClientRectangle, rectangle, CType(GraphicsUnit.Pixel, GraphicsUnit))
                    bitmap.Dispose()
                End If
            End If
        End Sub
    End Class
End Namespace
