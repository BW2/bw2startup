Imports System.Net
Imports System.Drawing

Namespace BW2Startup

    Public Class AddPortal
        Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelPortalURL As System.Windows.Forms.Label
        Friend WithEvents ButtonOK As Resources.RoundedButton
        Friend WithEvents PortalURL As Resources.RoundedTextbox
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddPortal))
            Me.LabelPortalURL = New System.Windows.Forms.Label()
            Me.PortalURL = New BW2Technologies.Resources.RoundedTextbox()
            Me.ButtonOK = New BW2Technologies.Resources.RoundedButton()
            Me.SuspendLayout()
            '
            'LabelPortalURL
            '
            Me.LabelPortalURL.BackColor = System.Drawing.Color.Transparent
            Me.LabelPortalURL.Font = New System.Drawing.Font("Century Gothic", 12.0!)
            Me.LabelPortalURL.ForeColor = System.Drawing.SystemColors.HighlightText
            Me.LabelPortalURL.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.LabelPortalURL.Location = New System.Drawing.Point(162, 159)
            Me.LabelPortalURL.Name = "LabelPortalURL"
            Me.LabelPortalURL.Size = New System.Drawing.Size(145, 24)
            Me.LabelPortalURL.TabIndex = 9
            Me.LabelPortalURL.Text = "Portal Name/URL"
            Me.LabelPortalURL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            'PortalURL
            '
            Me.PortalURL.BackColor = System.Drawing.Color.Transparent
            Me.PortalURL.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.PortalURL.ForeColor = System.Drawing.Color.Black
            Me.PortalURL.Location = New System.Drawing.Point(37, 195)
            Me.PortalURL.Name = "PortalURL"
            Me.PortalURL.PasswordField = Global.Microsoft.VisualBasic.ChrW(0)
            Me.PortalURL.Size = New System.Drawing.Size(395, 28)
            Me.PortalURL.TabIndex = 13
            '
            'ButtonOK
            '
            Me.ButtonOK.BackColor = System.Drawing.Color.White
            Me.ButtonOK.BorderColor = System.Drawing.Color.Empty
            Me.ButtonOK.ButtonColor = System.Drawing.Color.Empty
            Me.ButtonOK.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.ButtonOK.ForeColor = System.Drawing.Color.Navy
            Me.ButtonOK.Location = New System.Drawing.Point(161, 267)
            Me.ButtonOK.Name = "ButtonOK"
            Me.ButtonOK.OnHoverBorderColor = System.Drawing.Color.Empty
            Me.ButtonOK.OnHoverButtonColor = System.Drawing.Color.Empty
            Me.ButtonOK.OnHoverTextColor = System.Drawing.Color.Empty
            Me.ButtonOK.Size = New System.Drawing.Size(147, 31)
            Me.ButtonOK.TabIndex = 11
            Me.ButtonOK.Text = "Add Portal"
            Me.ButtonOK.TextColor = System.Drawing.Color.Empty
            Me.ButtonOK.UseVisualStyleBackColor = False
            '
            'AddPortal
            '
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
            Me.BackgroundImage = Global.BW2Technologies.My.Resources.Resources.loginNew
            Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
            Me.ClientSize = New System.Drawing.Size(454, 397)
            Me.Controls.Add(Me.PortalURL)
            Me.Controls.Add(Me.ButtonOK)
            Me.Controls.Add(Me.LabelPortalURL)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Location = New System.Drawing.Point(10, 20)
            Me.MaximizeBox = False
            Me.MaximumSize = New System.Drawing.Size(1000, 446)
            Me.MinimizeBox = False
            Me.MinimumSize = New System.Drawing.Size(470, 436)
            Me.Name = "AddPortal"
            Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
            Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
            Me.ResumeLayout(False)

        End Sub

#End Region


        ReadOnly Property LoginForm() As LoginForm
            Get
                Return CType(Me.Owner, LoginForm)
            End Get
        End Property

        Function GetCaption(ByVal key As String) As String
            Try
                Return CType(Me.Owner, LoginForm).GetCaption(key)
            Catch
                Return key
            End Try
        End Function

        Private m_closing As Boolean = False


        Private Sub ButtonOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonOK.Click
            Dim completions As String() = {"download"}

            Me.Cursor = Cursors.WaitCursor
            Me.ButtonOK.Enabled = False
            Me.PortalURL.Enabled = False
            Try
                Dim urltext As String = PortalURL.Text
                urltext = urltext.TrimEnd("/".ToCharArray)
                urltext = urltext.TrimEnd("\".ToCharArray)
                Dim discoveryCode As String = ""
                If Not Uri.IsWellFormedUriString(urltext, UriKind.Absolute) OrElse (urltext.IndexOf("://") < 0 AndAlso urltext.IndexOf("/") < 0) Then
                    ' test hosting code 
                    'Dim hi As New HostingInfo.HostingInfo

                    'Try
                    '    Dim u As New Uri(hi.Url)
                    '    LoginForm.DNSGetHostEntry(u.DnsSafeHost, 2000)
                    'Catch ex As Exception
                    '    If Me.DialogResult = Windows.Forms.DialogResult.Cancel Then Exit Sub

                    '    Me.DialogResult = Windows.Forms.DialogResult.None
                    '    MsgBox(GetCaption(ex.Message), MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, GetCaption("Discovery.ErrorTitle"))
                    '    Exit Sub
                    'End Try

                    'Try
                    '    Dim s As String = hi.GetPortalUrlOrState(urltext)
                    '    Dim us As String() = Split(s, ";")
                    '    Dim cs As String() = Split(us(0), ":")
                    '    If IsNumeric(cs(0)) AndAlso cs.Length = 2 Then
                    '        'error code
                    '        Me.Cursor = Cursors.Default
                    '        Dim errtxt As String = GetCaption("Discovery." & cs(0))
                    '        If errtxt.StartsWith("Discovery") Then
                    '            errtxt = GetCaption(cs(1))
                    '        End If
                    '        Me.DialogResult = Windows.Forms.DialogResult.None
                    '        MsgBox(errtxt, MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, GetCaption("Discovery.ErrorTitle"))
                    '        Exit Sub
                    '    End If
                    '    discoveryCode = urltext
                    '    urltext = us(0)
                    'Catch ex As Exception
                    '    Me.DialogResult = Windows.Forms.DialogResult.None
                    '    MsgBox(GetCaption(ex.Message), MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, GetCaption("Discovery.ErrorTitle"))
                    '    Exit Sub
                    'End Try
                End If

                ' Dim wc As New Net.WebClient

                For Each comp As String In completions

                    Try
                        Do
                            Try
                                If comp <> "" Then comp = "/" + comp

                                Dim fn As String = System.Windows.Forms.Application.StartupPath & "\Remote_Portals.xml"

                                Me.LoginForm.DownloadFile(urltext + comp + "/Portals.xml", fn, , , , , "OnAddPortalClicked")

                                'wc.DownloadFile(uri + comp + "/Portals.xml", "Remote_Portals.xml")
                                Me.Cursor = Cursors.Default
                                LoginForm.UpdateXML(fn, False, False, , discoveryCode)
                                IO.File.Delete(fn)
                                Me.DialogResult = DialogResult.OK
                                Exit Sub
                            Catch ex As BW2Global.BW2Exception When ex.Message = "retry"
                            End Try
                        Loop
                    Catch ex As Net.WebException When TypeOf ex.Response Is HttpWebResponse AndAlso CType(ex.Response, HttpWebResponse).StatusCode = HttpStatusCode.NotFound
                    Catch ex As Exception
                        Me.Cursor = Cursors.Default
                        If Me.DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                            MsgBox(GetCaption(ex.Message))
                        End If
                        Me.DialogResult = System.Windows.Forms.DialogResult.None
                        Exit Sub
                    End Try
                Next
                Me.Cursor = Cursors.Default
                MsgBox(GetCaption("Invalid Portal Server"))
                Me.DialogResult = DialogResult.None
            Finally
                Me.ButtonOK.Enabled = True
                Me.PortalURL.Enabled = True
                Me.ButtonOK.Focus()
                Me.PortalURL.Focus()
            End Try
        End Sub

        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
            Me.Text = GetCaption("Add Portal Server")
            Me.ButtonOK.Text = GetCaption("Add Portal")
            'Me.ButtonCancel.Text = GetCaption("Cancel")
            Me.LabelPortalURL.Text = GetCaption("Portal Name/URL")
        End Sub

        Private Sub PortalURL_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PortalURL.TextChanged
            If PortalURL.Text <> "" Then
                ButtonOK.Enabled = True
            Else
                ButtonOK.Enabled = False
            End If
        End Sub

        Private Sub ButtonCancel_Click(sender As Object, e As EventArgs)
            Me.Hide()
        End Sub

    End Class

End Namespace

