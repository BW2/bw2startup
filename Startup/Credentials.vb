Imports System.IO
Imports System.Net
Imports BW2Technologies.BW2Global

Namespace BW2Startup

    Public Class Credentials

        Private m_CredentialCache As New CredentialCache
        Private m_filepath As String

        Private m_Credentials As New Hashtable

        Public Sub New(ByVal filepath As String)
            m_filepath = filepath
            FileTools.AssertPathExists(filepath, True)
        End Sub


        Private ReadOnly Property SecretKey() As String
            Get
                Return Environment.MachineName.ToLower & ";" & m_filepath.ToLower
            End Get
        End Property

        Public Sub Load()
            m_CredentialCache = New CredentialCache
            Try
                If IO.File.Exists(m_filepath) Then
                    Dim sec As New BW2Technologies.BW2Global.Security.CryptoProvider
                    Dim fs As New FileStream(m_filepath, FileMode.Open, FileAccess.Read)
                    Dim br As New BinaryReader(fs)
                    Dim len As Integer = br.ReadInt32
                    Dim b(len - 1) As Byte
                    br.Read(b, 0, len)
                    b = sec.Decrypt(b, SecretKey)
                    br.Close()
                    Dim ms As New MemoryStream(b, False)
                    br = New BinaryReader(ms)
                    Dim num As Integer = br.ReadInt32
                    Dim i As Integer
                    For i = 0 To num - 1
                        Dim n As Integer = br.ReadInt32
                        Dim s(n - 1) As String
                        Dim j As Integer
                        For j = 0 To n - 1
                            s(j) = br.ReadString
                        Next
                        If s(0) <> "" Then
                            Dim cred As NetworkCredential
                            Select Case s(0).ToLower
                                Case "basic", "digest"
                                    cred = New NetworkCredential(s(2), s(3), s(4))
                                    m_CredentialCache.Add(New Uri(s(1)), s(0), cred)
                            End Select
                            m_Credentials(s(0).ToLower & " " & s(1)) = s
                        End If
                    Next
                    ms.Close()
                    br.Close()
                    fs.Close()
                End If
            Catch
            End Try
        End Sub

        Public Sub Save()
            Dim bw As BinaryWriter
            Dim fs As FileStream
            Dim ms As New MemoryStream
            bw = New BinaryWriter(ms)
            Dim de As DictionaryEntry
            bw.Write(m_Credentials.Count)
            For Each de In m_Credentials
                Dim s As String() = CType(de.Value, String())
                bw.Write(s.Length)
                Dim i As Integer
                For i = 0 To s.GetUpperBound(0)
                    bw.Write(s(i))
                Next
            Next
            bw.Close()
            Dim b As Byte() = ms.ToArray
            ms.Close()
            Dim sec As New BW2Technologies.BW2Global.Security.CryptoProvider
            b = sec.Encrypt(b, SecretKey, BW2Technologies.BW2Global.Security.EncryptionMethod.RC2_128Bit, BW2Technologies.BW2Global.Security.CompressionMethod.DeflateCompression, BW2Technologies.BW2Global.Security.MessageAuthenticationMethod.SHA1)
            fs = New FileStream(m_filepath & ".tmp", FileMode.OpenOrCreate, FileAccess.Write)
            bw = New BinaryWriter(fs)
            bw.Write(b.Length)
            bw.Write(b)
            bw.Close()
            fs.Close()
            If File.Exists(m_filepath) Then
                File.Delete(m_filepath)
            End If
            File.Move(m_filepath & ".tmp", m_filepath)

        End Sub

        Public Function GetCredentials() As ICredentials
            Return m_CredentialCache
        End Function

        Public Function PromptForUserCredentials(ByVal LoginForm As LoginForm, ByVal uriPrefix As System.Uri, ByVal authType As String, ByVal realm As String) As Boolean
            Dim m As New CredentialsForm
            m.Owner = LoginForm
            m.Text = realm
            m_CredentialCache.Remove(uriPrefix, authType)
            If m.ShowDialog = DialogResult.OK Then
                Dim cred As New NetworkCredential(m.UserName.Text, m.Password.Text)
                For Each authType In Split(authType, ",")
                    authType = Trim(authType)
                    Try
                        m_CredentialCache.Add(uriPrefix, authType, cred)
                        Dim s As String() = {authType, uriPrefix.ToString, m.UserName.Text, m.Password.Text, ""}
                        m_Credentials(s(0).ToLower & " " & s(1).ToLower) = s
                    Catch
                    End Try
                Next
                Save()
                Return True
            End If
        End Function


    End Class

End Namespace
