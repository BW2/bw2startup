Namespace BW2Startup

    Friend Class CredentialsForm
        Inherits System.Windows.Forms.Form

        Function GetCaption(ByVal key As String) As String
            Try
                Return CType(Me.Owner, LoginForm).GetCaption(key)
            Catch
                Return key
            End Try
        End Function

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents UserName As System.Windows.Forms.TextBox
        Friend WithEvents Password As System.Windows.Forms.TextBox
        Friend WithEvents Label2 As System.Windows.Forms.Label
        Friend WithEvents LabelUsername As System.Windows.Forms.Label
        Friend WithEvents ButtonOK As System.Windows.Forms.Button
        Friend WithEvents ButtonCancel As System.Windows.Forms.Button
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CredentialsForm))
            Me.ButtonOK = New System.Windows.Forms.Button()
            Me.ButtonCancel = New System.Windows.Forms.Button()
            Me.UserName = New System.Windows.Forms.TextBox()
            Me.Password = New System.Windows.Forms.TextBox()
            Me.LabelUsername = New System.Windows.Forms.Label()
            Me.Label2 = New System.Windows.Forms.Label()
            Me.SuspendLayout()
            '
            'ButtonOK
            '
            Me.ButtonOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.ButtonOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.ButtonOK.Location = New System.Drawing.Point(152, 88)
            Me.ButtonOK.Name = "ButtonOK"
            Me.ButtonOK.Size = New System.Drawing.Size(75, 23)
            Me.ButtonOK.TabIndex = 3
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.ButtonCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.ButtonCancel.Location = New System.Drawing.Point(248, 88)
            Me.ButtonCancel.Name = "ButtonCancel"
            Me.ButtonCancel.Size = New System.Drawing.Size(75, 23)
            Me.ButtonCancel.TabIndex = 4
            '
            'UserName
            '
            Me.UserName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.UserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.UserName.Location = New System.Drawing.Point(88, 16)
            Me.UserName.Name = "UserName"
            Me.UserName.Size = New System.Drawing.Size(232, 20)
            Me.UserName.TabIndex = 1
            '
            'Password
            '
            Me.Password.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Password.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
            Me.Password.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.Password.Location = New System.Drawing.Point(88, 48)
            Me.Password.Name = "Password"
            Me.Password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
            Me.Password.Size = New System.Drawing.Size(232, 20)
            Me.Password.TabIndex = 2
            '
            'LabelUsername
            '
            Me.LabelUsername.BackColor = System.Drawing.Color.Transparent
            Me.LabelUsername.ForeColor = System.Drawing.SystemColors.HighlightText
            Me.LabelUsername.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.LabelUsername.Location = New System.Drawing.Point(16, 16)
            Me.LabelUsername.Name = "LabelUsername"
            Me.LabelUsername.Size = New System.Drawing.Size(64, 24)
            Me.LabelUsername.TabIndex = 3
            Me.LabelUsername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'Label2
            '
            Me.Label2.BackColor = System.Drawing.Color.Transparent
            Me.Label2.ForeColor = System.Drawing.SystemColors.HighlightText
            Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.Label2.Location = New System.Drawing.Point(16, 48)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(64, 24)
            Me.Label2.TabIndex = 3
            Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'CredentialsForm
            '
            Me.AcceptButton = Me.ButtonOK
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
            Me.CancelButton = Me.ButtonCancel
            Me.ClientSize = New System.Drawing.Size(336, 118)
            Me.Controls.Add(Me.LabelUsername)
            Me.Controls.Add(Me.UserName)
            Me.Controls.Add(Me.Password)
            Me.Controls.Add(Me.ButtonCancel)
            Me.Controls.Add(Me.ButtonOK)
            Me.Controls.Add(Me.Label2)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.Name = "CredentialsForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

    End Class

End Namespace

