
Namespace BW2Startup

    Public Class ErrorPage
        Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents Label1 As System.Windows.Forms.Label
        Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
        Friend WithEvents Label2 As System.Windows.Forms.Label
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents ButtonOK As System.Windows.Forms.Button
        Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
        Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(ErrorPage))
            Me.Label1 = New System.Windows.Forms.Label
            Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
            Me.Label2 = New System.Windows.Forms.Label
            Me.GroupBox1 = New System.Windows.Forms.GroupBox
            Me.ButtonOK = New System.Windows.Forms.Button
            Me.TextBox1 = New System.Windows.Forms.TextBox
            Me.LinkLabel1 = New System.Windows.Forms.LinkLabel
            Me.GroupBox1.SuspendLayout()
            Me.SuspendLayout()
            '
            'Label1
            '
            Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Location = New System.Drawing.Point(64, 8)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(520, 48)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Label1"
            Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'ImageList1
            '
            Me.ImageList1.ImageSize = New System.Drawing.Size(32, 32)
            Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
            Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
            '
            'Label2
            '
            Me.Label2.ImageIndex = 1
            Me.Label2.ImageList = Me.ImageList1
            Me.Label2.Location = New System.Drawing.Point(16, 16)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(32, 32)
            Me.Label2.TabIndex = 1
            '
            'GroupBox1
            '
            Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupBox1.Controls.Add(Me.TextBox1)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 64)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(576, 352)
            Me.GroupBox1.TabIndex = 4
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = "Error Text"
            '
            'ButtonOK
            '
            Me.ButtonOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonOK.Location = New System.Drawing.Point(472, 424)
            Me.ButtonOK.Name = "ButtonOK"
            Me.ButtonOK.Size = New System.Drawing.Size(104, 23)
            Me.ButtonOK.TabIndex = 5
            Me.ButtonOK.Text = "OK"
            '
            'TextBox1
            '
            Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.TextBox1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.TextBox1.Location = New System.Drawing.Point(3, 16)
            Me.TextBox1.Multiline = True
            Me.TextBox1.Name = "TextBox1"
            Me.TextBox1.ReadOnly = True
            Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.TextBox1.Size = New System.Drawing.Size(570, 333)
            Me.TextBox1.TabIndex = 0
            Me.TextBox1.Text = "TextBox1"
            '
            'LinkLabel1
            '
            Me.LinkLabel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.LinkLabel1.Location = New System.Drawing.Point(8, 432)
            Me.LinkLabel1.Name = "LinkLabel1"
            Me.LinkLabel1.Size = New System.Drawing.Size(440, 16)
            Me.LinkLabel1.TabIndex = 1
            Me.LinkLabel1.TabStop = True
            Me.LinkLabel1.Text = "ErrorText.html"
            Me.LinkLabel1.Visible = False
            '
            'ErrorPage
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.ClientSize = New System.Drawing.Size(592, 454)
            Me.Controls.Add(Me.ButtonOK)
            Me.Controls.Add(Me.Label2)
            Me.Controls.Add(Me.Label1)
            Me.Controls.Add(Me.GroupBox1)
            Me.Controls.Add(Me.LinkLabel1)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Name = "ErrorPage"
            Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "ErrorPage"
            Me.GroupBox1.ResumeLayout(False)
            Me.ResumeLayout(False)

        End Sub

#End Region


        ReadOnly Property LoginForm() As LoginForm
            Get
                Return CType(Me.Owner, LoginForm)
            End Get
        End Property

        Function GetCaption(ByVal key As String) As String
            Try
                Return CType(Me.Owner, LoginForm).GetCaption(key)
            Catch
                Return key
            End Try
        End Function

        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
            Me.Text = GetCaption("Error Page")
            Me.ButtonOK.Text = GetCaption("OK")
            Me.GroupBox1.Text = GetCaption("Error Text")
            Me.LinkLabel1.Text = GetCaption("Open local error file.")
        End Sub

        Public Property ErrorMessage() As String
            Get
                Return Me.Label1.Text
            End Get
            Set(ByVal Value As String)
                Label1.Text = Value
            End Set
        End Property
        Public Property ErrorText() As String
            Get
                Return Me.TextBox1.Text
            End Get
            Set(ByVal Value As String)
                Me.TextBox1.Text = Value
                Me.TextBox1.Select(0, 0)
            End Set
        End Property
        Private filename As String = ""
        Public Property LinkFilename() As String
            Get
                Return filename
            End Get
            Set(ByVal Value As String)
                filename = Value
                LinkLabel1.Visible = filename <> ""
            End Set
        End Property
        Private Sub ButtonOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonOK.Click
            Me.Close()
        End Sub

        Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
            Try
                Process.Start(LinkFilename)
            Catch
            End Try
        End Sub
    End Class

End Namespace
