Option Strict On
Option Explicit On

Imports System.IO
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports BW2Technologies.BW2Global
Imports System.Resources
Imports System.Drawing



Namespace BW2Startup

    Public Class LoginForm
        Inherits System.Windows.Forms.Form

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        Friend WithEvents DropDownPortal As System.Windows.Forms.ComboBox
        Friend WithEvents ProgressBarDownload As System.Windows.Forms.ProgressBar
        Friend WithEvents LabelProgress As System.Windows.Forms.Label
        Friend WithEvents LabelUsername As System.Windows.Forms.Label
        Friend WithEvents LabelPassword As System.Windows.Forms.Label
        Friend WithEvents LabelPortal As System.Windows.Forms.Label
        Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
        Friend WithEvents MenuItemClearCache As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItemAddPortals As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItemRemovePortal As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItemShowLog As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItemInfo As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItemFile As System.Windows.Forms.MenuItem
        Friend WithEvents PictureBox1 As PictureBox
        Friend WithEvents LabelPortalName As Label
        Friend WithEvents ButtonOK As Resources.RoundedButton
        Friend WithEvents TextBoxPassword As Resources.RoundedTextbox
        Friend WithEvents TextBoxUsername As Resources.RoundedTextbox


        Public Function ExeName32() As String
            Return "/BW2Browser.x86.exe"
        End Function
        Public Function ExeNameAny() As String
            Return "/BW2Browser.exe"
        End Function
        'Friend WithEvents MenuItemUpdate As System.Windows.Forms.MenuItem
        'Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem

#Region " Windows Form Designer generated code "

        Private m_startupImage As Image
        Public Shared Instance As LoginForm = Nothing

        Public Sub New()
            MyBase.New()
            Instance = Me

            Me.ShowInTaskbar = True
            'This call is required by the Windows Form Designer.
            InitializeComponent()

            ws.EnableDecompression = True
            ws.PreAuthenticate = True
            ws.UserAgent = "BW2Startup"

            'Add any initialization after the InitializeComponent() call
            m_MessageFilter.Add(Me)
            m_MessageFilterEmpty.Add(Me)
            'Me.Label1.Text = Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString
        End Sub

        Protected Function ImageFromFile(ByVal fn As String) As Image
            Dim b As Byte() = IO.File.ReadAllBytes(fn)
            Using ms As New MemoryStream(b, False)
                Return Image.FromStream(ms)
            End Using
        End Function


        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub


        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoginForm))
            Me.ContextMenu1 = New System.Windows.Forms.ContextMenu()
            Me.MenuItemClearCache = New System.Windows.Forms.MenuItem()
            Me.MenuItemAddPortals = New System.Windows.Forms.MenuItem()
            Me.MenuItemRemovePortal = New System.Windows.Forms.MenuItem()
            Me.MenuItemShowLog = New System.Windows.Forms.MenuItem()
            Me.MenuItemFile = New System.Windows.Forms.MenuItem()
            Me.MenuItemInfo = New System.Windows.Forms.MenuItem()
            Me.LabelUsername = New System.Windows.Forms.Label()
            Me.LabelPassword = New System.Windows.Forms.Label()
            Me.DropDownPortal = New System.Windows.Forms.ComboBox()
            Me.LabelPortal = New System.Windows.Forms.Label()
            Me.ProgressBarDownload = New System.Windows.Forms.ProgressBar()
            Me.LabelProgress = New System.Windows.Forms.Label()
            Me.PictureBox1 = New System.Windows.Forms.PictureBox()
            Me.LabelPortalName = New System.Windows.Forms.Label()
            Me.TextBoxUsername = New BW2Technologies.Resources.RoundedTextbox()
            Me.TextBoxPassword = New BW2Technologies.Resources.RoundedTextbox()
            Me.ButtonOK = New BW2Technologies.Resources.RoundedButton()
            CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ContextMenu1
            '
            Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemClearCache, Me.MenuItemAddPortals, Me.MenuItemRemovePortal, Me.MenuItemShowLog, Me.MenuItemFile, Me.MenuItemInfo})
            '
            'MenuItemClearCache
            '
            Me.MenuItemClearCache.Index = 0
            Me.MenuItemClearCache.Text = "Clear Cache"
            '
            'MenuItemAddPortals
            '
            Me.MenuItemAddPortals.Index = 1
            Me.MenuItemAddPortals.Text = "Add Portals"
            '
            'MenuItemRemovePortal
            '
            Me.MenuItemRemovePortal.Index = 2
            Me.MenuItemRemovePortal.Text = "Remove Portal"
            '
            'MenuItemShowLog
            '
            Me.MenuItemShowLog.Index = 3
            Me.MenuItemShowLog.Text = "Show Logfile"
            '
            'MenuItemFile
            '
            Me.MenuItemFile.Index = 4
            Me.MenuItemFile.Text = "Update"
            '
            'MenuItemInfo
            '
            Me.MenuItemInfo.Enabled = False
            Me.MenuItemInfo.Index = 5
            Me.MenuItemInfo.Text = "Info"
            '
            'LabelUsername
            '
            Me.LabelUsername.BackColor = System.Drawing.Color.Transparent
            Me.LabelUsername.Enabled = False
            Me.LabelUsername.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LabelUsername.ForeColor = System.Drawing.SystemColors.HighlightText
            Me.LabelUsername.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.LabelUsername.Location = New System.Drawing.Point(59, 196)
            Me.LabelUsername.Name = "LabelUsername"
            Me.LabelUsername.Size = New System.Drawing.Size(112, 16)
            Me.LabelUsername.TabIndex = 9
            Me.LabelUsername.Text = "Username"
            Me.LabelUsername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            Me.LabelUsername.UseCompatibleTextRendering = True
            '
            'LabelPassword
            '
            Me.LabelPassword.BackColor = System.Drawing.Color.Transparent
            Me.LabelPassword.Enabled = False
            Me.LabelPassword.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LabelPassword.ForeColor = System.Drawing.SystemColors.HighlightText
            Me.LabelPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.LabelPassword.Location = New System.Drawing.Point(59, 231)
            Me.LabelPassword.Name = "LabelPassword"
            Me.LabelPassword.Size = New System.Drawing.Size(112, 16)
            Me.LabelPassword.TabIndex = 10
            Me.LabelPassword.Text = "Password"
            Me.LabelPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            Me.LabelPassword.UseCompatibleTextRendering = True
            '
            'DropDownPortal
            '
            Me.DropDownPortal.ContextMenu = Me.ContextMenu1
            Me.DropDownPortal.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
            Me.DropDownPortal.DropDownHeight = 160
            Me.DropDownPortal.DropDownWidth = 260
            Me.DropDownPortal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.DropDownPortal.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.DropDownPortal.IntegralHeight = False
            Me.DropDownPortal.ItemHeight = 20
            Me.DropDownPortal.Location = New System.Drawing.Point(177, 162)
            Me.DropDownPortal.Name = "DropDownPortal"
            Me.DropDownPortal.Size = New System.Drawing.Size(218, 26)
            Me.DropDownPortal.TabIndex = 3
            '
            'LabelPortal
            '
            Me.LabelPortal.BackColor = System.Drawing.Color.Transparent
            Me.LabelPortal.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LabelPortal.ForeColor = System.Drawing.SystemColors.HighlightText
            Me.LabelPortal.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.LabelPortal.Location = New System.Drawing.Point(59, 164)
            Me.LabelPortal.Name = "LabelPortal"
            Me.LabelPortal.Size = New System.Drawing.Size(112, 16)
            Me.LabelPortal.TabIndex = 9
            Me.LabelPortal.Text = "Portal"
            Me.LabelPortal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            Me.LabelPortal.UseCompatibleTextRendering = True
            '
            'ProgressBarDownload
            '
            Me.ProgressBarDownload.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ProgressBarDownload.BackColor = System.Drawing.Color.White
            Me.ProgressBarDownload.ForeColor = System.Drawing.Color.CornflowerBlue
            Me.ProgressBarDownload.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.ProgressBarDownload.Location = New System.Drawing.Point(61, 365)
            Me.ProgressBarDownload.Name = "ProgressBarDownload"
            Me.ProgressBarDownload.Size = New System.Drawing.Size(334, 10)
            Me.ProgressBarDownload.Style = System.Windows.Forms.ProgressBarStyle.Continuous
            Me.ProgressBarDownload.TabIndex = 13
            Me.ProgressBarDownload.Visible = False
            '
            'LabelProgress
            '
            Me.LabelProgress.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelProgress.BackColor = System.Drawing.Color.Transparent
            Me.LabelProgress.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LabelProgress.ForeColor = System.Drawing.SystemColors.HighlightText
            Me.LabelProgress.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.LabelProgress.Location = New System.Drawing.Point(108, 331)
            Me.LabelProgress.Name = "LabelProgress"
            Me.LabelProgress.Size = New System.Drawing.Size(250, 12)
            Me.LabelProgress.TabIndex = 14
            Me.LabelProgress.Text = "Powered by BW2 - � 2020"
            Me.LabelProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter
            Me.LabelProgress.UseCompatibleTextRendering = True
            Me.LabelProgress.UseMnemonic = False
            Me.LabelProgress.Visible = False
            '
            'PictureBox1
            '
            Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
            Me.PictureBox1.BackgroundImage = Global.BW2Technologies.My.Resources.Resources.CustomComboBoxNormal
            Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
            Me.PictureBox1.Location = New System.Drawing.Point(177, 157)
            Me.PictureBox1.Name = "PictureBox1"
            Me.PictureBox1.Size = New System.Drawing.Size(218, 32)
            Me.PictureBox1.TabIndex = 17
            Me.PictureBox1.TabStop = False
            '
            'LabelPortalName
            '
            Me.LabelPortalName.BackColor = System.Drawing.Color.White
            Me.LabelPortalName.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LabelPortalName.ForeColor = System.Drawing.Color.Black
            Me.LabelPortalName.ImeMode = System.Windows.Forms.ImeMode.NoControl
            Me.LabelPortalName.Location = New System.Drawing.Point(182, 163)
            Me.LabelPortalName.Name = "LabelPortalName"
            Me.LabelPortalName.Size = New System.Drawing.Size(176, 16)
            Me.LabelPortalName.TabIndex = 16
            Me.LabelPortalName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            Me.LabelPortalName.UseCompatibleTextRendering = True
            '
            'TextBoxUsername
            '
            Me.TextBoxUsername.BackColor = System.Drawing.Color.Transparent
            Me.TextBoxUsername.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.TextBoxUsername.ForeColor = System.Drawing.Color.Black
            Me.TextBoxUsername.Location = New System.Drawing.Point(177, 190)
            Me.TextBoxUsername.Name = "TextBoxUsername"
            Me.TextBoxUsername.PasswordField = Global.Microsoft.VisualBasic.ChrW(0)
            Me.TextBoxUsername.Size = New System.Drawing.Size(218, 27)
            Me.TextBoxUsername.TabIndex = 20
            '
            'TextBoxPassword
            '
            Me.TextBoxPassword.BackColor = System.Drawing.Color.Transparent
            Me.TextBoxPassword.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.TextBoxPassword.ForeColor = System.Drawing.Color.Black
            Me.TextBoxPassword.Location = New System.Drawing.Point(177, 224)
            Me.TextBoxPassword.Name = "TextBoxPassword"
            Me.TextBoxPassword.PasswordField = Global.Microsoft.VisualBasic.ChrW(42)
            Me.TextBoxPassword.Size = New System.Drawing.Size(218, 27)
            Me.TextBoxPassword.TabIndex = 21
            '
            'ButtonOK
            '
            Me.ButtonOK.BackColor = System.Drawing.Color.White
            Me.ButtonOK.BorderColor = System.Drawing.Color.White
            Me.ButtonOK.ButtonColor = System.Drawing.Color.Empty
            Me.ButtonOK.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.ButtonOK.ForeColor = System.Drawing.Color.Navy
            Me.ButtonOK.Location = New System.Drawing.Point(157, 280)
            Me.ButtonOK.Name = "ButtonOK"
            Me.ButtonOK.OnHoverBorderColor = System.Drawing.Color.Empty
            Me.ButtonOK.OnHoverButtonColor = System.Drawing.Color.Empty
            Me.ButtonOK.OnHoverTextColor = System.Drawing.Color.Empty
            Me.ButtonOK.Size = New System.Drawing.Size(144, 30)
            Me.ButtonOK.TabIndex = 22
            Me.ButtonOK.Text = "Login"
            Me.ButtonOK.TextColor = System.Drawing.Color.Empty
            Me.ButtonOK.UseVisualStyleBackColor = False
            '
            'LoginForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.BackColor = System.Drawing.SystemColors.Control
            Me.BackgroundImage = Global.BW2Technologies.My.Resources.Resources.loginNew
            Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
            Me.ClientSize = New System.Drawing.Size(454, 397)
            Me.ContextMenu = Me.ContextMenu1
            Me.Controls.Add(Me.LabelPortalName)
            Me.Controls.Add(Me.PictureBox1)
            Me.Controls.Add(Me.TextBoxUsername)
            Me.Controls.Add(Me.TextBoxPassword)
            Me.Controls.Add(Me.ButtonOK)
            Me.Controls.Add(Me.ProgressBarDownload)
            Me.Controls.Add(Me.DropDownPortal)
            Me.Controls.Add(Me.LabelPassword)
            Me.Controls.Add(Me.LabelUsername)
            Me.Controls.Add(Me.LabelPortal)
            Me.Controls.Add(Me.LabelProgress)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.Name = "LoginForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Protected Overrides ReadOnly Property ShowWithoutActivation() As Boolean
            Get
                Return False
            End Get
        End Property

#End Region

#Region "Forms interaction (Control Events)"

        Private m_MessageFilter As New MessageFilter
        Private m_MessageFilterEmpty As New MessageFilter

        Friend Sub DoEvents()
            '  If m_verybusy Then Exit Sub
            Dim b As Boolean = m_busy
            If b Then
                System.Windows.Forms.Application.AddMessageFilter(m_MessageFilterEmpty)
            Else
                System.Windows.Forms.Application.AddMessageFilter(m_MessageFilter)
            End If
            System.Windows.Forms.Application.DoEvents()
            If b Then
                System.Windows.Forms.Application.RemoveMessageFilter(m_MessageFilterEmpty)
            Else
                System.Windows.Forms.Application.RemoveMessageFilter(m_MessageFilter)
            End If
        End Sub

        Private Class MessageFilter
            Implements System.Windows.Forms.IMessageFilter

            Private controls As New Hashtable

            Public Sub Add(ByVal ctrl As System.Windows.Forms.Control)
                controls.Add(ctrl.Handle, ctrl)
            End Sub

            Public Sub Remove(ByVal ctrl As System.Windows.Forms.Control)
                controls.Remove(ctrl.Handle)
            End Sub

            Public Function PreFilterMessage(ByRef m As System.Windows.Forms.Message) As Boolean Implements System.Windows.Forms.IMessageFilter.PreFilterMessage
                If controls.ContainsKey(m.HWnd) Then
                    Return False
                End If
                ' block the following messages
                Dim b As Boolean = False
                Select Case m.Msg
                    Case &HA0 To &HAD
                        ' non-client mouse 
                        b = True
                    Case &H200 To &H20D
                        ' mouse events
                        b = True
                    Case &H100 To &H10F
                        ' key events
                        b = True
                    Case &H233
                        ' drop files
                        b = True
                End Select
                If b Then
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting
                End If
                Return b
            End Function
        End Class

        Private Sub SetProgress(ByVal text As String, ByVal pct As Double)
            If pct <= 0 Then
                ProgressBarDownload.Visible = False
            Else

                ProgressBarDownload.Maximum = 100
                ProgressBarDownload.Value = CInt(pct * 100)
                ProgressBarDownload.Visible = True
            End If
            If text = "" Then
                LabelProgress.Visible = False
            Else
                LabelProgress.Text = text
                LabelProgress.Visible = True
            End If
            DoEvents()
        End Sub
        Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                SaveHistory()
                Log("Exiting startup process... (Exit button pressed)")
            Catch
            End Try
            Environment.Exit(0)
        End Sub
        Private m_busy As Boolean = False
        Private m_verybusy As Boolean = False

        Private Sub UpdateUI()
            ButtonOK.Enabled = Not m_busy
            DropDownPortal.Enabled = Not m_busy
            TextBoxUsername.Enabled = Not m_busy
            UpdateIntegratedSecurity()
            If ButtonOK.Enabled Then
                ButtonOK.Focus()
                If TextBoxPassword.Enabled Then
                    TextBoxPassword.Focus()
                Else
                    TextBoxUsername.Focus()
                End If
            End If
        End Sub

        Friend Function SelectedPortal() As PortalInfo
            Try
                Dim portal As PortalInfo = CType(DropDownPortal.SelectedItem, PortalInfo)

                Return portal
            Catch
            End Try
            Return Nothing
        End Function

        Private Sub ButtonOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonOK.Click
            If m_busy Then Exit Sub
            m_busy = True
            Try
                UpdateUI()
                Dim portal As PortalInfo = CType(DropDownPortal.SelectedItem, PortalInfo)
                If Not portal Is Nothing Then
                    Dim pwd As String = TextBoxPassword.Text
                    Dim username As String = TextBoxUsername.Text
                    If m_StartingUser = "IntegratedSecurity" Then
                        pwd = ""
                        username = ""
                    End If
                    Dim sec As New BW2Technologies.BW2Global.Security.CryptoProvider
                    Dim PWHash As Byte() = sec.GetMAC(sec.GetMAC(pwd), "pw")
                    Dim dbpwd As String = ""
                    Dim i As Integer

                    Dim key As String = System.Environment.UserName.ToLower & " " & System.Environment.MachineName.ToLower
                    Dim dat As Long = CLng(Date.UtcNow.Ticks \ 10000000)
                    dat += 10 - (dat Mod 10)
                    dat = Date.Today.Ticks
                    key += " " & CStr(dat)
                    PWHash = sec.Encrypt(PWHash, key, BW2Technologies.BW2Global.Security.EncryptionMethod.RC2_128Bit, BW2Technologies.BW2Global.Security.CompressionMethod.None, BW2Technologies.BW2Global.Security.MessageAuthenticationMethod.SHA1)
                    pwd = ""
                    For i = 0 To PWHash.GetUpperBound(0)
                        If PWHash(i) < 16 Then pwd += "0"
                        pwd += Hex(PWHash(i))
                    Next

                    Dim proc As Process() = Process.GetProcessesByName("BW2Browser")
                    If proc.Length > 0 Then
                        ' Process is running
                        Dim processMngr As Integer
                        Dim processNameList As String()
                        Dim processPortal As String

                        For aux As Integer = 0 To proc.Length - 1
                            processNameList = proc(aux).MainModule.FileName.Split(CChar("\"))
                            processPortal = processNameList(processNameList.Length - 3)
                            If processPortal = portal.PortalGuid Then
                                processMngr = MsgBox("The process is currently active. Do you wish to terminate and restart it?", vbOKCancel, "Process Running Warning")
                                If processMngr = 1 Then
                                    proc(aux).Kill()
                                    If Not ProceedLogin(portal, username, pwd) Then
                                        ' login failed
                                        TextBoxUsername.Enabled = True
                                        TextBoxPassword.Enabled = True
                                        LabelUsername.Enabled = True
                                        LabelPassword.Enabled = True
                                        m_StartingUser = ""
                                        UpdateIntegratedSecurity()
                                        Me.Show()
                                    Else
                                        Log("Exiting startup process...")
                                        System.Environment.Exit(0)
                                    End If
                                End If
                            Else
                                If Not ProceedLogin(portal, username, pwd) Then
                                    ' login failed
                                    TextBoxUsername.Enabled = True
                                    TextBoxPassword.Enabled = True
                                    LabelUsername.Enabled = True
                                    LabelPassword.Enabled = True
                                    m_StartingUser = ""
                                    UpdateIntegratedSecurity()
                                    Me.Show()
                                Else
                                    Log("Exiting startup process...")
                                    System.Environment.Exit(0)
                                End If
                            End If
                        Next
                    Else
                        ' Process is not running
                        If Not ProceedLogin(portal, username, pwd) Then
                            ' login failed
                            TextBoxUsername.Enabled = True
                            TextBoxPassword.Enabled = True
                            LabelUsername.Enabled = True
                            LabelPassword.Enabled = True
                            m_StartingUser = ""
                            UpdateIntegratedSecurity()
                            Me.Show()
                        Else
                            Log("Exiting startup process...")
                            System.Environment.Exit(0)
                        End If
                    End If
                End If
            Finally
                m_busy = False
                UpdateUI()
            End Try
        End Sub
        Private m_lastSelection As Object = Nothing

        Private Sub DropDownPortal_DrawItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles DropDownPortal.DrawItem
            e.DrawBackground()
            If e.Index < 0 Then Exit Sub
            Dim o As Object = DropDownPortal.Items(e.Index)
            Using br As New SolidBrush(e.ForeColor)
                If TypeOf o Is String Then
                    Using f As New Font(e.Font, FontStyle.Bold)
                        e.Graphics.DrawString(CStr(o), f, br, e.Bounds)
                    End Using
                Else
                    Dim pi As PortalInfo = CType(o, PortalInfo)

                    e.Graphics.DrawString(pi.PortalDisplayName, e.Font, br, e.Bounds)
                    Dim sf As New StringFormat(StringFormatFlags.NoWrap)
                    sf.Alignment = StringAlignment.Far
                    e.Graphics.DrawString(pi.PortalDiscoveryCode, e.Font, Brushes.DarkGray, New Rectangle(e.Bounds.X, e.Bounds.Y, e.Bounds.Width - 2, e.Bounds.Height), sf)
                End If
            End Using

        End Sub

        Private Sub UpdateSelectedImage(ByVal portal As PortalInfo)
            Dim imgpathLocal As String = "/Data/Objects/" & portal.PortalGuid & "/startup_logo.gif"
            Dim imgpathServer As String = "/GetContent.aspx?DocumentKey=startup_logo&DocumentType=res"

            If portal.CurrentURL <> "" Then
                Try
                    Test(portal)
                    Try
                        DownloadFile(portal, imgpathServer, imgpathLocal, "Startup Logo")
                    Catch ex As Exception
                        Log("Startup logo", ex)
                    End Try
                    SetProgress("Powered by BW2 - � 2020", -1)
                Catch ex As BW2ConnectFailedCancelException
                    Log("URL-Test", ex)
                Catch ex As BW2ConnectFailedException
                    Log("URL-Test", ex)
                    MsgBox(portal.PortalDisplayName & "    " & vbCrLf & GetCaption("Connection failed") & ".   ", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly Or MsgBoxStyle.SystemModal, GetCaption("Connection failed"))
                Catch ex As Exception
                    Log("URL-Test", ex)
                End Try
            End If

            OnLoadBackgroundPortal(portal, "startup_logo.gif")

        End Sub

        Private Sub DropDownPortal_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownPortal.SelectedValueChanged
            Me.LabelPortalName.Text = Me.DropDownPortal.SelectedItem.ToString()
            Dim wasDD As Boolean = m_droppedDown
            If m_refreshingPI Then Exit Sub
            If m_busy Then Exit Sub
            If TypeOf DropDownPortal.SelectedItem Is String Then
                If Not AddPortal() Then
                    DropDownPortal.SelectedItem = m_lastSelection
                    Exit Sub
                End If
            End If
            m_busy = True
            m_verybusy = True
            UpdateUI()
            Try
                m_lastSelection = DropDownPortal.SelectedItem
                'SetProgress("", -1)
                Dim portal As PortalInfo = CType(DropDownPortal.SelectedItem, PortalInfo)
                If portal Is Nothing Then Exit Sub
                'SetProgress(portal.PortalDescription, -1)

                ' test AlternateURLs
                m_refreshed = False
                portal.Connect()

                While portal.CurrentPortalURL <> ""

                    Try
                        If IsTestUpdatePortalsXML(portal.PortalURL) Then
                            Dim pguid As String = portal.PortalGuid
                            Dim b As Boolean = LCase(portal.CurrentPortalURL.TrimEnd("\/".ToCharArray)) = LCase(m_StartupURL).TrimEnd("\/".ToCharArray) OrElse LCase(portal.PortalURL.TrimEnd("\/".ToCharArray)) = LCase(m_StartupURL).TrimEnd("\/".ToCharArray)
                            MergeDownloadPortalsXML(portal, b)
                            portal = CType(DropDownPortal.SelectedItem, PortalInfo)

                            If portal Is Nothing Then Exit Sub
                            If m_notfound Then
                                portal.CurrentURL = portal.CurrentPortalURL
                                Exit While
                            End If
                            If m_refreshed Then Exit While
                        End If
                    Catch
                    End Try
                    portal.Failover()
                End While

                UpdateSelectedImage(portal)

            Catch ex As Exception ' ignore all errors 
                Log("Startup portal changed: ", ex)
            Finally
                m_busy = False
                m_verybusy = False
                m_droppedDown = DropDownPortal.DroppedDown
                UpdateUI()
            End Try
        End Sub

        Private m_droppedDown As Boolean = False
        Private Sub DropDownPortal_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownPortal.DropDown
            m_droppedDown = True
        End Sub
        Private Sub LoginForm_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
            Log("Exiting startup process... (Form closed)")
            System.Windows.Forms.Application.Exit()
        End Sub
#End Region

#Region "Add portals..."
        Private Function AddPortal() As Boolean
            If m_busy Then
                Exit Function
            End If
            Dim f As New AddPortal
            f.Owner = Me
            Me.Show()
            f.DesktopLocation = New Point(Me.DesktopLocation.X, Me.DesktopLocation.Y)
            f.ShowDialog()
            Return False
        End Function

        Private Sub SyncNode(ByVal xmldoc As Xml.XmlDocument, ByVal newdoc As Xml.XmlDocument, ByVal path As String, ByVal element As String, Optional ByVal searchpath As String = "", Optional ByVal Overwrite As Boolean = False)
            Dim kid As Xml.XmlElement
            Dim node1 As Xml.XmlNode
            Dim node2 As Xml.XmlNode
            If searchpath = "" Then
                searchpath = path + "/" + element
            End If
            node1 = newdoc.DocumentElement.SelectSingleNode(searchpath)
            node2 = xmldoc.DocumentElement.SelectSingleNode(searchpath)

            If Not node1 Is Nothing Then
                If node2 Is Nothing Then
                    kid = xmldoc.CreateElement(element)
                    With xmldoc.DocumentElement.SelectSingleNode(path)
                        If .SelectSingleNode(element) Is Nothing Then
                            .PrependChild(kid)
                        Else
                            .AppendChild(kid)
                        End If
                        .AppendChild(xmldoc.CreateWhitespace(vbCrLf & vbCrLf))
                    End With
                    kid.InnerXml = node1.InnerXml
                ElseIf Overwrite Then
                    node2.InnerXml = node1.InnerXml
                End If
            End If
        End Sub

        Private Sub AssertNodeExists(ByVal xmldoc As Xml.XmlDocument, ByVal newdoc As Xml.XmlDocument, ByVal path As String, ByVal element As String)
            Dim kid As Xml.XmlElement
            Dim node1 As Xml.XmlNode
            Dim node2 As Xml.XmlNode
            node1 = newdoc.DocumentElement.SelectSingleNode(path + "/" + element)
            node2 = xmldoc.DocumentElement.SelectSingleNode(path + "/" + element)

            If node2 Is Nothing Then
                kid = xmldoc.CreateElement(element)
                xmldoc.DocumentElement.SelectSingleNode(path).PrependChild(kid)
                If Not node1 Is Nothing Then
                    kid.InnerXml = node1.InnerXml
                End If
            End If
        End Sub
        Friend Sub UpdateXML(ByVal filename As String, ByVal replaceLocalSettings As Boolean, ByVal replaceLocalStatupSettings As Boolean, Optional ByVal UpdateFileOnly As Boolean = False, Optional ByVal discoveryCode As String = "")
            Dim selectedGuid As String = ""
            Try
                Dim newdoc As New Xml.XmlDocument
                newdoc.PreserveWhitespace = True
                newdoc.Load(filename)

                Try
                    Dim node As Xml.XmlNode = newdoc.DocumentElement.SelectSingleNode("Portals")
                    If Not node Is Nothing Then
                        Try
                            selectedGuid = LCase(node.SelectSingleNode("Portal/Guid").InnerText())
                        Catch
                        End Try
                    End If

                Catch ex As Exception

                End Try

                Dim OrigFilename As String = System.Windows.Forms.Application.StartupPath + "\Portals.xml"
                If Not IO.File.Exists(OrigFilename) Then
                    IO.File.Copy(filename, OrigFilename)
                    Exit Try
                End If
                Dim xmlDoc As New Xml.XmlDocument
                xmlDoc.PreserveWhitespace = True
                xmlDoc.Load(OrigFilename)

                SyncNode(xmlDoc, newdoc, "/Configuration", "Startup", , replaceLocalStatupSettings)
                AssertNodeExists(xmlDoc, newdoc, "/Configuration", "Portals")

                For Each node As Xml.XmlNode In newdoc.DocumentElement.SelectNodes("/Configuration/Portals/Portal")

                    If GetXmlValue(node, "DiscoveryCode", "") = "" AndAlso discoveryCode <> "" Then
                        Dim el As Xml.XmlElement = newdoc.CreateElement("DiscoveryCode")
                        el.InnerText = discoveryCode
                        node.AppendChild(el)
                        'ElseIf GetXmlValue(node, "DiscoveryCode", "") <> "" AndAlso discoveryCode <> "" Then
                        '    Dim el As Xml.XmlElement = CType(node.SelectSingleNode("DiscoveryCode"), Xml.XmlElement)
                        '    el.InnerText = discoveryCode
                    End If
                    SyncNode(xmlDoc, newdoc, "/Configuration/Portals", "Portal", "/Configuration/Portals/Portal[Guid='" & GetXmlValue(node, "Guid", "").ToLower & "']", replaceLocalSettings OrElse GetXmlValue(node, "Guid", "").ToLower = selectedGuid)

                Next
                xmlDoc.Save(OrigFilename)

            Catch ex As Exception
                Throw

            End Try
            If UpdateFileOnly Then Exit Sub
            Dim lastGuid As String = selectedGuid
            If lastGuid = "" AndAlso Not m_lastSelection Is Nothing Then
                lastGuid = CType(m_lastSelection, PortalInfo).PortalGuid
            End If
            RefreshPortalInfo(lastGuid)
            If lastGuid <> "" Then
                For i As Integer = 0 To DropDownPortal.Items.Count - 1
                    If TypeOf DropDownPortal.Items(i) Is PortalInfo Then
                        If CType(DropDownPortal.Items(i), PortalInfo).PortalGuid = lastGuid Then
                            DropDownPortal.SelectedIndex = i
                            Exit For
                        End If
                    End If
                Next
            Else
                DropDownPortal.SelectedIndex = 0
            End If

        End Sub

        Private Sub RemovePortal()
            Try
                With CType(Me.DropDownPortal.SelectedItem, PortalInfo)
                    RemovePortalFromXML(.PortalGuid)

                    Me.DropDownPortal.Items.RemoveAt(Me.DropDownPortal.SelectedIndex)
                    Me.DropDownPortal.SelectedIndex = 0
                End With
            Catch ex As Exception
                MsgBox(GetCaption(ex.Message), MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical)
            End Try
        End Sub

        Private Sub RemovePortalFromXML(ByVal guid As String)
            Dim OrigFilename As String = System.Windows.Forms.Application.StartupPath + "\Portals.xml"
            If IO.File.Exists(OrigFilename) Then
                Try
                    Dim xmlDoc As New Xml.XmlDocument
                    xmlDoc.PreserveWhitespace = True
                    xmlDoc.Load(OrigFilename)

                    Dim node As Xml.XmlNode = xmlDoc.DocumentElement.SelectSingleNode("Portals")
                    For Each n As Xml.XmlNode In node.SelectNodes("Portal/Guid")
                        If LCase(GetXmlValue(n, "")) = LCase(guid) Then

                            Dim connstr As String = GetXmlValue(n.ParentNode, "ConnectionURL", "")
                            Dim ns As Xml.XmlNode = xmlDoc.DocumentElement.SelectSingleNode("Startup")
                            Dim stconnstr As String = GetXmlValue(ns, "UpdateURL", "")
                            If LCase(connstr) = LCase(stconnstr) Then
                                ' remove startup
                                ns.ParentNode.RemoveChild(ns)
                            End If

                            n.ParentNode.ParentNode.RemoveChild(n.ParentNode)
                            Exit For
                        End If
                    Next

                    xmlDoc.Save(OrigFilename)
                Catch ex As Exception
                    Throw New BW2Exception("Internal error occurred while updating configuration file Portals.xml.", ex)
                End Try
            End If
        End Sub

#End Region

#Region "Auto-Complete name..."

        Private m_pwdIsIS As Boolean = False

        Private Sub SetIntegratedSecurity()
            TextBoxPassword.Text = GetCaption("Windows Integrated Security")
            m_pwdIsIS = True
            TextBoxPassword.PasswordField = Nothing
            TextBoxPassword.Enabled = False
        End Sub

        Private Sub UpdateIntegratedSecurity()
            If TextBoxUsername.Text = "" Then
                SetIntegratedSecurity()
            Else
                If m_pwdIsIS = True Then
                    TextBoxPassword.Text = ""
                    m_pwdIsIS = False
                End If
                TextBoxPassword.PasswordField = CChar("*")
                TextBoxPassword.Enabled = TextBoxUsername.Enabled
            End If
        End Sub

        Dim m_UsernameChanging As Boolean = False
        Private Sub TextBoxUsername_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBoxUsername.TextChanged
            UpdateIntegratedSecurity()
            If Not m_UsernameChanging Then
                m_UsernameChanging = True
                Try
                    Dim s As String = TextBoxUsername.Text
                    If m_HistoryEntries.ContainsKey(s) Then Exit Sub ' exact match
                Catch ex As Exception
                Finally
                    m_UsernameChanging = False
                End Try
            End If
        End Sub
        Private Sub TextBoxUsername_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxUsername.KeyDown
            If e.KeyCode = Keys.Back OrElse e.KeyCode = Keys.Clear OrElse e.KeyCode = Keys.Delete Then
                m_UsernameChanging = True
            End If
        End Sub
        Private Sub TextBoxUsername_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxUsername.KeyUp
            m_UsernameChanging = False
        End Sub
#End Region

#Region "Translations..."

        Private m_transTree As Hashtable

        Private Sub ReadTranslations(ByVal ht As Hashtable, ByVal langNode As Xml.XmlElement)
            For Each node As Xml.XmlElement In langNode.GetElementsByTagName("data")
                Dim name As String = node.Attributes("name").Value
                Dim val As String = node.GetElementsByTagName("value")(0).InnerText
                val = Replace(val, vbTab, "")
                ht(name.ToLower) = val
            Next
        End Sub
        Private Sub BuildTranslationTree()
            If m_transTree Is Nothing Then
                m_transTree = New Hashtable
                Dim xd As New Xml.XmlDocument
                xd.Load(System.Windows.Forms.Application.StartupPath + "\Translations.xml")

                For Each node As Xml.XmlElement In xd.DocumentElement.GetElementsByTagName("language")
                    Dim langCode As String = node.Attributes("code").Value
                    Dim ht As New Hashtable
                    If langCode = "" OrElse langCode.ToLower = "default" Then
                        langCode = "neutral"
                    End If
                    m_transTree(langCode.ToLower) = ht
                    ReadTranslations(ht, node)
                Next
            End If
        End Sub

        Friend Function GetCaption(ByVal key As String) As String
            Try
                BuildTranslationTree()
                Dim ht As Hashtable
                Dim cult As System.Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture
                While Not cult Is System.Globalization.CultureInfo.InvariantCulture
                    ht = CType(m_transTree(cult.Name.ToLower), Hashtable)
                    If ht Is Nothing Then
                        ht = CType(m_transTree(cult.TwoLetterISOLanguageName.ToLower), Hashtable)
                    End If
                    If Not ht Is Nothing Then
                        Dim val As String = CStr(ht(key.ToLower))
                        If Not val Is Nothing Then
                            Return val
                        End If
                    End If
                    cult = cult.Parent
                End While
                ht = CType(m_transTree("neutral"), Hashtable)
                If Not ht Is Nothing Then
                    Dim val As String = CStr(ht(key.ToLower))
                    If Not val Is Nothing Then
                        Return val
                    End If
                End If


            Catch ex As Exception
            End Try

            key = Replace(key, "@LF ", vbCrLf, , , CompareMethod.Text)
            key = Replace(key, "@LF", vbCrLf, , , CompareMethod.Text)
            Return key
        End Function

        Friend Sub Retranslate()
            m_transTree = Nothing
            Try
                Me.ButtonOK.Text = GetCaption("Login")
                Me.LabelUsername.Text = GetCaption("Username")
                Me.LabelPassword.Text = GetCaption("Password")
                Me.LabelPortal.Text = GetCaption("Portal")
                Me.Text = GetCaption("Login")

                Me.MenuItemClearCache.Text = GetCaption("Clear all local data")
                Me.MenuItemAddPortals.Text = GetCaption("Add more portals...")
                Me.MenuItemRemovePortal.Text = GetCaption("Remove Portal...")
                Me.MenuItemShowLog.Text = GetCaption("Show logfile")

            Catch
            End Try
        End Sub
#End Region

#Region "Internal data and history initialisation..."

        Private Const EncodeHTML_Begin As String = "<html><body><binary>"
        Private Const EncodeHTML_End As String = "</binary></body></html>"
        Public Sub LogMessage(ByVal Message As String)
            Try
                If Not System.Diagnostics.EventLog.SourceExists("BW2Startup") Then
                    System.Diagnostics.EventLog.CreateEventSource("BW2Startup", "Application")
                End If
                System.Diagnostics.EventLog.WriteEntry("BW2Startup", Message, System.Diagnostics.EventLogEntryType.Warning)
            Catch : End Try
        End Sub

        Private Function ExToText(ByVal ex As Exception) As String
            Dim adi As String = ""
            If TypeOf ex Is WebException Then
                With CType(ex, WebException)
                    adi = " Status=" & .Status & " (" & .Status.ToString & ")"
                    If TypeOf .Response Is HttpWebResponse Then
                        adi += " StatusCode=" & CType(.Response, HttpWebResponse).StatusCode & " (" & CType(.Response, HttpWebResponse).StatusCode.ToString & ")"
                    End If
                End With

            End If

            Return ex.GetType.Name & " " & ex.Message & adi
        End Function

        Friend Sub Log(ByVal line As String, Optional ByVal ex As Exception = Nothing)
            Dim fs As FileStream = Nothing
            Dim d As Date = Date.Now
            Dim msg As String = d.ToShortDateString & " " & d.ToString("HH:mm:ss.fff") & " " & line
            If Not ex Is Nothing Then
                Dim sb As New System.Text.StringBuilder
                sb.Append(ExToText(ex))
                If Not ex.InnerException Is Nothing Then
                    sb.Append(": ")
                    sb.Append(ExToText(ex.InnerException))
                End If
                sb.Append(" ")
                msg += " " & sb.ToString
            End If
            For tries As Integer = 0 To 10
                Try
                    AssertPathExists("Data/Tracer")
                    Dim filename As String = "Data/Tracer/startup.log"
                    If File.Exists(filename) Then
                        If File.GetLastWriteTime(filename).CompareTo(Date.Today) < 0 Then
                            ' too old
                            fs = New FileStream(filename, FileMode.Truncate, FileAccess.Write, FileShare.Read)
                        Else
                            fs = New FileStream(filename, FileMode.Append, FileAccess.Write, FileShare.Read)
                        End If
                    Else
                        fs = New FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.Read)
                    End If
                    Exit For
                Catch ex2 As IOException When tries = 3

                    LogMessage(msg)
                    Exit Sub
                Catch ex2 As IOException
                    Threading.Thread.Sleep(100)

                End Try
            Next

            If fs.Position = 0 Then
                Dim b1 As Byte() = System.Text.Encoding.UTF8.GetPreamble
                fs.Write(b1, 0, b1.Length)
            End If
            Dim b As Byte() = System.Text.Encoding.UTF8.GetBytes(msg & vbCrLf)
            fs.Write(b, 0, b.Length)
            fs.Close()
        End Sub

        Private Sub LogLine(ByVal line As String)
            Log(line)
        End Sub

        Private m_refreshingPI As Boolean = False

        Private Sub RefreshPortalInfo(Optional ByVal selectedGuid As String = "")
            ReadPortalInfo()
            m_refreshingPI = True
            DropDownPortal.Items.Clear()
            DropDownPortal.Sorted = True
            Dim p As PortalInfo
            Dim ixSel As Integer = -1

            For Each p In m_Portals
                Dim ix As Integer = DropDownPortal.Items.Add(p)
            Next
            DropDownPortal.Sorted = False
            Dim pi As PortalInfo = Nothing
            If selectedGuid <> "" Then
                For ix As Integer = 0 To DropDownPortal.Items.Count - 1
                    pi = CType(DropDownPortal.Items(ix), PortalInfo)
                    If pi.PortalGuid = selectedGuid Then
                        ixSel = ix
                        Exit For
                    End If
                Next
            End If
            DropDownPortal.Items.Add(GetCaption("Add more portals..."))
            m_refreshingPI = False
            If ixSel >= 0 Then
                DropDownPortal.SelectedIndex = ixSel
            End If
        End Sub

        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
            If m_Credentials Is Nothing Then
                m_Credentials = New Credentials(System.Windows.Forms.Application.StartupPath & "\Data\Credentials\" & Environment.UserName.ToLower & ".dat")
                m_Credentials.Load()
            End If

            RefreshPortalInfo()
            LoadHistory()
            Retranslate()

            If m_Portals.Count = 0 Then
                AddPortal()
                RefreshPortalInfo()
            End If

        End Sub

        Private m_PortalGuidLookup As New Hashtable
        Private m_HistoryEntries As Hashtable = Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable
        Private m_LastUser As String = ""
        Private m_Portals As New ArrayList
        Private m_StartupURL As String = ""
        Private m_UseProxy As Boolean = True
        Private m_Proxy As String = ""
        Private m_CurrentStartupURL As String = ""
        Private m_StartupAlternateURLs As New ArrayList
        Private m_ShowProgress As Boolean = False

        Private Function OnLoadBackgroundPortal(ByVal portal As PortalInfo, ByVal logoName As String) As String
            Dim imgpathLocal As String = "/Data/Objects/" & portal.PortalGuid & "/" & logoName
            Try
                If IO.File.Exists(System.Windows.Forms.Application.StartupPath & imgpathLocal) Then
                    Me.BackgroundImage = Image.FromFile(System.Windows.Forms.Application.StartupPath & imgpathLocal)
                    Me.BackgroundImageLayout = ImageLayout.Stretch
                Else
                    Me.BackgroundImage = My.Resources.loginNew
                    Me.BackgroundImageLayout = ImageLayout.Stretch
                End If
            Catch ex As Exception
                Me.BackgroundImage = My.Resources.loginNew
                Me.BackgroundImageLayout = ImageLayout.Stretch
            End Try

        End Function
        Private Function GetXmlValue(ByVal node As Xml.XmlNode, ByVal name As String, ByVal defaultval As String) As String
            If node Is Nothing Then Return defaultval
            node = node.SelectSingleNode(name)
            If node Is Nothing Then Return defaultval
            Return node.InnerText
        End Function
        Private Function GetXmlValue(ByVal node As Xml.XmlNode, ByVal defaultval As String) As String
            If node Is Nothing Then Return defaultval
            Return node.InnerText
        End Function

        Private Function NormalizeURL(ByVal url As String) As String
            If url = "" Then Return url
            url = url.Replace("\", "/")
            url = System.Text.RegularExpressions.Regex.Replace(url, "/bin/\S*\.aspx", "/")
            If Not url.EndsWith("/") Then
                url += "/"
            End If

            Return url

        End Function

        Private m_refreshed As Boolean = False
        Private m_testedURI As Hashtable = Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable

        Private Function IsTestUpdatePortalsXML(ByVal uri As String) As Boolean
            If Trim(uri) = "" Then Return False
            Return True
        End Function
        Private Function IsTestUpdatePortals(ByVal p As PortalInfo) As Boolean
            Return True
        End Function
        Private Sub ReadServerInfo()

            Dim Filename As String = System.Windows.Forms.Application.StartupPath + "\Server.BW2info"
            If Not IO.File.Exists(Filename) Then
                Exit Sub
            End If
            Dim b() As Byte

            Dim fs As FileStream
            Try
                fs = New FileStream(Filename, FileMode.Open, FileAccess.Read)
            Catch e As Exception
                Log("Unable to open " & Filename, e)
                Exit Sub
            End Try

            Dim cp As New BW2Technologies.BW2Global.Security.CryptoProvider
            ReDim b(CInt(fs.Length) - 1)
            fs.Read(b, 0, CInt(fs.Length))
            fs.Close()

            b = cp.Decrypt(b, ms_CryptoKey)


            Dim i As Integer
            Dim ms As New MemoryStream(b)
            Dim br As New BinaryReader(ms)
            Try

                Dim cnt As Integer = br.ReadInt32
                For i = 1 To cnt

                    Dim Version As Integer = br.ReadInt32
                    Dim connectstring As String
                    Dim publicstr As String = ""
                    Dim DatabaseName As String
                    If Version = 0 Then
                        Dim ServerName As String = br.ReadString
                        DatabaseName = br.ReadString
                        Dim DatabaseUserID As String = br.ReadString
                        Dim DatabasePassword As String = br.ReadString
                        connectstring = "provider=sqloledb; data source = " + ServerName
                        connectstring += "; initial catalog=" + DatabaseName
                        connectstring += "; password=" + DatabasePassword
                        connectstring += "; user id=" + DatabaseUserID + ";"
                        publicstr = ServerName & " " & DatabaseName
                    Else
                        connectstring = br.ReadString
                        publicstr = ""
                    End If
                    Dim p As PortalInfo
                    Try

                        Dim dbc As New System.Data.OleDb.OleDbConnection(connectstring)
                        dbc.Open()
                        Try
                            Dim dbcmd As IDbCommand = dbc.CreateCommand()
                            dbcmd.CommandText = "SELECT tbw2Items.Item_Guid FROM tbw2PortalInfo INNER JOIN tbw2Items ON tbw2PortalInfo.PortalInfo_ID = tbw2Items.Item_ID"
                            Dim portalGuid As Guid = CType(dbcmd.ExecuteScalar(), System.Guid)
                            Dim pg As String = portalGuid.ToString.ToLower
                            For Each p In m_Portals
                                If p.PortalGuid = pg Then
                                    p.PortalDBConnectString = connectstring
                                End If
                            Next
                        Catch ex As Exception
                            Log("error connecting to " & publicstr, ex)
                        Finally
                            dbc.Close()
                            dbc.Dispose()
                            dbc = Nothing
                        End Try


                    Catch e As Exception
                        Log("Exception while reading portal info: " & e.ToString, e)
                    End Try

                Next i

            Finally
                br.Close()
            End Try
        End Sub

        Private SimulateSlowConnection As Integer = 0

        Private Sub ReadPortalInfo()
            Dim bRef As Boolean = m_refreshed

            m_PortalGuidLookup.Clear()
            m_Portals.Clear()
            m_StartupAlternateURLs.Clear()

            Dim Filename As String = System.Windows.Forms.Application.StartupPath + "\Portals.xml"
            Try
                Dim xmlDoc As New Xml.XmlDocument
                xmlDoc.Load(Filename)
                If xmlDoc.DocumentElement.Name = "Configuration" Then
                    Try
                        SimulateSlowConnection = 0
                        For Each e As Xml.XmlElement In xmlDoc.DocumentElement.GetElementsByTagName("SimulateSlowConnection")
                            SimulateSlowConnection = CInt(GetXmlValue(e, "0"))
                        Next
                        If SimulateSlowConnection > 1000 Then SimulateSlowConnection = 1000

                        If SimulateSlowConnection > 0 Then
                            Log("Simulating slow connection by adding " & SimulateSlowConnection & " ms to each request")
                        End If
                    Catch
                        SimulateSlowConnection = 0
                    End Try
                    m_StartupURL = GetXmlValue(xmlDoc.DocumentElement.SelectSingleNode("/Configuration/Startup"), "UpdateURL", "")
                    m_StartupURL = NormalizeURL(m_StartupURL)
                    Try
                        m_UseProxy = True
                        m_UseProxy = CBool(GetXmlValue(xmlDoc.DocumentElement.SelectSingleNode("/Configuration/Startup"), "ProxyEnabled", "true"))
                    Catch
                    End Try
                    Try
                        m_Proxy = ""
                        m_Proxy = GetXmlValue(xmlDoc.DocumentElement.SelectSingleNode("/Configuration/Startup"), "Proxy", "")
                    Catch
                    End Try

                    m_CurrentStartupURL = m_StartupURL

                    Dim ur As New Uri(m_StartupURL)
                    If ur.DnsSafeHost.EndsWith(".bw2hosting.ch", StringComparison.InvariantCultureIgnoreCase) Then
                        AutoCompleteURLs(m_StartupURL, m_StartupAlternateURLs, "")
                        If m_StartupAlternateURLs.Count > 0 Then
                            m_CurrentStartupURL = CStr(m_StartupAlternateURLs(0))
                        End If
                    Else
                        If m_StartupURL <> "" Then
                            m_StartupAlternateURLs.Add(m_StartupURL)
                        End If
                        Try
                            Dim n1 As Xml.XmlNode = xmlDoc.DocumentElement.SelectSingleNode("/Configuration/Startup/AlternateURLs")
                            If Not n1 Is Nothing Then
                                Dim altURLNodes As Xml.XmlNodeList = n1.ChildNodes
                                For Each altNode As Xml.XmlNode In altURLNodes
                                    If altNode.Name = "URL" Then
                                        Dim s As String = GetXmlValue(altNode, "")
                                        If s <> "" Then
                                            m_StartupAlternateURLs.Add(NormalizeURL(s))
                                        End If
                                    End If
                                Next
                            End If
                        Catch
                        End Try
                    End If

                    Dim portalNodes As Xml.XmlNodeList = xmlDoc.DocumentElement.SelectSingleNode("Portals").ChildNodes
                    Dim node As Xml.XmlNode
                    Dim htURI As Hashtable = Collections.Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable

                    For Each node In portalNodes
                        If node.Name = "Portal" Then
                            Try
                                Dim p As New PortalInfo

                                p.PortalGuid = node.SelectSingleNode("Guid").InnerText()
                                p.PortalDisplayName = node.SelectSingleNode("DisplayName").InnerText()
                                p.PortalURL = GetXmlValue(node, "ConnectionURL", "")

                                p.PortalURL = NormalizeURL(p.PortalURL)

                                htURI(p.PortalURL.TrimEnd("\/".ToCharArray)) = p

                                p.CurrentURL = p.PortalURL

                                Dim AutoUpdate As String = GetXmlValue(node, "AutoUpdate", "true").ToLower
                                If AutoUpdate = "true" OrElse AutoUpdate = "enabled" Then
                                    p.PortalAutoUpdate = True
                                Else
                                    p.PortalAutoUpdate = False
                                End If
                                p.PortalDescription = GetXmlValue(node, "Description", "")
                                Try
                                    p.PortalDiscoveryCode = GetXmlValue(node, "DiscoveryCode", "")
                                Catch ex As Exception
                                End Try
                                Try
                                    p.PortalUse64Bit = CBool(GetXmlValue(node, "Use64Bit", "1"))
                                Catch ex As Exception
                                End Try
                                Try
                                    Dim v As String = GetXmlValue(node, "Use32Bit", "")
                                    If v <> "" Then
                                        p.PortalUse64Bit = Not CBool(v)
                                    End If
                                Catch ex As Exception
                                End Try
                                Try
                                    p.AutoLoginOnWindowsDomain = CStr(GetXmlValue(node, "AutoLoginOnWindowsDomain", ""))
                                Catch ex As Exception
                                End Try

                                ur = New Uri(p.PortalURL)
                                If Not LCase(ur.DnsSafeHost).EndsWith(".bw2hosting.ch") Then
                                    If p.PortalURL <> "" Then
                                        p.PortalAlternateURLs.Add(p.PortalURL)
                                    End If
                                    Try
                                        Dim aun As Xml.XmlNode = node.SelectSingleNode("AlternateURLs")
                                        If Not aun Is Nothing Then
                                            Dim altURLNodes As Xml.XmlNodeList = aun.ChildNodes
                                            For Each altNode As Xml.XmlNode In altURLNodes
                                                If altNode.Name = "URL" Then
                                                    Dim s As String = GetXmlValue(altNode, "")
                                                    If s <> "" Then
                                                        p.PortalAlternateURLs.Add(NormalizeURL(s))
                                                    End If
                                                End If
                                            Next
                                        End If
                                    Catch
                                    End Try
                                Else
                                End If

                                m_Portals.Add(p)
                                m_PortalGuidLookup.Add(p.PortalGuid.ToLower, p)
                            Catch ex As Exception
                                Log("Configuration of " & GetXmlValue(node, "DisplayName", "'<DisplayName> missing'"), ex)
                            End Try
                        End If
                    Next
                    If Not bRef Then
                        Dim bUpdateStartup As Boolean = False
                        If Not htURI.ContainsKey(m_StartupURL.TrimEnd("\/".ToCharArray)) Then
                            bUpdateStartup = True
                        End If
                        For Each p As PortalInfo In m_Portals
                            If IsTestUpdatePortals(p) Then
                                Dim b As Boolean = bUpdateStartup AndAlso LCase(p.PortalURL.TrimEnd("\/".ToCharArray)) = LCase(m_StartupURL).TrimEnd("\/".ToCharArray)
                                If b Then ' startup portal only
                                    MergeDownloadPortalsXML(p, b)
                                End If
                                If m_refreshed Then bUpdateStartup = False
                            End If
                        Next
                    End If
                End If
            Catch ex As FileNotFoundException
                Log("Configuration missing", ex)
            Catch ex As Exception
                Log("Configuration error", ex)
                MsgBox(GetCaption("Internal error occurred while reading configuration file Portals.xml."), MsgBoxStyle.SystemModal, GetCaption("Login"))
                Log("Exiting startup process... (aborted)")
                System.Environment.Exit(1)
            End Try

            If Not bRef AndAlso m_refreshed Then
                Log("Portals.xml configuration has been updated.")
                Me.ReadPortalInfo()
            End If

            ReadServerInfo()
        End Sub

        Private Sub AutoCompleteURLs(ByVal url As String, ByVal AlternateURLs As ArrayList, ByVal portalGuid As String)
            Dim uri1 As New Uri(url)
            Dim hn As String = LCase(uri1.DnsSafeHost)
            'AndAlso Not hn.EndsWith("a.bw2hosting.ch") AndAlso Not hn.EndsWith("b.bw2hosting.ch") _
            If hn.StartsWith("ap") AndAlso hn.EndsWith(".bw2hosting.ch") AndAlso Not hn.EndsWith("i.bw2hosting.ch") _
            AndAlso (AlternateURLs.Count = 0 OrElse (AlternateURLs.Count = 1 AndAlso CStr(AlternateURLs(0)) = url)) Then
                Dim endings As String() = {"", "i", "a", "b"}
                Dim timeouts As Integer() = {1000, 1000, 500, 300}
                Dim alt As New ArrayList
                For j As Integer = 0 To endings.GetUpperBound(0)
                    Dim ending As String = endings(j)
                    Dim u As String = System.Text.RegularExpressions.Regex.Replace(url, "(ap\d*)[abi]?\.bw2hosting\.ch", "$1" & ending & ".bw2hosting.ch", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
                    Try
                        Dim ur As New Uri(u)
                        DNSGetHostEntry(ur.DnsSafeHost, timeouts(j), False)
                        alt.Clear()
                        alt.Add(u)
                        For tr As Integer = 1 To 2
                            Try
                                If portalGuid <> "" Then
                                    Test(u, alt, "ping.bw2?action=PING&portal=" & portalGuid)
                                    Exit For
                                Else
                                    Test(u, alt, "ping.bw2?action=PING")
                                    Exit For
                                End If
                            Catch ex As Exception When ex.Message = "retry"
                            End Try
                        Next
                        AlternateURLs.Add(u) ' valid DNS
                    Catch ex As Exception
                        ' unknown uri
                        Log("Skipping invalid host name: " & u & " (" & ex.Message & ")")
                    End Try
                Next
            End If
        End Sub


        Private m_notfound As Boolean = False
        Private Sub MergeDownloadPortalsXML(ByVal uri As String, Optional ByVal ReplaceStartup As Boolean = False)
            m_notfound = False
            Dim completions As String() = {"download"}
            Dim cold As Cursor = Me.Cursor
            Me.Cursor = Cursors.WaitCursor

            uri = uri.TrimEnd("/".ToCharArray)
            uri = uri.TrimEnd("\".ToCharArray)

            If m_testedURI.ContainsKey(uri) Then
                Exit Sub
            End If


            For Each comp As String In completions

                For tries As Integer = 1 To 2
                    Try
                        Do
                            Try
                                If comp <> "" Then comp = "/" + comp

                                Dim fn As String = System.Windows.Forms.Application.StartupPath & "\Remote_Portals_1.xml"

                                DownloadFile(uri + comp + "/Update_Portals.xml", fn, , , 3000 + (tries - 1) * 5000, False, "MergeDownloadPortalsXML")

                                m_testedURI(uri) = True
                                Me.Cursor = Cursors.Default
                                m_refreshed = True

                                UpdateXML(fn, True, ReplaceStartup, True)
                                IO.File.Delete(fn)
                                Me.Cursor = cold
                                Exit Sub
                            Catch ex As BW2Global.BW2Exception When ex.Message = "retry"
                            End Try
                        Loop
                    Catch ex As Net.WebException When TypeOf ex.Response Is HttpWebResponse AndAlso CType(ex.Response, HttpWebResponse).StatusCode = HttpStatusCode.NotFound
                        m_testedURI(uri) = True
                        m_notfound = True
                        Exit For


                    Catch ex As Net.WebException
                        Me.Log("Error while downloading in MergeDownloadPortalsXML: " & ex.Message, ex)
                        Me.Cursor = cold
                        If tries = 2 Then Exit Sub

                    Catch ex As BW2ConnectFailedCancelException
                        Me.Log("Connect failed (cancel) in MergeDownloadPortalsXML: " & ex.Message, ex)
                        Me.Cursor = cold
                        Exit Sub

                    Catch ex As BW2ConnectFailedException
                        Me.Log("Connect failed in MergeDownloadPortalsXML: " & ex.Message, ex)
                        Me.Cursor = cold
                        If tries = 2 Then Exit Sub

                    Catch ex As Exception
                        Me.Cursor = Cursors.Default
                        Me.Log("Unexpected error in MergeDownloadPortalsXML: " & ex.Message, ex)
                        MsgBox(ex.Message, MsgBoxStyle.SystemModal)
                        Me.Cursor = cold
                        Exit Sub
                    End Try
                Next
            Next
            Me.Cursor = cold
        End Sub
        Private Sub MergeDownloadPortalsXML(ByVal p As PortalInfo, Optional ByVal ReplaceStartup As Boolean = False)
            m_notfound = False
            Dim completions As String() = {"download"}
            Dim cold As Cursor = Me.Cursor
            Me.Cursor = Cursors.WaitCursor
            Dim urltext As String = p.PortalURL
            Dim filename As String = "Update_Portals.xml"

            urltext = urltext.TrimEnd("/".ToCharArray)
            urltext = urltext.TrimEnd("\".ToCharArray)

            If m_testedURI.ContainsKey(urltext) Then
                Exit Sub
            End If
            m_testedURI(urltext) = False

            'If p.PortalDiscoveryCode <> "" Then
            '    Dim hi As New HostingInfo.HostingInfo
            '    Try
            '        Dim u As New Uri(hi.Url)
            '        DNSGetHostEntry(u.DnsSafeHost, 5000)
            '    Catch ex As Exception
            '        Me.DialogResult = Windows.Forms.DialogResult.None
            '        Me.LabelProgress.Text = GetCaption("Discovery.ErrorTitle") & ": " & GetCaption(ex.Message)
            '        Me.LabelProgress.Visible = True
            '        Exit Sub
            '    End Try

            '    Try
            '        hi.Timeout = 7000
            '        Dim s As String = hi.GetPortalUrlOrState(p.PortalDiscoveryCode)
            '        Dim us As String() = Split(s, ";")
            '        Dim cs As String() = Split(us(0), ":")
            '        If IsNumeric(cs(0)) AndAlso cs.Length = 2 Then
            '            'error code
            '            Me.Cursor = Cursors.Default
            '            Dim errtxt As String = GetCaption("Discovery." & cs(0))
            '            If errtxt.StartsWith("Discovery") Then
            '                errtxt = GetCaption(cs(1))
            '            End If
            '            Exit Try
            '        End If

            '        Dim mismatch As Boolean = False
            '        If p.PortalAlternateURLs.Count = us.Length Then
            '            For i As Integer = 0 To p.PortalAlternateURLs.Count - 1
            '                If Not us(i).TrimEnd("/\".ToCharArray).Equals(CStr(p.PortalAlternateURLs(i)).TrimEnd("/\".ToCharArray), StringComparison.InvariantCultureIgnoreCase) Then
            '                    mismatch = True
            '                    Exit For
            '                End If
            '            Next
            '        Else
            '            mismatch = True
            '        End If
            '        If Not mismatch Then
            '            m_testedURI(urltext) = True
            '            Exit Sub
            '        End If
            '        urltext = us(0)
            '        filename = "Portals.xml"

            '    Catch ex As Web.Services.Protocols.SoapException
            '        Me.DialogResult = Windows.Forms.DialogResult.None
            '        Log("Soap Error", ex)
            '        Exit Sub

            '    Catch ex As Exception
            '        Me.DialogResult = Windows.Forms.DialogResult.None
            '        Me.LabelProgress.Text = GetCaption("Discovery.ErrorTitle") & ": " & GetCaption(ex.Message)
            '        Me.LabelProgress.Visible = True
            '        Exit Sub
            '    End Try
            'End If

            For Each comp As String In completions

                For tries As Integer = 1 To 2
                    Try
                        Do
                            Try
                                If comp <> "" Then comp = "/" + comp

                                Dim fn As String = System.Windows.Forms.Application.StartupPath & "\Remote_Portals_1.xml"

                                DownloadFile(urltext & comp & "/" & filename, fn, , , 3000 + (tries - 1) * 5000, False, "MergeDownloadPortalsXML:" & p.PortalDisplayName)

                                m_testedURI(urltext) = True
                                Me.Cursor = Cursors.Default
                                m_refreshed = True

                                UpdateXML(fn, True, ReplaceStartup, True, p.PortalDiscoveryCode)
                                IO.File.Delete(fn)
                                Me.Cursor = cold
                                Exit Sub
                            Catch ex As BW2Global.BW2Exception When ex.Message = "retry"
                            End Try
                        Loop
                    Catch ex As Net.WebException When TypeOf ex.Response Is HttpWebResponse AndAlso CType(ex.Response, HttpWebResponse).StatusCode = HttpStatusCode.NotFound
                        m_testedURI(urltext) = True
                        m_notfound = True
                        Exit For


                    Catch ex As Net.WebException
                        Me.Log("Error while downloading in MergeDownloadPortalsXML: " & ex.Message, ex)
                        Me.Cursor = cold
                        If tries = 2 Then Exit Sub

                    Catch ex As BW2ConnectFailedCancelException
                        Me.Log("Connect failed (cancel) in MergeDownloadPortalsXML: " & ex.Message, ex)
                        Me.Cursor = cold
                        Exit Sub

                    Catch ex As BW2ConnectFailedException
                        Me.Log("Connect failed in MergeDownloadPortalsXML: " & ex.Message, ex)
                        Me.Cursor = cold
                        If tries = 2 Then Exit Sub

                    Catch ex As Exception
                        Me.Cursor = Cursors.Default
                        Me.Log("Unexpected error in MergeDownloadPortalsXML: " & ex.Message, ex)
                        MsgBox(ex.Message, MsgBoxStyle.SystemModal)
                        Me.Cursor = cold
                        Exit Sub
                    End Try
                Next
            Next
            Me.Cursor = cold
        End Sub

        Private Sub LoadHistory()

            Dim Filename As String = System.Windows.Forms.Application.StartupPath + "\History.dat"

            If Not File.Exists(Filename) Then Exit Sub

            Dim Last As String = ""
            Dim LastPortal As String = ""
            Try
                Dim fs As New FileStream(Filename, FileMode.Open, FileAccess.Read)
                Dim br As New BinaryReader(fs)
                Dim b As Byte() = br.ReadBytes(CInt(fs.Length))
                br.Close()
                fs.Close()

                Dim sec As New BW2Technologies.BW2Global.Security.CryptoProvider
                b = sec.Decrypt(b, "history")
                Dim ms As New MemoryStream(b, False)
                br = New BinaryReader(ms)
                Dim version As Integer = br.ReadInt32
                ' should be 1
                Dim cnt As Integer = br.ReadInt32, i As Integer
                Last = br.ReadString.ToLower
                LastPortal = br.ReadString.ToLower

                m_LastUser = Last

                For i = 1 To cnt
                    Dim PortalGuid As String = br.ReadString.ToLower
                    Dim LoginName As String = br.ReadString.ToLower

                    Dim p As PortalInfo = CType(m_PortalGuidLookup(PortalGuid), PortalInfo)
                    If Not p Is Nothing Then
                        m_HistoryEntries.Add(LoginName, p)
                    End If
                Next i

                br.Close()
                ms.Close()
            Catch
                File.Delete(Filename)
            End Try

        End Sub
        Private Sub SaveHistory()
            Dim Last As String = TextBoxUsername.Text
            Dim LastPortal As String = ""
            If TypeOf DropDownPortal.SelectedItem Is PortalInfo Then
                LastPortal = CType(DropDownPortal.SelectedItem, PortalInfo).PortalGuid
            End If
            If LastPortal <> "" Then
                If Last = "" Then
                    m_HistoryEntries(Environment.UserDomainName & "\" & Environment.UserName) = DropDownPortal.SelectedItem
                Else
                    m_HistoryEntries(Last) = DropDownPortal.SelectedItem
                End If
            End If
            Dim ms As New MemoryStream
            Dim bw As New BinaryWriter(ms)

            bw.Write(1)
            bw.Write(Me.m_HistoryEntries.Count)
            bw.Write(Last.ToLower)
            bw.Write(LastPortal.ToLower)

            Dim de As DictionaryEntry
            For Each de In m_HistoryEntries
                bw.Write(CType(de.Value, PortalInfo).PortalGuid.ToLower)
                bw.Write(CStr(de.Key).ToLower)
            Next de
            Dim sec As New BW2Technologies.BW2Global.Security.CryptoProvider
            Dim b() As Byte = sec.Encrypt(ms.ToArray, "history", BW2Technologies.BW2Global.Security.EncryptionMethod.RC2_128Bit, BW2Technologies.BW2Global.Security.CompressionMethod.None, BW2Technologies.BW2Global.Security.MessageAuthenticationMethod.None)

            bw.Close()
            ms.Close()

            Dim Filename As String = System.Windows.Forms.Application.StartupPath + "\History.dat"

            If b.Length = 0 Then
                If File.Exists(Filename) Then
                    File.Delete(Filename)
                End If
            End If

            Dim fs As New FileStream(Filename, FileMode.Create, FileAccess.Write)
            bw = New BinaryWriter(fs)

            bw.Write(b)

            bw.Close()
            fs.Close()
        End Sub

        Private Shared ms_CryptoKey() As Byte = New Byte() {17, 95, 30, 8, 67, 83, 165, 213, 2, 222}
        Private m_Credentials As Credentials

        Friend Class PortalInfo
            Implements IComparable

            Public PortalGuid As String
            Public PortalDisplayName As String
            ''' <summary>
            ''' the primary connection
            ''' </summary>
            Public PortalURL As String
            ''' <summary>
            ''' flag indicates whether the browser is updated automatically
            ''' </summary>
            Public PortalAutoUpdate As Boolean
            ''' <summary>
            ''' alternate connect URLs, used if the primary connection is broken
            ''' </summary>
            Public PortalAlternateURLs As New ArrayList

            Public PortalDiscoveryCode As String = ""
            Public PortalUse64Bit As Boolean = True

            Public AutoLoginOnWindowsDomain As String = ""

            Public PortalDescription As String = ""
            Friend PortalDBConnectString As String
            Public CurrentURL As String
            Public Overrides Function ToString() As String
                If Me.PortalDiscoveryCode = "" Then
                    Return Me.PortalDisplayName
                Else
                    Return Me.PortalDisplayName & " [" & Me.PortalDiscoveryCode & "]"
                End If
            End Function

            Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
                If TypeOf obj Is PortalInfo Then
                    Return String.Compare(Me.ToString, obj.ToString, True)
                ElseIf TypeOf obj Is String Then
                    Return -1
                End If
                Return -1
            End Function

            Private m_conn As New ArrayList
            Private m_connIx As Integer = 0
            Public Sub Connect()
                m_conn.Clear()

                LoginForm.Instance.AutoCompleteURLs(Me.PortalURL, Me.PortalAlternateURLs, Me.PortalGuid)
                If Me.PortalAlternateURLs.Count > 0 Then
                    Me.PortalURL = CStr(Me.PortalAlternateURLs(0))
                End If
                '  m_conn.Add(PortalURL)
                For i As Integer = 0 To PortalAlternateURLs.Count - 1
                    m_conn.Add(PortalAlternateURLs(i))
                Next
                m_connIx = 0
            End Sub

            Public Sub Failover()
                If m_connIx < m_conn.Count Then
                    m_connIx += 1
                End If
            End Sub

            Public Function CurrentPortalURL() As String
                If m_connIx < m_conn.Count Then
                    Return CStr(m_conn(m_connIx))
                End If
                Return ""
            End Function
        End Class

#End Region

#Region "Login and Update"
        Private proc As Process
        Private Const StartupVersion As Integer = 4

        Private Function ProceedLogin(ByVal portal As PortalInfo, ByVal user As String, ByVal pwd As String) As Boolean
            If Not CheckForUpdateStartup() Then Return False
            Do
                Dim path As String = "/Data/Objects/" & portal.PortalGuid & "/Assemblies"
                Dim exe As String = ExeNameAny()
                If portal.PortalAutoUpdate Then
                    Log("Requesting Browser updates...")
                    Do
                        Try
                            exe = UpdateBrowser(path, portal, user, pwd)
                            Exit Do
                        Catch ex As Exception When ex.Message = "retry"
                            Log("Retry browser download.")
                        Catch ex As System.Web.Services.Protocols.SoapException
                            Dim msg As String = ex.Message
                            If msg.IndexOf("---> ") >= 0 Then
                                msg = msg.Substring(msg.IndexOf("---> ") + 5)
                            End If
                            Log("UpdateBrowser", ex)
                            MsgBox(GetCaption(msg), MsgBoxStyle.SystemModal, GetCaption("Download failed"))
                            Return False
                        Catch ex As Exception
                            Log("UpdateBrowser", ex)
                            MsgBox(GetCaption(ex.Message), MsgBoxStyle.SystemModal, GetCaption("Download failed"))
                            Return False
                        End Try
                    Loop
                Else
                    Log("Using local browser, not updating...")
                    '                path = ""
                End If

                If exe = "!" Then
                    Return False

                ElseIf exe.StartsWith("!") Then
                    MsgBox(GetCaption(exe.Substring(1)), MsgBoxStyle.SystemModal, GetCaption("Download failed"))
                    Return False

                ElseIf Not File.Exists(System.Windows.Forms.Application.StartupPath & path & exe) Then
                    MsgBox(GetCaption("Download failed"), MsgBoxStyle.SystemModal, GetCaption("Download failed"))
                    If Directory.Exists(System.Windows.Forms.Application.StartupPath & path) Then
                        Directory.Delete(System.Windows.Forms.Application.StartupPath & path, True)
                    End If
                    Return False
                End If

                SaveHistory()

                DoEvents()
                Me.Hide()
                DoEvents()

                Dim s As String = """" & portal.PortalGuid & """ """ & user & """ """ & pwd & """ """ & System.Windows.Forms.Application.StartupPath & """ """ & portal.CurrentURL & """"
                Log("Starting browser with portal '" & portal.PortalDisplayName & "' as " & user)
                Dim pid As Integer = Shell(System.Windows.Forms.Application.StartupPath & path & exe & " " & s, AppWinStyle.NormalFocus)
                ' give child process time to start up before exiting
                Threading.Thread.Sleep(400)
                Log("Exiting startup process... (started process " & CStr(pid) & ")")
                Process.GetCurrentProcess.Kill()
            Loop While proc.ExitCode = 2
            Return True
        End Function

        Private Sub AssertPathExists(ByVal path As String, Optional ByVal isFilePath As Boolean = False)
            If isFilePath Then
                Dim fi As New FileInfo(path)
                If Not fi.Directory.Exists Then
                    fi.Directory.Create()
                End If
            ElseIf Not IO.Directory.Exists(path) Then
                IO.Directory.CreateDirectory(path)
            End If
        End Sub

        'Friend Function UpdateBrowser3(ByVal path As String, ByVal portal As PortalInfo, ByVal user As String, ByVal pwd As String) As String
        '    Dim localpath As String = Windows.Forms.Application.StartupPath
        '    Dim tmppath As String = Windows.Forms.Application.StartupPath & "/tmp_dld"
        '    Dim remotepath As String = portal.PortalUpdateURL
        '    Dim fIndex As String = path & "/dirinfo.txt"
        '    Dim f As String = fIndex
        '    Dim lastVersion As String = ""
        '    Try
        '        Dim fIndexCurrent As New FileStream(localpath & f, FileMode.Open, FileAccess.Read)
        '        Dim strIndexCurrent As New StreamReader(fIndexCurrent)
        '        Try
        '            lastVersion = strIndexCurrent.ReadLine()
        '        Finally
        '            strIndexCurrent.Close()
        '            fIndexCurrent.Close()
        '        End Try
        '    Catch ex As Exception
        '        Log("Error while reading local " & fIndex, ex)
        '    End Try

        '    Dim toRemove As New ArrayList()
        '    Dim toRename As New Specialized.StringDictionary()

        '    If Directory.Exists(tmppath) Then
        '        Directory.Delete(tmppath, True)
        '    End If
        '    AssertPathExists(tmppath & path, True)
        '    If Directory.Exists(localpath & path) Then
        '        Directory.Move(localpath & path, tmppath & path)
        '        toRename.Add(tmppath & path, localpath & path)
        '        AssertPathExists(localpath & path)
        '        If File.Exists(tmppath & fIndex) Then
        '            File.Copy(tmppath & fIndex, localpath & fIndex)
        '        End If
        '    End If
        '    Try
        '        Dim i As Integer
        '        Try
        '            AssertPathExists(localpath & fIndex, True)
        '            DownloadFile(remotepath & fIndex, localpath & fIndex, "_new")
        '            toRemove.Add(localpath & fIndex & "_new")
        '            Dim fsIndex As New FileStream(localpath & fIndex & "_new", FileMode.Open, FileAccess.Read)
        '            Dim strIndex As New StreamReader(fsIndex)
        '            Try
        '                Dim line As String
        '                Dim files As New ArrayList()
        '                line = strIndex.ReadLine()
        '                If line = lastVersion Then
        '                    Return "/BW2Browser.exe"
        '                    ' must be up to date
        '                End If

        '                OnBeforeBrowserDownload(localpath, portal)

        '                Do
        '                    line = strIndex.ReadLine()
        '                    If line = "" Then Exit Do
        '                    Dim strs As String() = Split(line, " ")
        '                    f = Replace(strs(0), "\", "/")
        '                    files.Add(f)
        '                Loop
        '                strIndex.Close()
        '                fsIndex.Close()
        '                strIndex = Nothing
        '                i = 0
        '                For Each f In files
        '                    SetProgress("Downloading Updates", CDbl(i) / files.Count)
        '                    Try
        '                        AssertPathExists(localpath & f, True)
        '                        DownloadFile(remotepath & f, localpath & f, "_new")
        '                        toRemove.Add(localpath & f & "_new")
        '                    Catch e As Exception
        '                        SetProgress("Download failed. " & vbCrLf & f, -1)
        '                        Throw
        '                    End Try
        '                    i += 1
        '                Next f
        '                SetProgress("Actualizing files", 1.0)

        '                Dim n As String
        '                Dim allfiles As New ArrayList(toRemove)
        '                For Each n In allfiles
        '                    Dim origfile As String = n.Substring(0, n.Length - 4)
        '                    If File.Exists(origfile) Then
        '                        If File.Exists(origfile & "_old") Then
        '                            File.Delete(origfile & "_old")
        '                        End If
        '                        File.Move(origfile, origfile & "_old")
        '                        toRename(origfile & "_old") = origfile
        '                        toRemove.Add(origfile & "_old")
        '                    Else
        '                        toRename(origfile) = n   ' a trick to delete the file on failure
        '                    End If
        '                    File.Move(n, origfile)
        '                    toRemove.Remove(n)
        '                Next n
        '                toRename.Clear()
        '                SetProgress("", -1)
        '                Return "/BW2Browser.exe"
        '            Finally
        '                If Not strIndex Is Nothing Then
        '                    strIndex.Close()
        '                    fsIndex.Close()
        '                End If
        '            End Try
        '        Catch ex As WebException
        '            Return "!Portal not responding... " & vbCrLf & ex.Message
        '        Catch ex As Exception
        '            Return "!Error in communication... Try later." & vbCrLf & ex.Message
        '        End Try
        '    Finally
        '        Dim de As DictionaryEntry
        '        Dim n As String
        '        For Each de In toRename
        '            n = CStr(de.Key)
        '            If Directory.Exists(n) Then
        '                If Directory.Exists(CStr(de.Value)) Then
        '                    Directory.Delete(CStr(de.Value), True)
        '                End If
        '                Directory.Move(n, CStr(de.Value))
        '            ElseIf File.Exists(n) Then
        '                If File.Exists(CStr(de.Value)) Then
        '                    File.Delete(CStr(de.Value))
        '                End If
        '                File.Move(n, CStr(de.Value))
        '            End If
        '        Next de
        '        For Each n In toRemove
        '            If Directory.Exists(n) Then
        '                Directory.Delete(n, True)
        '            ElseIf File.Exists(n) Then
        '                File.Delete(n)
        '            End If
        '        Next
        '        If Directory.Exists(tmppath) Then
        '            Directory.Delete(tmppath, True)
        '        End If
        '    End Try
        '    Return "!Login failed."
        'End Function
        Private Sub OnBeforeBrowserDownload(ByVal localpath As String, ByVal portal As PortalInfo)

            Log("Downloading new BW2Browser. Removing all cache files...")
            On Error Resume Next
            Dim path As String = localpath & "/Data/Objects/" & portal.PortalGuid
            Directory.Delete(path & "/Cache", True)
            Directory.Delete(path & "/Streams", True)
            Directory.Delete(path & "/Patterns", True)
            File.Delete(path & "/Patterns.BW2Info")
            File.Delete(path & "/Solutions.BW2Info")
            File.Delete(path & "/Objects.BW2Info")
            File.Delete(path & "/ObjectGroups.BW2Info")
        End Sub
        Friend Sub DownloadFileStartup(ByVal filename As String, Optional ByVal FileInfo As String = "")
            If m_StartupURL <> "" Then
                DownloadFile(m_StartupURL & filename, System.Windows.Forms.Application.StartupPath & filename, , , , , FileInfo)
            Else
                Throw New WebException("Download not configured", WebExceptionStatus.ProtocolError)
            End If
        End Sub
        Friend Sub DownloadFile(ByVal portal As PortalInfo, ByVal filenameServer As String, ByVal filenameLocal As String, Optional ByVal FileInfo As String = "")
            If portal.CurrentURL <> "" Then
                DownloadFile(portal.CurrentURL & filenameServer, System.Windows.Forms.Application.StartupPath & filenameLocal, , , , , FileInfo)
            Else
                Throw New WebException("Download not configured", WebExceptionStatus.ProtocolError)
            End If
        End Sub
        Friend Function DownloadFile(ByVal remoteFilename As String, ByVal localFilename As String, Optional ByVal localExtension As String = "", Optional ByVal tstamp As Object = Nothing, Optional ByVal timeout As Integer = 0, Optional ByVal interactive As Boolean = True, Optional ByVal FileInfo As String = "") As Boolean
            Me.Cursor = Cursors.WaitCursor
            'Dim ProgressWasVisible As Boolean = False
            Dim ProgressWasVisible As Boolean = ProgressBarDownload.Visible
            Dim resp As HttpWebResponse = Nothing
            Dim sFilenameOnError As String = ""
            If LCase(remoteFilename).EndsWith(".exe") Then
                sFilenameOnError = remoteFilename.Substring(0, remoteFilename.Length - 4) + ".e_x_e_"
            End If
            Dim OrigFileInfo As String = FileInfo
            If FileInfo <> "" Then
                FileInfo = "<" & FileInfo & "> "
            End If
            Try
                remoteFilename = Replace(remoteFilename, "\", "/")
                Log("Downloading " & remoteFilename & " -> " & localFilename & localExtension)

                Dim tmppath As String = localFilename
                Do
                    resp = Nothing
                    Dim w As HttpWebRequest
                    w = CType(WebRequest.Create(remoteFilename), HttpWebRequest)
                    If timeout = 0 Then
                        w.Timeout = 5000
                    Else
                        w.Timeout = timeout
                    End If
                    Try
                        w.Headers.Add("Pragma", "no-cache")
                    Catch
                    End Try

                    If tstamp Is Nothing Then
                        Try
                            w.IfModifiedSince = File.GetLastWriteTime(localFilename)
                        Catch
                        End Try
                    Else
                        w.IfModifiedSince = CDate(tstamp)
                    End If
                    w.UserAgent = "BW2Startup"
                    w.AutomaticDecompression = DecompressionMethods.GZip Or DecompressionMethods.Deflate
                    Try
                        If timeout > 0 Then
                            resp = GetResponse(w, timeout, timeout, interactive, True, OrigFileInfo)
                        Else
                            resp = GetResponse(w, , , , , OrigFileInfo)
                        End If
                    Catch ex As BW2Exception When ex.Message = "retry"
                        resp = Nothing
                    Catch ex As WebException When TypeOf ex.Response Is HttpWebResponse AndAlso CType(ex.Response, HttpWebResponse).StatusCode = HttpStatusCode.NotModified
                        resp = CType(ex.Response, HttpWebResponse)
                    Catch ex As WebException When TypeOf ex.Response Is HttpWebResponse AndAlso (CType(ex.Response, HttpWebResponse).StatusCode = HttpStatusCode.Forbidden OrElse CType(ex.Response, HttpWebResponse).StatusCode = HttpStatusCode.NotFound) AndAlso sFilenameOnError <> ""
                        resp = CType(ex.Response, HttpWebResponse)

                    End Try
                    If Not resp Is Nothing Then

                        If resp.StatusCode = HttpStatusCode.NotModified Then
                            If localExtension <> "" Then
                                Dim fi As New FileInfo(localFilename & localExtension)
                                If fi.Exists Then
                                    If (fi.Attributes And FileAttributes.ReadOnly) = FileAttributes.ReadOnly Then
                                        fi.Attributes = fi.Attributes And Not FileAttributes.ReadOnly
                                    End If
                                    fi.Delete()
                                End If
                                File.Copy(localFilename, localFilename & localExtension)
                            End If
                            Log("NotModified: " & FileInfo & remoteFilename & " -> " & localFilename & localExtension)
                            Try
                                resp.Close()
                                resp = Nothing
                            Catch ex As Exception
                            End Try
                            Return False

                        ElseIf resp.StatusCode = HttpStatusCode.OK Then
                            If Not tstamp Is Nothing AndAlso resp.LastModified.Ticks > 0 AndAlso CDate(tstamp) >= resp.LastModified Then
                                Log("Unchanged, response ignoring: " & FileInfo & remoteFilename & " -> " & localFilename & localExtension & " " & resp.LastModified & " older than local " & CDate(tstamp))
                                Try
                                    resp.Close()
                                    resp = Nothing
                                Catch
                                End Try
                                Return False
                            End If
                            Exit Do
                        Else 'If resp.StatusCode = HttpStatusCode.Forbidden Then
                            Try
                                resp.Close()
                                resp = Nothing
                            Catch
                            End Try
                            If sFilenameOnError <> "" Then
                                remoteFilename = sFilenameOnError
                                sFilenameOnError = ""
                            Else
                                Exit Do
                            End If
                        End If
                    End If
                Loop


                If LCase(resp.ContentType) = "text/html" Then
                    Dim lext As String = localFilename + localExtension
                    Dim ix As Integer = lext.LastIndexOf(".")
                    If ix >= 0 Then lext = lext.Substring(ix) Else lext = ""
                    If LCase(lext) <> ".htm" AndAlso LCase(lext) <> ".html" Then
                        ' must be an error

                        Me.Log("Received error page (HTML) instead of file content for: " & FileInfo & remoteFilename)

                        Dim fn1 As String = System.Windows.Forms.Application.StartupPath & "\ErrorPage.html"

                        Dim fs1 As New FileStream(fn1, FileMode.Create, FileAccess.Write, FileShare.None)
                        Dim rs1 As Stream = resp.GetResponseStream
                        Dim bs1 As New BufferedStream(rs1)
                        Dim len1 As Integer = CInt(resp.ContentLength)
                        Dim b1(1024 * 16 - 1) As Byte
                        Try
                            Dim off As Integer = 0
                            While off < len1 OrElse len1 = -1
                                Dim readlen As Integer = bs1.Read(b1, 0, Math.Min(CInt(IIf(len1 = -1, b1.Length, len1 - off)), 1024))
                                If readlen = 0 Then Exit While
                                fs1.Write(b1, 0, readlen)
                                off += readlen
                            End While
                        Finally
                            bs1.Close()
                            rs1.Close()
                            fs1.Close()
                        End Try

                        Dim enc As System.Text.Encoding = System.Text.Encoding.Default
                        Try
                            enc = System.Text.Encoding.GetEncoding(resp.CharacterSet)
                        Catch

                        End Try

                        Dim sr1 As New StreamReader(fn1, enc)
                        Dim errorText As String = ""
                        Try
                            errorText = sr1.ReadToEnd
                        Finally
                            sr1.Close()
                        End Try
                        Select Case LCase(resp.ContentEncoding)
                            Case "quoted-printable", "quoted printable", "quoted/printable", "quotedprintable"

                                errorText = TextConverter.QuotedPrintableDecode(errorText)

                            Case "base64"

                                errorText = TextConverter.Base64Decode(errorText)

                        End Select
                        errorText = Replace(errorText, "</p>", "<br></p>", , , CompareMethod.Text)
                        Dim txt As String = TextConverter.ExtractTextFromHTML(errorText)
                        txt = Replace(txt, vbCrLf, vbLf)
                        If txt.IndexOf(vbCr) < 0 Then
                            txt = Replace(txt, vbLf, vbCrLf)
                        ElseIf txt.IndexOf(vbLf) < 0 Then
                            txt = Replace(txt, vbCr, vbCrLf)
                        End If
                        Do
                            Dim ix1 As Integer = txt.IndexOf(" " & vbCrLf)
                            If ix1 < 0 Then Exit Do
                            txt = Replace(Replace(txt, "  ", " "), " " & vbCrLf, vbCrLf)
                        Loop
                        Do
                            Dim ix1 As Integer = txt.IndexOf(vbCrLf & vbCrLf & vbCrLf)
                            If ix1 < 0 Then Exit Do
                            txt = Replace(txt, vbCrLf & vbCrLf & vbCrLf, vbCrLf & vbCrLf)
                        Loop

                        txt = Replace(txt, vbTab, " ")
                        Do
                            Dim ix1 As Integer = txt.IndexOf("  ")
                            If ix1 < 0 Then Exit Do
                            txt = Replace(txt, "  ", " ")
                        Loop
                        txt = txt.Trim((vbCrLf & " ").ToCharArray)

                        Log(txt.Substring(0, Math.Min(txt.Length, 2000)))

                        Dim etxt As String = GetCaption("A network component returned an error page instead of the requested file content.")

                        Dim ep As New ErrorPage
                        ep.ErrorMessage = etxt
                        ep.ErrorText = txt
                        ep.LinkFilename = fn1
                        ep.ShowDialog(Me)

                        Throw New WebException("A network component returned an error page instead of the requested file content.", WebExceptionStatus.ConnectFailure)
                    End If
                End If

                Dim filename As String = "/" + localFilename
                filename = filename.Substring(filename.LastIndexOfAny("\/".ToCharArray) + 1)
                AssertPathExists(localFilename, True)
                Dim fs As New FileStream(localFilename & localExtension & ".tmp", FileMode.Create, FileAccess.Write, FileShare.None)
                Dim rs As Stream = resp.GetResponseStream
                Dim bs As New BufferedStream(rs)

                Dim len As Integer = CInt(resp.ContentLength)
                Dim b(1024 * 16 - 1) As Byte
                Try
                    Dim off As Integer = 0
                    While off < len OrElse len = -1
                        If len > 64000 OrElse (ProgressBarDownload.Visible AndAlso m_ShowProgress) Then
                            SetProgress(filename, CDbl(off) / len)
                            m_ShowProgress = True
                        End If
                        Dim readlen As Integer = bs.Read(b, 0, Math.Min(CInt(IIf(len = -1, b.Length, len - off)), 1024))
                        If readlen = 0 Then Exit While
                        fs.Write(b, 0, readlen)
                        off += readlen
                        DoEvents()
                    End While
                Finally
                    bs.Close()
                    rs.Close()
                    fs.Close()
                End Try
                IO.File.SetLastWriteTime(localFilename & localExtension & ".tmp", resp.LastModified)
                Dim fi2 As New FileInfo(localFilename & localExtension)
                If fi2.Exists Then
                    fi2.Attributes = fi2.Attributes And Not FileAttributes.ReadOnly
                    fi2.Delete()
                End If
                IO.File.Move(localFilename & localExtension & ".tmp", localFilename & localExtension)
                Try
                    resp.Close()
                    resp = Nothing
                Catch
                End Try

            Catch ex As WebException When TypeOf ex.Response Is HttpWebResponse AndAlso CType(ex.Response, HttpWebResponse).StatusCode = HttpStatusCode.NotFound
                Log("Not found: " & FileInfo & remoteFilename & " -> " & localFilename & localExtension)
                Throw

            Catch ex As WebException When TypeOf ex.Response Is HttpWebResponse AndAlso CType(ex.Response, HttpWebResponse).StatusCode = HttpStatusCode.NotModified
                Throw

            Catch ex As Exception
                Log("Error while downloading " & FileInfo & remoteFilename & " -> " & localFilename & localExtension, ex)
                Throw
            Finally
                If Not resp Is Nothing Then
                    Try
                        resp.Close()
                        resp = Nothing
                    Catch
                    End Try
                End If
                Me.Cursor = Cursors.Default
                ProgressBarDownload.Visible = ProgressWasVisible
            End Try
            Return True
        End Function

        Friend Function CheckForUpdateStartup() As Boolean
            If m_CurrentStartupURL = "" Then
                ' local version
                Log("Local startup version not updating...")
                Return True 'ok
            End If


            Do
                Try
                    Test(m_CurrentStartupURL, m_StartupAlternateURLs, "download/Portals.xml")
                    Exit Do

                Catch ex As BW2ConnectFailedCancelException
                    Return True

                Catch ex As BW2ConnectFailedException
                    Dim res As Microsoft.VisualBasic.MsgBoxResult = MsgBox(GetCaption("Server not reachable. @LF Do you want to retry to connect?"), MsgBoxStyle.Critical Or MsgBoxStyle.AbortRetryIgnore Or MsgBoxStyle.SystemModal, GetCaption("Connection failed"))
                    If res = MsgBoxResult.Abort Then
                        Return False
                    ElseIf res = MsgBoxResult.Ignore Then
                        Return True
                    End If

                Catch ex As WebException When CType(ex, WebException).Status = WebExceptionStatus.Timeout
                    Dim res As Microsoft.VisualBasic.MsgBoxResult = MsgBox(GetCaption("Server timed out.@LF Do you want to retry?"), MsgBoxStyle.Critical Or MsgBoxStyle.AbortRetryIgnore Or MsgBoxStyle.SystemModal, GetCaption("Connection timed out"))
                    If res = MsgBoxResult.Abort Then
                        Return False
                    ElseIf res = MsgBoxResult.Ignore Then
                        Return True
                    End If

                Catch ex As WebException When CType(ex, WebException).Status = WebExceptionStatus.TrustFailure
                    Dim res As Microsoft.VisualBasic.MsgBoxResult = MsgBox(GetCaption("Trust Failure. The certificate can't be validated.@LF Do you want connect anyway?"), MsgBoxStyle.Critical Or MsgBoxStyle.YesNo Or MsgBoxStyle.SystemModal Or MsgBoxStyle.DefaultButton2, GetCaption("Trust Failure"))
                    If res = MsgBoxResult.No Then
                        Return False
                    ElseIf res = MsgBoxResult.Yes Then
                        Return True
                    End If

                Catch ex As Exception
                    Dim res As Microsoft.VisualBasic.MsgBoxResult = MsgBox("Server returned an error." & vbCrLf & ex.Message & vbCrLf & "Do you want to retry?", MsgBoxStyle.Critical Or MsgBoxStyle.AbortRetryIgnore Or MsgBoxStyle.SystemModal, GetCaption("Connection error"))
                    If res = MsgBoxResult.Abort Then
                        Return False
                    ElseIf res = MsgBoxResult.Ignore Then
                        Return True
                    End If

                End Try
            Loop

            m_ShowProgress = False

            Dim tmppath As String = System.Windows.Forms.Application.StartupPath & "/bin_dld"
            'SetProgress(GetCaption("Checking for updates..."), -1)
            'Try
            '    If Not File.Exists(Windows.Forms.Application.StartupPath & "\BW2Startup.exe.manifest") Then
            '        DownloadFileStartup("/BW2Startup.exe.manifest")
            '    Else
            '        If Date.Now.AddDays(-7).CompareTo(File.GetLastWriteTime(Windows.Forms.Application.StartupPath & "\BW2Startup.exe.manifest")) > 0 Then
            '            DownloadFileStartup("/BW2Startup.exe.manifest")
            '        End If
            '    End If
            'Catch ex As Exception
            'End Try
            Try
                Dim tstamp As Object = Nothing

                Dim download As Boolean = False
                'Try
                '    download = download Or DownloadFile(m_CurrentStartupURL & "/zlib.dll", Windows.Forms.Application.StartupPath & "/zlib.dll")

                'Catch ex As Exception
                '    Log("zlib not downloaded", ex)
                'End Try
                Try
                    If DownloadFile(m_CurrentStartupURL & "/download/Translations.xml", System.Windows.Forms.Application.StartupPath & "/Translations.xml", "", , , , "CheckForUpdateStartup") Then
                        Retranslate()
                    End If
                Catch ex As Exception
                End Try

                If Restarting Then Return True
                If System.Environment.GetCommandLineArgs.Length <> 2 OrElse System.Environment.GetCommandLineArgs(1) <> "FORCE" Then
                    Try
                        tstamp = File.GetLastWriteTimeUtc(System.Windows.Forms.Application.StartupPath & "/BW2Startup.exe")
                    Catch
                        tstamp = Nothing
                    End Try
                End If
                download = False
                ws.Url = m_CurrentStartupURL & "/io.asmx"
                SetProxy(ws)

                Try
                    Dim modified As Date = CDate(tstamp)
                    Dim b As Byte() = DownloadFileAsync("bin/BW2Startup.exe", modified)
                    If Not IO.Directory.Exists(tmppath) Then
                        IO.Directory.CreateDirectory(tmppath)
                    End If
                    IO.File.WriteAllBytes(tmppath & "/BW2Startup.exe", b)
                    IO.File.SetLastWriteTimeUtc(tmppath & "/BW2Startup.exe", modified)
                    download = True
                Catch ex As Net.WebException
                Catch ex As IO.IOException

                End Try
                'Try
                '    download = download Or DownloadFile(m_CurrentStartupURL & "/download/BW2Startup.exe", tmppath & "/BW2Startup.exe", "", tstamp)
                'Catch ex As Exception
                '    download = False
                'End Try

                'If Not download Then
                '    Try
                '        download = DownloadFile(m_startupURL & "/BW2Startup", tmppath & "/BW2Startup.exe", "", tstamp)
                '    Catch ex As Exception
                '    End Try
                'End If
                If download Then
                    SetProgress(GetCaption("Restarting Login form..."), 1)
                    Shell(tmppath & "/BW2Startup.exe" & " ""UPDATE"" " & CStr(Process.GetCurrentProcess.Id) & " """ & System.Windows.Forms.Application.StartupPath & """ " & m_AddArgs)
                    Try
                        Threading.Thread.Sleep(1000)
                        Process.GetCurrentProcess.Kill()
                    Catch
                    End Try
                    System.Environment.Exit(0)
                End If
            Finally
                If Directory.Exists(tmppath) Then
                    Directory.Delete(tmppath, True)
                End If
                SetProgress("Powered by BW2 - � 2020", -1)
                m_ShowProgress = False
            End Try
            Return True
        End Function

        Private Function ReadLocalVersions(ByVal path As String, ByVal portal As PortalInfo) As Hashtable
            path = System.Windows.Forms.Application.StartupPath
            Dim filepath As String = "/Data/Objects/" & portal.PortalGuid & "/Assemblies"
            Dim ht As Hashtable = Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable
            Dim fs As FileStream = Nothing
            Dim br As BinaryReader = Nothing
            Try
                fs = File.Open(path + filepath + "/versions.txt", FileMode.Open, FileAccess.Read)
                br = New BinaryReader(fs)

                Do
                    Dim fn As String = br.ReadString
                    ht(fn.ToLower) = br.ReadString
                    If Not File.Exists(System.Windows.Forms.Application.StartupPath + fn) Then
                        ht.Remove(fn.ToLower)
                    End If
                Loop
            Catch ex As EndOfStreamException
                If Not br Is Nothing Then br.Close()
                If Not fs Is Nothing Then fs.Close()
            Catch
            End Try
            Try
                Dim fpath As String = path + filepath
                Dim files As String() = Directory.GetFiles(fpath)
                Dim filename As String
                For Each filename In files
                    filename = Replace(filename.ToLower.Substring(path.Length), "\", "/")
                    Dim fi As New FileInfo(filename)
                    If LCase(fi.Extension) <> ".pdb" Then ' allow pdb files
                        If Not ht.ContainsKey(filename) AndAlso fi.Name.ToLower <> "versions.txt" Then
                            ht.Add(filename, "unknown")
                        End If
                    End If
                Next
            Catch
            End Try
            Return ht
        End Function

        Public Class BackBufferedStream
            Inherits MemoryStream

            Private m_s As Stream
            Private m_pos As Long = 0
            Private m_len As Long = 0

            Public Sub New(ByVal s As Stream)
                m_s = s
            End Sub

            Public Overrides ReadOnly Property CanRead() As Boolean
                Get
                    Return m_s.CanRead
                End Get
            End Property

            Public Overrides ReadOnly Property CanSeek() As Boolean
                Get
                    Return True
                End Get
            End Property

            Public Overrides ReadOnly Property CanWrite() As Boolean
                Get
                    Return False
                End Get
            End Property

            Public Overrides Sub Flush()
                m_s.Flush()
            End Sub

            Public Overrides ReadOnly Property Length() As Long
                Get
                    Return m_s.Length
                End Get
            End Property

            Public Overrides Property Position() As Long
                Get
                    Return MyBase.Position
                End Get
                Set(ByVal Value As Long)
                    MyBase.Position = Value
                End Set
            End Property

            Public Overrides Function Read(ByVal buffer() As Byte, ByVal offset As Integer, ByVal count As Integer) As Integer
                Dim c1 As Integer = 0
                If MyBase.Position < MyBase.Length Then
                    c1 = Math.Min(CInt(MyBase.Length - MyBase.Position), count)
                    MyBase.Read(buffer, offset, c1)
                    count -= c1
                    offset += c1
                End If
                If MyBase.Position + count > MyBase.Length Then
                    Dim cnt As Integer = m_s.Read(buffer, offset, count)
                    If cnt > 0 Then
                        MyBase.Write(buffer, offset, cnt)
                        Return cnt + c1
                    End If
                End If
                Return c1
            End Function

            Public Overrides Function Seek(ByVal offset As Long, ByVal origin As System.IO.SeekOrigin) As Long
                Select Case origin
                    Case SeekOrigin.Begin
                        If offset >= 0 AndAlso offset <= MyBase.Length Then
                            Return MyBase.Seek(offset, origin)
                        End If
                    Case SeekOrigin.Current
                        If MyBase.Position + offset >= 0 AndAlso MyBase.Position + offset <= MyBase.Length Then
                            Return MyBase.Seek(offset, origin)
                        End If
                    Case SeekOrigin.End

                End Select
                Throw New System.NotSupportedException("Write not supported")
            End Function

            Public Overrides Sub SetLength(ByVal value As Long)
                Throw New System.NotSupportedException("Write not supported")
            End Sub

            Public Overrides Sub Write(ByVal buffer() As Byte, ByVal offset As Integer, ByVal count As Integer)
                Throw New System.NotSupportedException("Write not supported")
            End Sub

            Public Overrides Function ReadByte() As Integer
                Dim b(0) As Byte
                Dim cnt As Integer = Read(b, 0, 1)
                If cnt = 0 Then
                    Return -1
                End If
                Return b(0)
            End Function
        End Class

        Public Class BW2EncodedInputStream
            Inherits Stream

            Private m_s As BackBufferedStream

            Private m_UseEncoding As Boolean = False
            Private m_UseBase64Encoding As Boolean = False
            Private m_PreambleRead As Boolean = False

            Public Sub New(ByVal base As Stream)
                m_s = New BackBufferedStream(base)
            End Sub


            Public Const EncodeHTML_Begin As String = "<html><body><binary>"
            Public Const EncodeHTML_End As String = "</binary></body></html>"
            Public Const EncodeHTML2_Begin As String = "<html><body><p>"
            Public Const EncodeHTML2_End As String = "</p></body></html>"

            Private Sub Prepare()
                If m_PreambleRead Then Exit Sub
                m_PreambleRead = True
                Dim b As Integer = m_s.ReadByte
                If b < 0 Then
                    Throw New IO.EndOfStreamException

                ElseIf b = 1 Then
                    'binary error
                    m_s.Seek(0, SeekOrigin.Begin)

                ElseIf b = 0 Then
                    'binary OK
                    m_s.Seek(0, SeekOrigin.Begin)

                ElseIf b = Asc("<"c) Then
                    m_s.Seek(0, SeekOrigin.Begin)
                    m_UseEncoding = True
                    Dim bTemp(Math.Max(EncodeHTML_Begin.Length, EncodeHTML_Begin.Length) - 1) As Byte
                    If m_s.Read(bTemp, 0, bTemp.Length) = bTemp.Length Then
                        If System.Text.ASCIIEncoding.ASCII.GetString(bTemp, 0, EncodeHTML_Begin.Length).StartsWith(EncodeHTML_Begin) Then
                            m_s.Seek(EncodeHTML_Begin.Length, SeekOrigin.Begin)
                        ElseIf System.Text.ASCIIEncoding.ASCII.GetString(bTemp, 0, EncodeHTML2_Begin.Length).StartsWith(EncodeHTML2_Begin) Then
                            m_UseBase64Encoding = True
                            m_s.Seek(EncodeHTML2_Begin.Length, SeekOrigin.Begin)
                            m_basepos = m_s.Position
                        End If
                    End If
                Else
                    Throw New BW2ServerException("Unsupported protocol")

                End If
            End Sub



            Private m_bytepos As Long = 0
            Private m_basepos As Long = 0

            Public Overrides Function Read(ByVal buffer() As Byte, ByVal offset As Integer, ByVal count As Integer) As Integer
                Prepare()
                If count = 0 Then Return 0
                If m_UseBase64Encoding Then
                    Dim b64offset As Integer = CInt((m_bytepos \ 3) * 4)
                    Dim b64offset2 As Integer = CInt(((m_bytepos + count + 2) \ 3) * 4)
                    Dim buf(b64offset2 - b64offset - 1) As Byte
again:
                    m_s.Seek(b64offset + m_basepos, SeekOrigin.Begin)
                    Dim cnt As Integer = m_s.Read(buf, 0, buf.Length)
                    If cnt Mod 4 > 0 Then
                        cnt = cnt - (cnt Mod 4)
                        If cnt = 0 Then
                            Threading.Thread.Sleep(20)
                            GoTo again
                        End If
                    End If

                    Dim msgtext As String = System.Text.ASCIIEncoding.ASCII.GetString(buf, 0, cnt)

                    Dim ix As Integer = msgtext.IndexOf("<"c)
                    If ix >= 0 Then
                        msgtext = msgtext.Substring(0, ix)
                    End If
                    Dim b As Byte() = Nothing
                    Try
                        b = Convert.FromBase64String(msgtext)
                    Catch ex As Exception
                        Debug.WriteLine(msgtext)
                        Throw
                    End Try

                    Dim o1 As Integer = CInt(m_bytepos Mod 3)

                    Dim reallen As Integer = b.Length - o1
                    If reallen > count Then
                        reallen = count
                    End If

                    Dim len As Integer = reallen

                    If len <= 0 Then
                        len = 0
                    Else
                        Array.Copy(b, o1, buffer, offset, len)
                        m_bytepos += len
                    End If
                    Return len
                Else
                    Return m_s.Read(buffer, offset, count)
                End If
            End Function


            Public Overrides Function ReadByte() As Integer
                Dim b(0) As Byte
                Dim cnt As Integer = Read(b, 0, 1)
                If cnt = 0 Then
                    Return -1
                End If
                Return b(0)
            End Function

            Public Overrides ReadOnly Property CanRead() As Boolean
                Get
                    Return True
                End Get
            End Property

            Public Overrides ReadOnly Property CanSeek() As Boolean
                Get
                    Return False
                End Get
            End Property

            Public Overrides ReadOnly Property CanWrite() As Boolean
                Get
                    Return False
                End Get
            End Property

            Public Overrides Sub Flush()
                Throw New NotSupportedException
            End Sub

            Public Overrides ReadOnly Property Length() As Long
                Get
                    Throw New NotSupportedException

                End Get
            End Property

            Public Overrides Property Position() As Long
                Get
                    Return m_bytepos
                End Get
                Set(ByVal Value As Long)
                    Throw New NotSupportedException
                End Set
            End Property

            Public Overrides Function Seek(ByVal offset As Long, ByVal origin As System.IO.SeekOrigin) As Long
                Throw New NotSupportedException
            End Function

            Public Overrides Sub SetLength(ByVal value As Long)
                Throw New NotSupportedException
            End Sub

            Public Overrides Sub Write(ByVal buffer() As Byte, ByVal offset As Integer, ByVal count As Integer)
                Throw New NotSupportedException

            End Sub
        End Class

        Friend Function UpdateBrowser(ByVal path As String, ByVal portal As PortalInfo, ByVal user As String, ByVal pwd As String) As String
            'Dim ws As New BW2WebAppService.BW2WebAppService
            ws.Url = portal.CurrentURL & "/io.asmx"
            SetProxy(ws)

            Dim localVersions As Hashtable = ReadLocalVersions(path, portal)
            Dim pguid As String = portal.PortalGuid.ToLower
            Dim dfis As BW2WebAppService.DownloadFileInfo() = DownloadFileDataPrepareAsync(False, pguid, ws.Timeout)
            Dim filepath As String = "/Data/Objects/" & pguid & "/Assemblies"
            AssertPathExists(System.Windows.Forms.Application.StartupPath + filepath + "/versions.txt", True)

            Dim nBytesDownloaded As Long = 0
            Dim t1 As Date = Date.UtcNow
            For i As Integer = 0 To dfis.GetUpperBound(0)
                Dim fn As String = dfis(i).Filename
                Dim v As String = CStr(localVersions(fn))
                Dim fd As String = System.IO.Path.GetFileName(fn)
                If v Is Nothing Then v = ""
                If dfis(i).Version <> v Then
                    SetProgress(fd, CDbl(i) / dfis.Length)
                    Try
                        DownloadFileInfo(pguid, dfis(i), v)
                        Dim fi As New FileInfo(System.Windows.Forms.Application.StartupPath + dfis(i).Filename)
                        nBytesDownloaded += fi.Length
                        Dim t3 As Date = Date.UtcNow
                        If t3 > t1 Then
                            Dim sp As Double = (nBytesDownloaded / (t3.Subtract(t1).TotalSeconds)) / 1024
                            If sp < 1000 Then
                                Log("Download speed low: " & Format(sp, "0.000") & " kB/s")
                            End If
                        End If

                    Catch ex As Exception
                        Dim fs1 As FileStream = File.Open(System.Windows.Forms.Application.StartupPath + filepath + "/versions.txt", FileMode.Create, FileAccess.Write)
                        Dim bw1 As New BinaryWriter(fs1)
                        For i1 As Integer = 0 To i - 1
                            bw1.Write(dfis(i1).Filename)
                            bw1.Write(dfis(i1).Version)
                        Next
                        For Each de As DictionaryEntry In localVersions
                            Dim fn1 As String = CStr(de.Key)
                            bw1.Write(fn1)
                            bw1.Write(CStr(de.Value))
                        Next
                        bw1.Flush()
                        fs1.Close()
                        Log("Error downloading " & dfis(i).Filename & " timeout=" & ws.Timeout, ex)
                        Throw
                    End Try
                End If
                localVersions.Remove(fn)
            Next
            Dim t2 As Date = Date.UtcNow
            If t2 > t1 Then
                Log("Download speed: " & Format((nBytesDownloaded / (t2.Subtract(t1).TotalSeconds)) / 1024, "0.000") & " kB/s")
            End If

            For Each de As DictionaryEntry In localVersions
                Try
                    Dim fi As New FileInfo(System.Windows.Forms.Application.StartupPath + CStr(de.Key))
                    fi.Attributes = fi.Attributes And Not FileAttributes.ReadOnly
                    fi.Delete()
                Catch
                End Try
            Next

            Dim fs As FileStream = File.Open(System.Windows.Forms.Application.StartupPath + filepath + "/versions.txt", FileMode.Create, FileAccess.Write)
            Dim bw As New BinaryWriter(fs)
            For i As Integer = 0 To dfis.GetUpperBound(0)
                bw.Write(dfis(i).Filename)
                bw.Write(dfis(i).Version)
            Next
            bw.Flush()
            fs.Close()
            SetProgress("Powered by BW2 - � 2020", -1)
            Dim tpath As String = System.Windows.Forms.Application.StartupPath & filepath
            If portal.PortalUse64Bit OrElse Not IO.File.Exists(tpath & "/" & ExeName32()) Then
                Return ExeNameAny()
            Else
                Return ExeName32()

            End If
        End Function



        Private Delegate Function DownloadFileAsyncHandler(ByVal path As String, ByRef utcModifiedDate As Date) As Byte()

        Private Function DownloadFileAsync(ByVal path As String, ByRef utcModifiedDate As Date, Optional ByVal Timeout As Integer = -1, Optional ByVal SuppressDoEvents As Boolean = False) As Byte()
            Try
                SetProxy(ws)
                If Timeout < 0 Then Timeout = ws.Timeout
                Dim callback As New DownloadFileAsyncHandler(AddressOf ws.DownloadFile)
                Dim result As IAsyncResult = callback.BeginInvoke(path, utcModifiedDate, Nothing, Nothing)
                Dim ts As Date = Date.Now
                Dim te As Date = ts.AddMilliseconds(Timeout)
                While ts < te
                    If (result.AsyncWaitHandle.WaitOne(Math.Max(1, Math.Min(50, CInt(te.Subtract(ts).TotalMilliseconds))), False)) Then
                        Return callback.EndInvoke(utcModifiedDate, result)
                    End If
                    If Not SuppressDoEvents Then
                        System.Windows.Forms.Application.DoEvents()
                    End If
                    ts = Date.Now
                    SetTimeoutProgress(ts, te, Timeout)
                End While

                Throw New WebException("Connection timed out", WebExceptionStatus.Timeout)
            Finally
                HideTimeoutProgress()
            End Try
        End Function




        Private Delegate Function DownloadFileDataPrepareAsyncHandler(ByVal isService As Boolean, ByVal portalGuid As String) As BW2WebAppService.DownloadFileInfo()

        Private Function DownloadFileDataPrepareAsync(ByVal isService As Boolean, ByVal portalGuid As String, Optional ByVal Timeout As Integer = -1, Optional ByVal SuppressDoEvents As Boolean = False) As BW2WebAppService.DownloadFileInfo()
            Try
                If Timeout < 0 Then Timeout = ws.Timeout
                Dim callback As New DownloadFileDataPrepareAsyncHandler(AddressOf ws.DownloadFileDataPrepare)
                Dim result As IAsyncResult = callback.BeginInvoke(isService, portalGuid, Nothing, Nothing)
                Dim ts As Date = Date.Now
                Dim te As Date = ts.AddMilliseconds(Timeout)
                While ts < te
                    If (result.AsyncWaitHandle.WaitOne(Math.Max(1, Math.Min(50, CInt(te.Subtract(ts).TotalMilliseconds))), False)) Then
                        Return callback.EndInvoke(result)
                    End If
                    If Not SuppressDoEvents Then
                        System.Windows.Forms.Application.DoEvents()
                    End If
                    ts = Date.Now
                    SetTimeoutProgress(ts, te, Timeout)
                End While

                Throw New WebException("Connection timed out", WebExceptionStatus.Timeout)
            Finally
                HideTimeoutProgress()
            End Try
        End Function




        Private Delegate Function DownloadFileDataAsyncHandler(ByVal pguid As String, ByVal dfi As BW2WebAppService.DownloadFileInfo) As Byte()

        Private Function DownloadFileDataAsync(ByVal pguid As String, ByVal dfi As BW2WebAppService.DownloadFileInfo, Optional ByVal Timeout As Integer = -1, Optional ByVal SuppressDoEvents As Boolean = False, Optional ByVal AllowQueryContinue As Boolean = False) As Byte()


            Dim callback As New DownloadFileDataAsyncHandler(AddressOf ws.DownloadFileData)
            Dim result As IAsyncResult = callback.BeginInvoke(pguid, dfi, Nothing, Nothing)
            Dim ts As Date = Date.Now
            Dim te As Date = Date.MaxValue
            If Timeout >= 0 Then
ContinueDownload:
                te = ts.AddMilliseconds(Timeout)
                While ts < te
                    If (result.AsyncWaitHandle.WaitOne(Math.Max(1, Math.Min(50, CInt(te.Subtract(ts).TotalMilliseconds))), False)) Then
                        Return callback.EndInvoke(result)
                    End If
                    If Not SuppressDoEvents Then
                        System.Windows.Forms.Application.DoEvents()
                    End If
                    ts = Date.Now
                End While
                If AllowQueryContinue Then
                    'If MsgBox(GetCaption("TimeoutDoYouWantToContinue?"), MsgBoxStyle.OkCancel, GetCaption("Downloading updates")) = MsgBoxResult.Ok Then
                    GoTo ContinueDownload
                    'End If
                End If
            Else
                Do
                    If (result.AsyncWaitHandle.WaitOne(50, False)) Then
                        Return callback.EndInvoke(result)
                    End If
                    If Not SuppressDoEvents Then
                        System.Windows.Forms.Application.DoEvents()
                    End If
                Loop
            End If
            Throw New WebException("The request timed out", WebExceptionStatus.Timeout)
        End Function


        Private Sub DownloadFileInfo(ByVal pguid As String, ByVal dfi As BW2WebAppService.DownloadFileInfo, ByVal v As String)
            If v Is Nothing Then v = ""
            If dfi.Version = v Then Exit Sub
            Dim vo As String = dfi.Version

            dfi.Version = v
            Dim b As Byte() = DownloadFileDataAsync(pguid, dfi, ws.Timeout, False, True)
            If b IsNot Nothing Then
                AssertPathExists(System.Windows.Forms.Application.StartupPath + dfi.Filename, True)

                Dim fi As New FileInfo(System.Windows.Forms.Application.StartupPath + dfi.Filename)
                If fi.Exists Then
                    If (fi.Attributes And FileAttributes.ReadOnly) = FileAttributes.ReadOnly Then
                        fi.Attributes = fi.Attributes And Not FileAttributes.ReadOnly
                    End If
                    fi.Delete()
                End If

                IO.File.WriteAllBytes(System.Windows.Forms.Application.StartupPath + dfi.Filename, b)
                dfi.Version = vo
            End If

        End Sub


#End Region

#Region "URL Test..."

        Private m_htres As New Hashtable

        Private Function ResolveDNS(ByVal w As HttpWebRequest) As Boolean
            ResolveDNS = True

            With w.RequestUri
                If .HostNameType = UriHostNameType.IPv4 OrElse .HostNameType = UriHostNameType.IPv6 Then
                Else
                    If Not m_htres.ContainsKey(.Host) Then
                        Dim d1 As Date = Date.UtcNow
                        Dim s As String = ""
                        Try
                            Dim iph As IPHostEntry = DNSGetHostEntry(.DnsSafeHost)
                            For Each adr As IPAddress In iph.AddressList
                                s &= adr.ToString & " "
                            Next
                            m_htres(.Host) = s
                        Catch ex As Exception
                            s = "Resolve error for " & .Host & ": " & ex.Message
                            ResolveDNS = False
                        End Try
                        Dim d2 As Date = Date.UtcNow
                        Log("DNS Lookup " & .Host & " results: " & s & " in " & d2.Subtract(d1).TotalMilliseconds.ToString("0.000") & " ms resolving " & w.RequestUri.ToString)
                    End If
                End If
            End With
        End Function

        Private Sub SetURL(ByVal url As String)
            'Log("Setting URL to " & url)
            Me.MenuItemInfo.Text = url
        End Sub

        Private Sub Test(ByVal portal As PortalInfo)
            Test(portal.CurrentURL, portal.PortalAlternateURLs, "ping.bw2?action=PING&portal=" & portal.PortalGuid, portal)
        End Sub


        Private Sub Test(ByRef CurrentURL As String, ByVal AlternateURLs As ArrayList, Optional ByVal filename As String = "", Optional ByVal portalinfo As PortalInfo = Nothing)
            Dim alternateTests As Integer = 0
            'Dim uri1 As New Uri(CurrentURL)
            'Dim hn As String = LCase(uri1.DnsSafeHost)
            'If hn.StartsWith("ap") AndAlso hn.EndsWith(".bw2hosting.ch") AndAlso Not hn.EndsWith("i.bw2hosting.ch") AndAlso Not hn.EndsWith("a.bw2hosting.ch") AndAlso Not hn.EndsWith("b.bw2hosting.ch") _
            'AndAlso (AlternateURLs.Count = 0 OrElse (AlternateURLs.Count = 1 AndAlso CStr(AlternateURLs(0)) = CurrentURL)) Then
            '    Dim endings As String() = {"i", "a", "b"}
            '    For Each ending As String In endings
            '        Dim u As String = Replace(CurrentURL, ".bw2hosting.ch", ending & ".bw2hosting.ch", , , CompareMethod.Text)
            '        Try
            '            Dim ur As New Uri(u)
            '            DNSGetHostEntry(ur.DnsSafeHost, 2000, False)
            '            AlternateURLs.Add(u) ' valid DNS
            '        Catch ex As Exception
            '            ' unknown uri
            '            Log("Skipping invalid host name: " & u & " (" & ex.Message & ")")
            '        End Try
            '    Next
            'End If

            Try
                Dim lastalt As Integer = 0
                Me.Cursor = Cursors.WaitCursor

                For j As Integer = 0 To AlternateURLs.Count - 1
                    CurrentURL = CStr(AlternateURLs(j))
                    Dim uri As New Uri(CurrentURL)
                    If portalinfo IsNot Nothing Then
                        SetURL(uri.Authority)
                    End If

                    Dim w As HttpWebRequest
                    w = CType(WebRequest.Create(CurrentURL + filename), HttpWebRequest)
                    w.Pipelined = False
                    w.AutomaticDecompression = DecompressionMethods.GZip
                    Log("Test-URL: " + CurrentURL + filename)
                    Dim x As HttpWebResponse = GetResponse(w, 1000, 3000, False, False, "Testing")
                    With x
                        .Close()
                    End With

                    Exit Sub

                Next
                Exit Sub
                Dim times(AlternateURLs.Count - 1) As Long
                For k As Integer = 0 To 2
                    For j As Integer = 0 To AlternateURLs.Count - 1
                        CurrentURL = CStr(AlternateURLs(j))
                        Dim t1 As Long = Stopwatch.GetTimestamp
                        Dim uri As New Uri(CurrentURL)
                        If portalinfo IsNot Nothing Then
                            SetURL(uri.Authority)
                        End If

                        Dim w As HttpWebRequest
                        w = CType(WebRequest.Create(CurrentURL + filename), HttpWebRequest)
                        w.Pipelined = False
                        w.AutomaticDecompression = DecompressionMethods.GZip
                        Dim x As HttpWebResponse = GetResponse(w, 1000, 3000, False, False, "Testing")
                        With x
                            .Close()
                        End With
                        Dim t2 As Long = Stopwatch.GetTimestamp
                        Dim dt As Long = t2 - t1
                        Log("Testing: (OK) " + CurrentURL + filename & " duration: " & Format((dt * 1000L) / Stopwatch.Frequency, "0.0") & " ms")
                        times(j) += dt
                    Next
                Next
                Dim smallest As Integer = -1
                Dim smallestTime As Long = 0
                For j As Integer = 0 To times.GetUpperBound(0)
                    If smallest < 0 OrElse smallestTime > times(j) Then
                        smallestTime = times(j)
                        smallest = j
                    End If
                Next
                CurrentURL = CStr(AlternateURLs(smallest))

                'Do
                '    Try
                '        Dim t1 As Long = Stopwatch.GetTimestamp
                '        Dim uri As New Uri(CurrentURL)
                '        If portalinfo IsNot Nothing Then
                '            SetURL(uri.Authority)
                '        End If

                '        Dim w As HttpWebRequest
                '        w = CType(WebRequest.Create(CurrentURL + filename), HttpWebRequest)
                '        w.Pipelined = False
                '        w.AutomaticDecompression = DecompressionMethods.GZip

                '        '    SetProxy(w)
                '        Dim x As HttpWebResponse = GetResponse(w, 1000, 3000, True, alternateTests = 0)
                '        With x
                '            .Close()
                '        End With
                '        Dim t2 As Long = Stopwatch.GetTimestamp

                '        Log("Testing: (OK) " + CurrentURL + filename & " duration: " & format(((t2 - t1) * 1000L) / Stopwatch.Frequency,"0.0") & " ms"
                '        'Exit Sub
                '    Catch ex As BW2Exception When ex.Message = "retry"
                '        If lastalt <> alternateTests Then
                '            If alternateTests = AlternateURLs.Count Then
                '                alternateTests = 0
                '            End If
                '            CurrentURL = CStr(AlternateURLs(alternateTests))
                '            alternateTests += 1
                '            If portalinfo IsNot Nothing Then
                '                Dim uri As New Uri(CurrentURL)
                '                SetURL(uri.Authority)
                '            End If
                '        End If
                '        lastalt = alternateTests

            Catch ex As BW2ConnectFailedCancelException
                Throw

            Catch ex As BW2ConnectFailedException When alternateTests < AlternateURLs.Count
                Threading.Thread.Sleep(20)
                CurrentURL = CStr(AlternateURLs(alternateTests))
                alternateTests += 1
                If portalinfo IsNot Nothing Then
                    Dim uri As New Uri(CurrentURL)
                    SetURL(uri.Authority)
                End If

            Catch ex As WebException When ex.Status = WebExceptionStatus.ProtocolError
                If Not ex.Response Is Nothing Then
                    Dim resp As HttpWebResponse = CType(ex.Response, HttpWebResponse)
                    If resp.StatusCode = HttpStatusCode.Forbidden Then
                        Return
                    ElseIf resp.StatusCode = HttpStatusCode.NotFound Then
                        Dim s As String = ""
                        If Not portalinfo Is Nothing Then
                            s = portalinfo.PortalDisplayName & " (" & portalinfo.PortalGuid & ")"
                        End If
                        Log("Portal not found:" & s)
                        Me.Cursor = Cursors.Default
                        MsgBox(GetCaption("Portal not found on server.") & vbCrLf & s, MsgBoxStyle.SystemModal)
                        Return
                    End If
                End If
                Throw
                'Catch ex As WebException When ex.Status = WebExceptionStatus.ConnectFailure
            Catch ex As Exception
                Throw
                'End Try
                '    Loop
            Finally
                Me.Cursor = Cursors.Default
            End Try

        End Sub

        Private m_allowProxy As Boolean = False

        Private Sub SetProxy(ByVal req As HttpWebRequest)
            If m_allowProxy Then
                Dim t1 As Date = Date.UtcNow

                If m_UseProxy Then
                    Dim prox As WebProxy = Nothing
                    If m_Proxy <> "" Then
                        prox = New WebProxy(m_Proxy)
                    End If
                    If Not prox Is Nothing AndAlso Not prox.Address Is Nothing AndAlso Not prox.IsBypassed(req.RequestUri) Then

                        req.Proxy = prox
                        req.Proxy.Credentials = m_Credentials.GetCredentials

                        Try
                            '  Log("Adding Proxy: " + prox.Address.ToString)
                        Catch
                        End Try
                    ElseIf m_Proxy = "" AndAlso Not req.Proxy Is Nothing Then
                        req.Proxy.Credentials = m_Credentials.GetCredentials
                    End If
                End If

                If req.Proxy IsNot Nothing AndAlso TypeOf req.Proxy.Credentials Is CredentialCache Then
                    Dim puri As Uri = req.Proxy.GetProxy(req.RequestUri)
                    Try

                        If CType(req.Proxy.Credentials, CredentialCache).GetCredential(puri, "NTLM") Is Nothing Then
                            CType(req.Proxy.Credentials, CredentialCache).Add(puri, "NTLM", System.Net.CredentialCache.DefaultCredentials.GetCredential(puri, "NTLM"))

                        End If

                    Catch
                    End Try
                End If
            Else
                req.Proxy = Nothing
            End If

            req.Credentials = m_Credentials.GetCredentials
            req.PreAuthenticate = True
            req.Pipelined = False
        End Sub

        Private Sub SetProxy(ByVal req As BW2WebAppService.BW2WebAppService)
            If m_allowProxy Then
                Dim t1 As Date = Date.UtcNow
                Dim reqUri As New Uri(req.Url)
                If m_UseProxy Then
                    Dim prox As WebProxy = Nothing
                    If m_Proxy <> "" Then
                        prox = New WebProxy(m_Proxy)
                    End If
                    If Not prox Is Nothing AndAlso Not prox.Address Is Nothing AndAlso Not prox.IsBypassed(reqUri) Then

                        req.Proxy = prox
                        req.Proxy.Credentials = m_Credentials.GetCredentials

                        Try
                            '  Log("Adding Proxy: " + prox.Address.ToString)
                        Catch
                        End Try
                    ElseIf m_Proxy = "" AndAlso Not req.Proxy Is Nothing Then
                        req.Proxy.Credentials = m_Credentials.GetCredentials
                    End If
                End If

                If req.Proxy IsNot Nothing AndAlso TypeOf req.Proxy.Credentials Is CredentialCache Then
                    Dim puri As Uri = req.Proxy.GetProxy(reqUri)
                    Try

                        If CType(req.Proxy.Credentials, CredentialCache).GetCredential(puri, "NTLM") Is Nothing Then
                            CType(req.Proxy.Credentials, CredentialCache).Add(puri, "NTLM", System.Net.CredentialCache.DefaultCredentials.GetCredential(puri, "NTLM"))

                        End If

                    Catch
                    End Try
                End If
            ElseIf req.Proxy Is Nothing Then
                Dim pro As New WebProxy
                Dim reqUri As New Uri(req.Url)
                pro.BypassArrayList.Add(reqUri)
                req.Proxy = pro
            End If

            req.Credentials = m_Credentials.GetCredentials
            req.PreAuthenticate = True

        End Sub

        Private m_addTimeout As Integer = 500


        Private Delegate Function GetResponseHandler() As WebResponse

        Public Shared Function GetResponseAsync(ByVal req As HttpWebRequest, Optional ByVal SuppressDoEvents As Boolean = False) As WebResponse
            Try
                Dim timeout As Integer = req.Timeout
                Dim callback As New GetResponseHandler(AddressOf req.GetResponse)
                Dim result As IAsyncResult = callback.BeginInvoke(Nothing, Nothing)
                Dim ts As Date = Date.UtcNow
                Dim te As Date = ts.AddMilliseconds(timeout)
                While ts < te
                    If (result.AsyncWaitHandle.WaitOne(Math.Max(1, Math.Min(50, CInt(te.Subtract(ts).TotalMilliseconds))), False)) Then
                        Return callback.EndInvoke(result)
                    End If
                    If Not SuppressDoEvents Then
                        System.Windows.Forms.Application.DoEvents()
                    End If
                    ts = Date.UtcNow
                    SetTimeoutProgress(ts, te, timeout, req.RequestUri.DnsSafeHost)
                End While

                Throw New WebException("Connection timed out", WebExceptionStatus.Timeout)
            Finally
                HideTimeoutProgress()
            End Try
        End Function


        Private Function GetResponse(ByVal req As HttpWebRequest, Optional ByVal Timeout As Integer = 5000, Optional ByVal TotalTimeout As Integer = 20000, Optional ByVal interactive As Boolean = True, Optional ByVal autofailover As Boolean = False, Optional ByVal FileInfo As String = "") As HttpWebResponse
            Dim t1 As Date = Date.UtcNow
            '            req.ServicePoint.ConnectionLimit = 10

            If SimulateSlowConnection > 0 Then
                Threading.Thread.Sleep(SimulateSlowConnection)
            End If

            If FileInfo <> "" Then
                FileInfo = "<" & FileInfo & "> "
            End If
            Do
                'Log("GetResponse: " & req.RequestUri.AbsoluteUri)
                Try
                    DoEvents()

                    If Not ResolveDNS(req) Then
                        Throw New BW2DNSException(New ArgumentException(req.RequestUri.Host))

                    End If
                    req.Timeout = Math.Min(m_addTimeout, TotalTimeout) ' Timeout + m_addTimeout
                    If req.Method = "GET" Then
                        SetProxy(req)
                    End If

                    Dim ta1 As Date = Date.UtcNow
                    Dim resp As HttpWebResponse = Nothing
                    Try
                        resp = CType(GetResponseAsync(req), HttpWebResponse)
                    Finally
                        Dim ta2 As Date = Date.UtcNow
                        Dim s As String = "FAILED"
                        If resp IsNot Nothing Then s = resp.StatusDescription
                        Dim s1 As String = ""
                        If SimulateSlowConnection > 0 Then
                            s1 = " + " & CStr(SimulateSlowConnection)
                        End If
                        Log(Format(ta2.Subtract(ta1).TotalMilliseconds, "0.0") & s1 & " ms " & req.Address.ToString & " (" & s & ")")
                    End Try
                    Return resp
                Catch ex As WebException

                    If ex.Status = WebExceptionStatus.ConnectFailure OrElse ex.Status = WebExceptionStatus.ConnectionClosed OrElse ex.Status = WebExceptionStatus.NameResolutionFailure Then
                        Throw New BW2ConnectFailedException(ex)
                    End If
                    If ex.Status = WebExceptionStatus.Timeout Then
                        m_addTimeout += Timeout \ 2

                        If autofailover AndAlso (m_addTimeout >= TotalTimeout OrElse Date.UtcNow.Subtract(t1).TotalMilliseconds > TotalTimeout) Then
                            Throw New BW2ConnectFailedException(ex)
                        End If

                        If Date.UtcNow.Subtract(t1).TotalMilliseconds > TotalTimeout Then
                            If interactive Then
                                Dim res As Microsoft.VisualBasic.MsgBoxResult = MsgBox(GetCaption("Server timed out.@LF Do you want to retry?"), MsgBoxStyle.Critical Or MsgBoxStyle.RetryCancel Or MsgBoxStyle.SystemModal, GetCaption("Connection timed out"))
                                If res = MsgBoxResult.Cancel Then
                                    Throw New BW2ConnectFailedCancelException(ex)
                                End If
                            Else
                                Throw New BW2ConnectFailedException(ex)
                            End If
                        End If
                        Me.Log("Timeout: " & FileInfo & "=> retry")
                        If m_allowProxy = False Then
                            Me.Log("Proxy may be required, enabling default Proxy")
                            m_allowProxy = True
                        End If
                        Throw New BW2Exception("retry")
                    End If
                    If ex.Status = WebExceptionStatus.ProtocolError Then
                        If Not ex.Response Is Nothing Then
                            Dim resp As HttpWebResponse = CType(ex.Response, HttpWebResponse)
                            Try
                                If resp.StatusCode = HttpStatusCode.Unauthorized Then

                                    Dim auth As String = resp.Headers.Item("WWW-Authenticate")
                                    If auth <> "" Then 'AndAlso auth.ToLower.IndexOf(" realm=") >= 0 Then
                                        Dim ix As Integer = auth.ToLower.IndexOf(" realm=")
                                        Dim authType As String = auth
                                        Dim realm As String = GetCaption("Authentication")
                                        If ix >= 0 Then
                                            authType = auth.Substring(0, ix)
                                            realm = auth.Substring(ix + 7).Trim(""""c)
                                        End If
                                        If m_Credentials.PromptForUserCredentials(Me, resp.ResponseUri, authType, realm) Then
                                            ' try again
                                            Throw New BW2Exception("retry")
                                        End If
                                    End If
                                ElseIf resp.StatusCode = HttpStatusCode.UseProxy Then
                                    m_Proxy = resp.GetResponseHeader("Location")
                                    'm_Proxy = resp.ResponseUri.ToString

                                    If m_allowProxy = False Then
                                        Me.Log("Proxy Authentication Required, enabling Proxy: " & m_Proxy)
                                        m_allowProxy = True
                                        Throw New BW2Exception("retry")
                                    End If

                                ElseIf resp.StatusCode = HttpStatusCode.ProxyAuthenticationRequired Then
                                    '  Me.Log("WebException", ex)
                                    If m_allowProxy = False Then
                                        Me.Log("Proxy Authentication Required, enabling Proxy")
                                        m_allowProxy = True
                                        Throw New BW2Exception("retry")
                                    End If
                                    Me.Log("Proxy Authentication Required: " & FileInfo)
                                    Dim k As String() = resp.Headers.AllKeys
                                    Dim s As String
                                    For Each s In k
                                        Log("Header." & s & "=" & resp.Headers.Item(s))
                                    Next
                                    Try
                                        Log(resp.ResponseUri.ToString())
                                        Log(req.Proxy.GetProxy(resp.ResponseUri).ToString())
                                    Catch
                                    End Try
                                    Dim auth As String = resp.Headers.Item("Proxy-Authenticate")
                                    If auth <> "" Then 'AndAlso auth.ToLower.IndexOf(" realm=") >= 0 Then
                                        Dim ix As Integer = auth.ToLower.IndexOf(" realm=")
                                        Dim authType As String = auth
                                        Dim realm As String = GetCaption("Proxy Authentication")
                                        If ix >= 0 Then
                                            authType = auth.Substring(0, ix)
                                            realm = auth.Substring(ix + 7).Trim(""""c)
                                        End If
                                        If m_Credentials.PromptForUserCredentials(Me, req.Proxy.GetProxy(resp.ResponseUri), authType, realm) Then
                                            ' try again
                                            Throw New BW2Exception("retry")
                                        End If
                                    End If
                                ElseIf resp.StatusCode = HttpStatusCode.InternalServerError Then
                                    Me.Log("WebException " & FileInfo, ex)

                                    Throw New BW2ConnectFailedException
                                End If
                                Dim sst As String = ""
                                If resp.StatusCode.ToString <> Replace(resp.StatusDescription, " ", "") Then
                                    sst = " [" & resp.StatusDescription & "]"
                                End If
                                Me.Log("WebException: " & FileInfo & "Status=(" & CInt(resp.StatusCode) & ") " & resp.StatusCode.ToString & sst)
                            Finally
                                resp.Close()
                            End Try
                        Else
                            Me.Log("WebException: " & FileInfo, ex)
                        End If
                    Else
                        Me.Log("WebException: " & FileInfo, ex)
                    End If


                    Throw
                Catch ex As IOException
                    Throw New BW2ConnectFailedException(ex)
                End Try
            Loop
        End Function


        Class BW2ConnectFailedException
            Inherits Exception

            Public Sub New(Optional ByVal InnerException As Exception = Nothing)
                MyBase.New("Connection Failed", InnerException)
            End Sub

            Public Sub New(ByVal msg As String, Optional ByVal InnerException As Exception = Nothing)
                MyBase.New(msg, InnerException)
            End Sub
        End Class

        Class BW2DNSException
            Inherits BW2ConnectFailedException

            Public Sub New(Optional ByVal InnerException As Exception = Nothing)
                MyBase.New("Name Resolution Failed", InnerException)
            End Sub

        End Class


        Class BW2ConnectFailedCancelException
            Inherits BW2ConnectFailedException

            Public Sub New(Optional ByVal InnerException As Exception = Nothing)
                MyBase.New(InnerException)
            End Sub

        End Class
#End Region

        Private Delegate Function GetHostEntryHandler(ByVal adr As String) As IPHostEntry

        Private Shared m_htdnsCache As Hashtable = Specialized.CollectionsUtil.CreateCaseInsensitiveHashtable
        Private Shared m_dnsCacheReset As Date = Date.MinValue

        Public Shared Function DNSGetHostEntry(ByVal adr As String, Optional ByVal timeout As Integer = 10000, Optional ByVal SuppressDoEvents As Boolean = False) As IPHostEntry
            If m_dnsCacheReset < Date.UtcNow Then
                m_htdnsCache.Clear()
                m_dnsCacheReset = Date.MinValue
            End If
            If m_htdnsCache.ContainsKey(adr) Then
                Dim o As Object = m_htdnsCache(adr)
                If TypeOf o Is IPHostEntry Then
                    Return CType(m_htdnsCache(adr), IPHostEntry)
                Else
                    Throw CType(o, Exception)
                End If
            End If
            Try
                Dim callback As New GetHostEntryHandler(AddressOf Dns.GetHostEntry)
                Dim result As IAsyncResult = callback.BeginInvoke(adr, Nothing, Nothing)
                Dim ts As Date = Date.Now
                Dim te As Date = ts.AddMilliseconds(timeout)
                While ts < te
                    If (result.AsyncWaitHandle.WaitOne(Math.Max(1, Math.Min(50, CInt(te.Subtract(ts).TotalMilliseconds))), False)) Then
                        Dim iph As IPHostEntry = CType(callback.EndInvoke(result), IPHostEntry)
                        m_htdnsCache(adr) = iph
                        If m_dnsCacheReset = Date.MinValue Then m_dnsCacheReset = Date.UtcNow.AddSeconds(60)
                        Return iph
                    End If
                    If Not SuppressDoEvents Then
                        System.Windows.Forms.Application.DoEvents()
                        Dim f As Form = System.Windows.Forms.Form.ActiveForm
                        If f IsNot Nothing Then
                            If f.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                                Throw New TimeoutException("DNS Timeout")
                            End If
                        End If
                    End If
                    ts = Date.Now
                    SetTimeoutProgress(ts, te, timeout, adr)
                End While

                Throw New TimeoutException("DNS Timeout")

            Catch ex As Sockets.SocketException When ex.SocketErrorCode = Sockets.SocketError.HostNotFound OrElse ex.SocketErrorCode = Sockets.SocketError.HostUnreachable
                m_htdnsCache(adr) = ex
                If m_dnsCacheReset = Date.MinValue Then m_dnsCacheReset = Date.UtcNow.AddSeconds(60)
                Throw

            Finally
                HideTimeoutProgress()
            End Try
        End Function

        Private Shared Sub SetTimeoutProgress(ByVal ts As Date, ByVal te As Date, ByVal timeout As Integer, Optional ByVal adr As String = "")

            If ProgressTimeout IsNot Nothing Then

                If ts >= te.Subtract(TimeSpan.FromMilliseconds(Math.Max(0, timeout - 1500))) Then
                    With CType(ProgressTimeout.Parent, LoginForm)
                        Dim conn As String = .GetCaption("Connecting")
                        If adr <> "" Then
                            .LabelProgress.Text = conn & ": " & adr
                        Else
                            .LabelProgress.Text = conn
                        End If
                        .LabelProgress.Visible = True
                    End With

                    'ProgressTimeout.Visible = True
                    'ProgressTimeout.Value = Math.Max(0, Math.Min(CInt(te.Subtract(ts).TotalMilliseconds - 100), timeout)) * 100 \ timeout
                    System.Windows.Forms.Application.DoEvents()
                End If
            End If

        End Sub
        Private Shared Sub HideTimeoutProgress()
            If ProgressTimeout IsNot Nothing Then
                ProgressTimeout.Visible = False
                With CType(ProgressTimeout.Parent, LoginForm)
                    If .LabelProgress.Text.StartsWith(.GetCaption("Connecting")) Then
                        .LabelProgress.Text = ""
                        .LabelProgress.Visible = False
                    End If
                End With
            End If
        End Sub

        Private Shared ProgressTimeout As ProgressBar = Nothing

#Region "Automatic Login..."

        Friend m_AddArgs As String = ""
        Friend m_StartingPortal As String = ""
        Friend m_StartingUser As String = ""
        Friend Restarting As Boolean

        Friend Sub Init()

            If m_StartingPortal <> "" AndAlso m_PortalGuidLookup.ContainsKey(m_StartingPortal) Then
                If m_StartingUser <> "" Then
                    If m_StartingUser <> "IntegratedSecurity" Then
                        m_LastUser = m_StartingUser
                    Else
                        m_LastUser = ""
                    End If
                End If

                DropDownPortal.SelectedItem = m_PortalGuidLookup(m_StartingPortal)
                TextBoxUsername.Text = m_LastUser

                If m_StartingUser = "IntegratedSecurity" Then
                    TextBoxUsername.Text = "" ' Environment.UserDomainName & "\" & Environment.UserName
                    SetIntegratedSecurity()
                    ButtonOK_Click(Nothing, Nothing)
                    Exit Sub
                End If

            Else
                Dim Last As String = m_LastUser
                If m_LastUser = "" Then
                    Last = Environment.UserDomainName & "\" & Environment.UserName
                End If

                DropDownPortal.SelectedItem = m_HistoryEntries(Last.ToLower)
                TextBoxUsername.Text = m_LastUser
            End If
            TextBoxPassword.Text = ""
            TextBoxUsername.Enabled = True
            TextBoxPassword.Enabled = True
            LabelUsername.Enabled = True
            LabelPassword.Enabled = True
            UpdateIntegratedSecurity()

            If DropDownPortal.SelectedItem Is Nothing AndAlso m_Portals.Count > 0 Then
                DropDownPortal.SelectedItem = m_Portals(0)
            End If
            If TextBoxUsername.Text = "" Then
                TextBoxUsername.Focus()
            Else
                TextBoxPassword.Focus()
            End If



        End Sub

#End Region



        Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemClearCache.Click
            Try
                ' clear all local data
                With CType(Me.DropDownPortal.SelectedItem, PortalInfo)
                    If MsgBox(GetCaption("Do you really want to delete all cached data?"), MsgBoxStyle.Question Or MsgBoxStyle.OkCancel, .PortalDisplayName) = MsgBoxResult.Ok Then

                        Dim subpaths As String() = {"Data\Objects\" & .PortalGuid & "\Users"} ', "Documents\" & .PortalGuid}
                        For Each sp As String In subpaths

                            Dim path As String = System.Windows.Forms.Application.StartupPath + "\" + sp
                            If IO.Directory.Exists(path) Then
                                Dim path2 As String = path & "_" & System.Guid.NewGuid.ToString
                                Try
                                    IO.Directory.Move(path, path2)

                                Catch ex As Exception
                                    MsgBox(ex.Message, MsgBoxStyle.SystemModal)
                                    Exit Sub
                                End Try
                                IO.Directory.Delete(path2, True)
                            End If
                        Next
                        Dim subpaths2 As String() = {"Documents\" & .PortalGuid}
                        For Each sp As String In subpaths2

                            Dim path As String = System.Windows.Forms.Application.StartupPath + "\" + sp
                            If IO.Directory.Exists(path) Then
                                Dim di As New IO.DirectoryInfo(path)
                                ' user
                                For Each userdir As IO.DirectoryInfo In di.GetDirectories

                                    Dim copypath As String = userdir.FullName & "\Copy"
                                    If IO.Directory.Exists(copypath) Then
                                        Dim path2 As String = copypath & "_" & System.Guid.NewGuid.ToString
                                        Try
                                            IO.Directory.Move(copypath, path2)

                                        Catch ex As Exception
                                            MsgBox(ex.Message, MsgBoxStyle.SystemModal)
                                            Exit Sub
                                        End Try
                                        IO.Directory.Delete(path2, True)
                                    End If

                                Next
                            End If
                        Next

                    End If
                End With
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.SystemModal)
            End Try
        End Sub

        'remove portal
        Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemRemovePortal.Click
            ' remove selected portal
            Try
                With CType(Me.DropDownPortal.SelectedItem, PortalInfo)

                    If MsgBox(Me.GetCaption("Do you really want to remove the selected Portal?"), MsgBoxStyle.Question Or MsgBoxStyle.OkCancel, .PortalDisplayName) = MsgBoxResult.Ok Then
                        RemovePortal()
                    End If
                End With
            Catch
            End Try
        End Sub

        'add portal
        Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAddPortals.Click
            AddPortal()
        End Sub



        'show logfile
        Private Sub MenuItemShowLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemShowLog.Click
            Try
                ShowLog()
            Catch ex As Exception

            End Try
        End Sub

        Public Sub ShowLog()
            Try
                If IO.File.Exists(System.Windows.Forms.Application.StartupPath & "\Data\Tracer\startup.log") Then
                    Shell("Notepad.exe """ & System.Windows.Forms.Application.StartupPath & "\Data\Tracer\startup.log""", AppWinStyle.NormalFocus)
                End If
            Catch ex As Exception

            End Try
        End Sub

        Protected ws As New BW2WebAppService.BW2WebAppService

        Private Sub MenuItemFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemFile.Click
            Try
                Dim fn As String = System.Windows.Forms.Application.StartupPath & "\Remote_Portals.xml"
                DownloadFile(SelectedPortal.PortalURL.TrimEnd("/\".ToCharArray) + "/download/Portals.xml", fn, , , , , "OnUpdatePortalClicked")
                UpdateXML(fn, True, Me.m_Portals.Count = 1)
                IO.File.Delete(fn)
            Catch
            End Try
        End Sub

        Private Sub LoginForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
            Environment.Exit(0)
        End Sub

        Private Sub LoginForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            Me.FormBorderStyle = FormBorderStyle.None
            Me.Height = 300
            Me.Width = 400
            Dim p As New Drawing2D.GraphicsPath()
            p.StartFigure()
            p.AddArc(New Rectangle(0, 0, 40, 40), 180, 90)
            p.AddLine(40, 0, Me.Width - 40, 0)
            p.AddArc(New Rectangle(Me.Width - 40, 0, 40, 40), -90, 90)
            p.AddLine(Me.Width, 40, Me.Width, Me.Height - 40)
            p.AddArc(New Rectangle(Me.Width - 40, Me.Height - 40, 40, 40), 0, 90)
            p.AddLine(Me.Width - 40, Me.Height, 40, Me.Height)
            p.AddArc(New Rectangle(0, Me.Height - 40, 40, 40), 90, 90)
            p.CloseFigure()
            Me.Region = New Region(p)
            Me.BackColor = Color.Red
        End Sub

        Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
            Me.DropDownPortal.DroppedDown = True
            'MsgBox("Test")
        End Sub

        Private Sub PictureBox1_MouseDown(sender As Object, e As MouseEventArgs) Handles PictureBox1.MouseDown
            Me.PictureBox1.Image = My.Resources.CustomComboBoxHoverStyle
        End Sub

        Private Sub PictureBox1_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureBox1.MouseUp
            Me.PictureBox1.Image = My.Resources.CustomComboBoxNormal

        End Sub
    End Class



    Module [Global]
        Friend Declare Sub DisableProcessWindowsGhosting Lib "user32" ()

        Public Function CertificateValidation(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean

            '  MsgBox(certificate.Issuer)



            Return True

        End Function


        Public Sub Main()
            Try
                DisableProcessWindowsGhosting()
            Catch
            End Try

            ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf CertificateValidation)


            'ServicePointManager.CertificatePolicy = New MyCertValidation(ServicePointManager.CertificatePolicy)

            Dim bTest As Boolean = False
            'If Windows.Forms.Form.ModifierKeys = Keys.Shift Then
            '    bTest = True
            '    '    MsgBox("Environment:")
            '    '    MsgBox("Environment.MachineName=" & Environment.MachineName)
            '    '    MsgBox("Environment.UserDomainName=" & Environment.UserDomainName)
            '    '    MsgBox("Environment.UserName=" & Environment.UserName)
            'End If

            'Windows.Forms.Application.EnableVisualStyles()
            'Windows.Forms.Application.DoEvents()

            Dim strs As String() = System.Environment.GetCommandLineArgs
            If strs.Length >= 4 AndAlso strs(1) = "UPDATE" Then
                ' running from downloaded assembly path (.\bin_dld)
                Dim ix As Integer = 4
                Dim AddArgs As String = ""
                While ix < strs.Length
                    AddArgs += " """ & strs(ix) & """"
                    ix += 1
                End While
                Try
                    Process.GetProcessById(CInt(strs(2))).WaitForExit()
                Catch
                End Try
                Dim path As String = System.Windows.Forms.Application.StartupPath
                Dim targetpath As String = strs(3)
                If File.Exists(path & "/BW2Startup.exe") Then
                    Dim tries As Integer = 30
                    If File.Exists(targetpath & "/BW2Startup_old.exe") Then
                        File.Delete(targetpath & "/BW2Startup_old.exe")
                    End If
                    While tries > 0
                        Try
                            File.Move(targetpath & "/BW2Startup.exe", targetpath & "/BW2Startup_old.exe")
                            File.Move(path & "/BW2Startup.exe", targetpath & "/BW2Startup.exe")
                            If File.Exists(targetpath & "/BW2Startup_old.exe") Then
                                File.Delete(targetpath & "/BW2Startup_old.exe")
                            End If
                            Dim s As String = ""

                            Shell(targetpath & "/BW2Startup.exe ""RESTART""" & AddArgs, AppWinStyle.NormalFocus)
                            Try
                                Threading.Thread.Sleep(500)
                                Process.GetCurrentProcess.Kill()
                            Catch
                            End Try
                            System.Environment.Exit(0)
                        Catch
                        End Try
                        Threading.Thread.Sleep(1000)
                        tries -= 1
                    End While
                    Dim form As New LoginForm
                    MsgBox(form.GetCaption("Could not install new BW2Startup.exe"), MsgBoxStyle.SystemModal, form.GetCaption("Download failed"))
                End If
            Else
                Dim form As New LoginForm

                Dim path As String = System.Windows.Forms.Application.StartupPath
                Dim testfile As String = path & "\Data\Tracer\Temp\" & System.Guid.NewGuid.ToString & ".txt"
                Try
                    IO.Directory.CreateDirectory(path & "\Data\Tracer\Temp")
                    Dim fs As IO.FileStream = IO.File.Create(testfile)
                    fs.WriteByte(65)
                    fs.Close()
                    IO.File.Delete(testfile)

                    path = path & "\Data\Tracer"
                    form.Log("BW2Startup.exe (" & Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString & ") is starting up...")

                Catch ex As IO.IOException
                    MsgBox(Replace(form.GetCaption("Write not allowed in directory [{Path}].@LF Please change access rights or contact your administrator."), "[{Path}]", path, , , CompareMethod.Text), MsgBoxStyle.SystemModal, form.GetCaption("Startup error"))
                    End
                Catch ex As System.Security.SecurityException
                    MsgBox(Replace(form.GetCaption("Write not allowed in directory [{Path}].@LF Please change access rights or contact your administrator."), "[{Path}]", path, , , CompareMethod.Text), MsgBoxStyle.SystemModal, form.GetCaption("Startup error"))
                    End
                Catch ex As System.UnauthorizedAccessException
                    MsgBox(Replace(form.GetCaption("Write not allowed in directory [{Path}].@LF Please change access rights or contact your administrator."), "[{Path}]", path, , , CompareMethod.Text), MsgBoxStyle.SystemModal, form.GetCaption("Startup error"))
                    End
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.SystemModal)
                    End
                End Try

                Dim ix As Integer = 1
                If strs.Length > 1 AndAlso strs(1) = "RESTART" Then
                    form.Restarting = True
                    ix += 1
                End If
                If ix < strs.Length Then
                    form.m_StartingPortal = strs(ix)
                End If
                If ix + 1 < strs.Length Then
                    form.m_StartingUser = strs(ix + 1)
                End If
                While ix < strs.Length
                    form.m_AddArgs += " """ & strs(ix) & """"
                    ix += 1
                End While
                form.Show()

                form.Init()
                form.Log("Check for Update Startup.exe")
                If Not form.CheckForUpdateStartup() Then
                    Environment.Exit(0)
                    Exit Sub
                End If

                If (System.Windows.Forms.Form.ModifierKeys And Keys.Shift) = 0 Then
                    If form.SelectedPortal IsNot Nothing AndAlso form.SelectedPortal.AutoLoginOnWindowsDomain <> "" AndAlso form.SelectedPortal.AutoLoginOnWindowsDomain.Equals(System.Environment.UserDomainName, StringComparison.InvariantCultureIgnoreCase) Then
                        form.m_StartingUser = "IntegratedSecurity"
                        form.ButtonOK.PerformClick()
                        Exit Sub
                    End If
                Else
                    form.Activate()
                    form.BringToFront()
                    System.Windows.Forms.Application.DoEvents()
                End If
                form.Log("Running windows loop...")

                If bTest Then
                    System.Windows.Forms.Application.DoEvents()
                    form.Show()
                    Do
                        If form.Visible = False Then
                            Exit Do
                        End If
                        Try
                            System.Windows.Forms.Application.DoEvents()
                            Threading.Thread.Sleep(10)
                        Catch ex As Exception
                            If Not form Is Nothing Then form.Log("Message Loop", ex)
                        End Try
                    Loop
                Else
                    System.Windows.Forms.Application.Run()
                End If
            End If
        End Sub

    End Module

End Namespace