Public Class TextConverter

    Public Shared Function QuotedPrintableDecode(ByVal SourceText As String) As String

        If IsNothing(SourceText) Then Return ""

        Dim i As Integer
        Dim ix As Integer
        Dim n As Integer
        Dim ca As Char() = SourceText.ToCharArray

        'Number of "=" to size the byte array
        Dim ce As CharEnumerator = SourceText.GetEnumerator
        While ce.MoveNext
            If ce.Current = "=" Then
                n += 1
            End If
        End While

        Dim ba(SourceText.Length - (n * 2) - 1) As Byte

        Do While ix < SourceText.Length
            If ca(ix) = "=" Then
                'each "=XX" (XX means a hexadecimal expression) gives ONE byte do decode
                ba(i) = CByte(CInt("&H" & ca(ix + 1) & ca(ix + 2)))
                ix += 3
            Else
                ba(i) = CByte(Asc(ca(ix)))
                ix += 1
            End If
            i += 1
        Loop

        Return System.Text.Encoding.UTF8.GetString(ba)


    End Function

    Public Shared Function Base64Decode(ByVal SourceText As String) As String
        If IsNothing(SourceText) Then Return Nothing
        If SourceText.Length = 0 Then Return ""

        Return System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(SourceText))
    End Function

    Private Shared Function RemoveHTMLComments(ByVal s As String) As String
        Dim li As Integer = 0
        Do
            li = s.IndexOf("<!--", li)
            If li < 0 Then
                Exit Do
            End If
            Dim ri As Integer = s.IndexOf("-->", li)
            If ri < 0 Then
                s = s.Substring(0, li)
                Exit Do
            End If
            s = s.Remove(li, ri + 3 - li)
        Loop
        Return s
    End Function
    Private Shared Function RemoveHTMLTagsInner(ByVal s As String) As String
        'extract special signs
        s = Replace(s, "<br>", vbCrLf, , , CompareMethod.Text)
        s = Replace(s, "&nbsp;", " ")
        Dim re As New Text.RegularExpressions.Regex("<[^>]*>", Text.RegularExpressions.RegexOptions.Multiline)
        s = re.Replace(s, "")
        'extract white space
        Dim li As Integer = 0
        Do
            li = s.IndexOf(vbCrLf, li)
            If li < 0 Then
                Exit Do
            End If
            Dim ri As Integer = li + 2
            While ri < s.Length AndAlso Char.IsWhiteSpace(s.Chars(ri))
                ri += 1
            End While
            li += 2
            If ri > li Then
                s = s.Remove(li, ri - li)
            End If
        Loop

        s = Replace(s, "&;", "&amp;;")
        'decode html conform
        Return System.Web.HttpUtility.HtmlDecode(s)
    End Function
    Public Shared Function ExtractTextFromHTML(ByVal s As String) As String
        If s = "" Then
            Return ""
        Else
            s = RemoveHTMLComments(s)
            Return RemoveHTMLTagsInner(s)
        End If
    End Function
    Public Shared Function ExtractTextFromHTMLBody(ByVal s As String) As String
        If s = "" Then
            Return ""
        Else
            s = RemoveHTMLComments(s)

            Dim li As Integer = s.ToLower.IndexOf("<body")
            If li >= 0 Then
                Dim ri As Integer = s.ToLower.IndexOf("</body>", li)
                If ri >= 0 Then
                    s = s.Substring(li, ri - li + 7)
                End If
            End If

            s = RemoveHTMLTagsInner(s)

            If s = "" Then
                Return ""
            Else
                Return Replace(s, Chr(160), "")
            End If
        End If
    End Function

End Class